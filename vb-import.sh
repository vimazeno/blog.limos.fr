vboxmanage import ~/Documents/kali.ova
vboxmanage import ~/Documents/dvwa.ova
vboxmanage import ~/Documents/debian.ova
vboxmanage import ~/Documents/proxy.ova
vboxmanage import ~/Documents/thenetwork.ova
vboxmanage modifyvm kali --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm dvwa --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm debian --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm proxy --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm thenetwork --nic1 natnetwork --nat-network1 natwebsec