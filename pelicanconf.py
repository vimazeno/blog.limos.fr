#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Vincent Mazenod'
SITENAME = 'Vincent Mazenod / ingénieur de recherche'
SITELOGO = 'images/mazenovi.png'
OIMPHOTO = 'images/oim.jpg'
FAVICON = 'images/favicon.ico'
SITEURL = 'https://perso.limos.fr/~mazenovi/'
# DISQUS_SITENAME = 'limos-1'
# DISQUS_SHORTNAME = 'mazenovi'

LINKS = (
    (
        'user-circle',
        'Farouk Toumani',
        'http://isima.fr/~ftoumani/'
    ),
    (
        'user-circle',
        'Pascal Lafourcade',
        'https://sancy.iut.uca.fr/~lafourcade/index-en.html'
    ),
    (
        'user-circle',
        'Christophe Rey',
        'http://chrey.blogspot.com/'
    ),
    (
        'user-circle',
        'Marie Pailloux',
        'http://isima.fr/~pailloux/'
    ),
    (
        'user-circle',
        'Engelbert Mephu Nguifo',
        'http://isima.fr/~mephu/'
    ),
    (
        'user-circle',
        'Jean-Marie Favreau',
        'https://jmfavreau.info/'
    ),
    (
        'user-circle',
        'Pascal Huguet',
        'https://lapsco.fr/la-direction.html'
    ),
)

RESEARCH = (
    (
        'https://limos.fr/static/limos/limos.png',
        'width: 40px',
        'LIMOS',
        'https://limos.fr/detailperson/109',
    ),
    (
        'https://hal.science/assets/img/hal-logo-header.png',
        'width: 20px',
        'HAL',
        'https://cv.hal.science/vincent-mazenod',        
    ),
    (
        'https://static-00.iconduck.com/assets.00/google-scholar-icon-2048x2048-sjbhklt7.png',
        'width: 20px',
        'Google Scholar',
        'https://scholar.google.com/citations?user=CFWYNi8AAAAJ',        
    ),
)

SOCIAL = (
    (
        'users-cog',
        '<span style="font-size:13px">CRI ISIMA/LIMOS</span>',
        'https://hedgedoc.isima.fr/p/gD_i47uIi#/'
    ),
    (
        'building',
        '<span style="font-size:13px">Bureau A115 - 1<sup>ère</sup> étage</span>',
        '#'
    ),
    # (
    #     'phone',
    #     '04 73 40 50 27',
    #     '#'
    # ),
    (
        'envelope',
        '<span style="font-size:13px">vincent.mazenod@limos.fr</span>',
        'mailto:vincent.mazenod@limos.fr'
    ),
    (
        'linkedin',
        'LinkedIn',
        'pages/pgp.html'
    ),
    (
        'key',
        'clé publique PGP',
        'https://www.linkedin.com/in/mazenovi'
    ),
)

#INDEX_SAVE_AS = 'index.html'
#PAGE_SAVE_AS = '{slug}.html'
#PAGE_URL = '{slug}.html'

# INDEX_LABEL = '<i class="fa fa-lightbulb-o" aria-hidden="true"></i> Blog'
# HIDE_SIDEBAR = 'true'

CC_LICENSE = "CC-BY-NC-ND"
# "CC-BY" (require attribution)
# "CC-BY-SA" (require ShareAlike)
# "CC-BY-ND" (NoDerivatives)
# "CC-BY-NC" (require attribution, no commercial reuse)
# "CC-BY-NC-SA" (require ShareAlike, no commercial reuse)
# "CC-BY-NC-ND" (NoDerivatives, no commercial reuse).
CC_LICENSE_DERIVATIVES = "ShareAlike"
# "yes" if permitted
# "no" if not permitted
# "ShareAlike" if derivatives must be shared under the same terms.
CC_LICENSE_COMMERCIAL = "no"
# "yes" if commercial reuse is permitted
# "no" otherwise.
# CUSTOM_LICENSE='Unless otherwise stated, all articles are published under' +
#        'the <a href="http://www.wtfpl.net/about/">WTFPL</a> license.'""
# comment CC_* vars and uncommment CUSTOM LICENSE TO ENABLE IT


DISPLAY_TAGS_ON_SIDEBAR = True
TAG_CLOUD_STEPS = 8
TAG_CLOUD_MAX_ITEMS = 20
TAG_CLOUD_BADGE = True

DIRECT_TEMPLATES = ('index', 'categories', 'authors', 'archives', 'search')


# Vous ne devriez pas toucher à cette partie
# à moins que vous ne sachiez ce que vous faites ;)
RELATIVE_URLS = True
DEFAULT_LANG = 'fr'
LOCALE = "fr_FR.UTF-8"
ROOT_LANG = 'fr'
I18N_TEMPLATES_LANG = 'en'
I18N_SUBSITES = {
    'en': {
        'OUTPUT_PATH': 'output/en/',
    },
    'fr': {
        'Summary': 'Table des matières',
        }
    }

DEFAULT_PAGINATION = 10
BOOTSTRAP_THEME = "flatly"
PATH = 'content'
TIMEZONE = 'Europe/Paris'
DISPLAY_CATEGORIES_ON_MENU = False
SITELOGO_SIZE = "50px"
# Feed generation is usually not desired when developing
FEED_ATOM = "atom.xml"
FEED_RSS = "rss.xml"
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
THEME = os.path.dirname(os.path.abspath(__file__)) + "/themes/limos"
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGIN_PATHS = [os.path.dirname(os.path.abspath(__file__)) + '/plugins']
# @todo https://github.com/getpelican/pelican-plugins/tree/master/assets
PLUGINS = ['i18n_subsites', 'tag_cloud', 'tipue_search', 'math_render', 'extract_toc']
# MARKDOWN = {'extensions': ['toc']} # collision with pygment
MARKDOWN = {
    'extensions': ['toc'],
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}
# https://bootswatch.com/
SHOW_ARTICLE_AUTHOR = False
SHOW_ARTICLE_CATEGORY = True
SHOW_DATE_MODIFIED = True
CUSTOM_CSS = 'static/custom.css'
STATIC_PATHS = ['images', 'extra/custom.css', 'extra/custom.js', 'node_modules', 'slides']
ARTICLE_EXCLUDES = ['node_modules', 'slides']
PAGE_EXCLUDES = ['node_modules', 'slides']
STATIC_EXCLUDES = ['node_modules', 'slides']
EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
    'extra/custom.js': {'path': 'static/custom.js'}
}
PYGMENTS_STYLE = "monokai"
USE_PAGER = True
BOOTSTRAP_FLUID = False
DISPLAY_BREADCRUMBS = True
DISPLAY_CATEGORY_IN_BREADCRUMBS = True
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True
BOOTSTRAP_NAVBAR_INVERSE = False
DISPLAY_ARTICLE_INFO_ON_INDEX = True
DISABLE_SIDEBAR_TITLE_ICONS = True
PAGE_PATHS = ['pages']
