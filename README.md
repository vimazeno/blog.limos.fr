# Blog LIMOS de Vincent Mazenod

```
./bin/setup
source bin/activate
./bin/start
```

## Utilisation avec docker

### Build image 

```
docker build -t pelican_perso .
```

### run conatiner 

```
docker run -it --rm -v $(pwd):/srv/pelican mjjacko/pelican pelican-quickstart -p my-site
```

## Utilisation avec venv

### Lancer le serveur de prévisualisation

```
bash develop_server.sh start
```

* http://0.0.0.0:8000/

### faire tourner les slides en local

```
./bin/local
```

* http://slides/slides/



## see also

* [https://limos.isima.fr/~mazenod/blog-et-pages-perso-au-limos.html](https://limos.isima.fr/~mazenod/blog-et-pages-perso-au-limos.html)
