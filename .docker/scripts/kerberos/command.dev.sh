source .env 
SSH_CMD="ssh -o StrictHostKeyChecking=no -K ${KRB_USER}@${SERVER}"
RSYNC_CMD=''
kinit ${KRB_USER}
${SSH_CMD} "mkdir -p ~/public_html/"
${SSH_CMD} "echo 'Options +FollowSymLinks' >  ~/public_html/.htaccess"
${SSH_CMD} "echo 'RewriteEngine on' >>  ~/public_html/.htaccess"
${SSH_CMD} "echo 'RewriteCond %{HTTP_HOST} ^perso.isima.fr$' >>  ~/public_html/.htaccess"
${SSH_CMD} "echo 'RewriteRule (.*) https://perso.limos.fr%{REQUEST_URI} [R=301,L]' >>  ~/public_html/.htaccess"
${SSH_CMD} "mkdir -p ~/public_html/_"
${SSH_CMD} "echo '<FilesMatch "\.phar">' >  ~/public_html/_/.htaccess"
${SSH_CMD} "echo '   php_flag engine off' >>  ~/public_html/_/.htaccess"
${SSH_CMD} "echo '   ForceType application/octet-stream' >>  ~/public_html/_/.htaccess"
${SSH_CMD} "echo '   Header set Content-Disposition attachment' >>  ~/public_html/_/.htaccess"
${SSH_CMD} "echo '</FilesMatch>' >>  ~/public_html/_/.htaccess"
rsync -az --exclude "_" -e "ssh -o StrictHostKeyChecking=no -K" ./output/ ${KRB_USER}@${SERVER}:~/public_html/