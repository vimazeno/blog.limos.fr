git submodule init
git submodule update --recursive --remote
sed -i 's/\$port &/\$port/g' ./develop_server.sh
chmod +x develop_server.sh
export PATH=/root/.local/bin:$PATH
export PY='/usr/bin/python3'
./develop_server.sh start 8000