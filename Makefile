BOLD    := \033[1m
RESET   := \033[0m
RED     := \033[31m
GREEN   := \033[32m
YELLOW  := \033[33m
BLUE    := \033[34m
MAGENTA := \033[35m

SHELL    := /bin/bash
IMAGES   := $(shell docker compose config --services)
TAG      := $(shell git rev-parse --short HEAD)
VOLUMES  := content/node_modules output

# Executables (local)
DOCKER_COMP   := docker compose
.DEFAULT_GOAL := help

# Docker containers
KERBEROS_CONT  := $(DOCKER_COMP) run kerberos

include .env

build: ## Build required docker compose images
	@$(DOCKER_COMP) build
.PHONY: build	

push: ## Push build docker compose images
	@TAG=$(shell git rev-parse --short HEAD)
	@docker login ${REGISTRY_URL}
	@docker push ${NAMESPACE}/${APP_NAME}_python:latest ;
	@docker tag ${NAMESPACE}/${APP_NAME}_python:latest ${NAMESPACE}/${APP_NAME}_python:${TAG} ;
	@docker push ${NAMESPACE}/${APP_NAME}_python:${TAG} ;
	@docker push ${NAMESPACE}/${APP_NAME}_kerberos:latest ;
	@docker tag ${NAMESPACE}/${APP_NAME}_kerberos:latest ${NAMESPACE}/${APP_NAME}_kerberos:${TAG} ;
	@docker push ${NAMESPACE}/${APP_NAME}_kerberos:${TAG} ;
.PHONY: push

up: ## Make required containers up
	@$(DOCKER_COMP) up --remove-orphans node
	@echo -e "⏰ ${BOLD}${GREEN}Wait for node to install needed js (there's no problem we're just wait)...${RESET}"
	@$(DOCKER_COMP) run dockerize -timeout 180s -wait-retry-interval 3s \
			-wait file:///tmp/content/node_modules/.yarn-integrity ;
	@echo -e "⏰ ${BOLD}${GREEN}Wait for pelican to generate content (there's no problem we're just wait)...${RESET}"
	@echo -e "${GREEN}${BOLD}up and running${RESET}"
	@echo -e " 🐳 blog in docker ${BLUE}${BOLD}http://localhost:${PORT}${RESET}"
	@$(DOCKER_COMP) up --remove-orphans python
.PHONY: up

down: ## Make required containers down
	@$(DOCKER_COMP) down
.PHONY: down

logs: ## Show live containers logs. Pass the parameter "c=" to see one specific container's logs, example: make logs c='frontend'
	@$(eval c ?=)
	@if [ -z "$(c)" ]; then \
		$(DOCKER_COMP) logs --follow; \
	else \
		$(DOCKER_COMP) logs --timestamps --follow $(c); \
	fi 
.PHONY: logs

errors: ## Show live container errors.
	@$(DOCKER_COMP) logs | grep error
.PHONY: errors

publish: ## Open bash on php container
	@$(KERBEROS_CONT) bash .docker/scripts/kerberos/command.dev.sh
.PHONY: publish

clean: ## Remove node and php folders and files
	@for volume in ${VOLUMES} ; do \
		sudo rm -rf $${volume} ; \
	done
.PHONY: clean

slides: ## Force slides refresh
	@sudo cp -R content/slides output/
.PHONY: slides

help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help