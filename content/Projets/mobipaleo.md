Title: Mobipaleo <i class="fas fa-spinner fa-spin"></i>
Date: 2016-11-15 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, mining


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
        <span class="sr-only">90% complete</span>
    </div>
</div>

[Mobipaleo](https://mobipaleo.limos.fr/) est un **projet interdisciplinaire** visant à modéliser la biodiversité à partir d'études paleoécologiques et écologiques. Le [Geolab](https://geolab.uca.fr/) et le [Muséum national d'Histoire naturelle](https://www.mnhn.fr/fr) sont impliqués dans ce projet.

<hr />

## Pour ce projet (depuis 2016)

* j'ai défini les besoins et l'architecture technique
* j’ai développé [https://mobipaleo.limos.fr](http://mobipaleo.limos.fr) qui permet

    * d’exploiter facilement un outil d’extraction de corrélations.
    * de présenter visuellement les résultats
    * de gérer l'accès à la plateforme via un système de gestion d'utilisateur et une interface d'administration complète
    * [<i class="fab fa-linkedin"></i> Rémy ISSARD](https://www.linkedin.com/in/r%C3%A9my-issard-7b130731/?originalSubdomain=fr) est en charge de la maintenance du produit depuis 2023
* je suis **co auteur de plusieurs <a href="/pages/recherche.html"><span class="label label-success">articles de recherche</span></a>**