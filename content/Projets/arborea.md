Title: ARBORea <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
Date: 2019-10-06 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, santé


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        <span class="sr-only">100% complete</span>
    </div>
</div>

ARBORea est un **projet interdisciplinaire** mené en collaboration avec le [Centre Hospitalier Universitaire (CHU) de Clermont-Ferrand](https://www.chu-clermontferrand.fr/)

**Le recours aux contentions mécaniques est une pratique fréquente pour les soignants en service de réanimation**. Cet acte, réalisé sur prescription médicale, requiert l’attention de l’ensemble des équipes paramédicales et médicales pour sa mise en place, sa surveillance et sa levée. Il constitue une restriction majeure de la liberté individuelle des patients. La [Haute Autorité de Santé (HAS0](https://www.has-sante.fr/) a défini, pour les services de gériatrie et de psychiatrie, dix critères de bonnes pratiques afin d’assurer la qualité d’utilisation des contentions. **Aucune législation n’est retrouvée concernant ces pratiques en réanimation, bien que quotidiennement utilisées**. En pratique courante, le principal critère d’instauration des contentions rapporté par les soignants est la sécurité. L’équipe paramédicale, en première ligne au chevet des patients, est garante de leur sécurité et de leur intégrité physique. Cette décision, souvent laissée à la seule appréciation de l’infirmier(e), varie en fonction de sa propre représentation du risque et dépend de plusieurs facteurs : son ancienneté en réanimation, du ratio infirmier/patient et de la charge de travail dans l’unité.

Les patients de réanimation ayant souvent un conditionnement vital, l’évaluation de l’importance de celui-ci et du risque lié à son endommagement ou son retrait inopiné conditionne la décision de contention par le soignant. 
La désorientation ou le delirium peuvent entraîner des incidents sévères en favorisant des déconditionnements accidentels. Cependant, il est reconnu que les contentions peuvent devenir une cause majeure d’apparition d’un syndrome confusionnel (delirium) et d’une agitation. 

D’après la littérature, environ **33% des patients de réanimation font l’objet d’une contention**, et un déconditionnement accidentel est observé majoritairement chez ces derniers. Lors d’un recueil de données réalisées en 2018 au [Centre Hospitalier Universitaire (CHU) de Clermont-Ferrand](https://www.chu-clermontferrand.fr/), sur 275 patients de réanimation adulte, il ressortait un taux de contention de 28%. 

Afin de réduire la subjectivité et l’hétérogénéité des pratiques, un outil d’aide à la décision de mise en place des contentions a été développé. Il repose sur des éléments objectifs concernant l’état neurologique du patient sur la base des **scores de RASS (Richmond Agitation-Sedation Scale)** et de **CAM-ICU (Confusion Assessment Method for the Intensive Care Unit)** et par les **modifications de posologie des sédations**. Par ailleurs, trois catégories de patients ont été définies selon le caractère invasif du conditionnement et donc selon le risque encouru en cas de retrait inopiné. Enfin, la présence de la famille du patient et son adhésion à sa surveillance ont été intégrées à l’outil.

* **Objectifs** : Étudier conjointement l’efficacité et la tolérance de l’utilisation d’un outil d’aide à la décision de contention mécanique des patients hospitalisés en réanimation.

* **Type d’étude** : Essai contrôlé randomisé multicentrique en stepped wedge (permutation en grappe)

* **Nombre de centres** : 19 centres

<hr/>

## Pour ce projet (entre 2019 et 2022)

* j'ai défini les besoins et l'architecture technique
* j’ai développé un maquette, sécurisée en conformité avec la [cadre légal concernant l'hébergement et le traitement des données de santé](https://www.cnil.fr/fr/thematiques/sante), selon permettant aux soignants
  * de saisir un nouveau dossier pseudonymisé
  * de bénéficier d'une aide à la décision de contention
  * d'effectuer le suivi des incidents
  * de gérer l'accès des soignants à la plateforme via un système de gestion d'utilisateur et une interface d'administration complète
* Le projet est désormais géré par le [<i class="fab fa-linkedin"></i> Ruben Martinez](https://www.linkedin.com/in/ruben-martinez-8337a469/) actuellement en poste au [Centre Hospitalier Universitaire (CHU) de Clermont-Ferrand](https://www.chu-clermontferrand.fr/)
