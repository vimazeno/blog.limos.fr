Title: eP3C  <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
Date: 2017-11-21 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, éducation, STI, LRS


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        <span class="sr-only">100% complete</span>
    </div>
</div>


Le projet [e-P3C (Pluralité des Contextes, Compétences et Comportements)](https://www.ac-clermont.fr/presentation-des-resultats-e-fran-123094) est un projet lauréat de l'appel à projet [e-Fran](https://www.education.gouv.fr/e-fran-des-territoires-educatifs-d-innovation-numerique-326083) initié par la mission Monteil (mission interministérielle sur le numérique éducatif) dans le cadre du [Programme d’Investissements d’Avenir (PIA2)](https://www.education.gouv.fr/bo/15/Hebdo30/MENB1517013N.htm) et dans le but d’apporter un soutien financier à des projets mobilisateurs de territoires éducatifs innovants, de stimuler la création d’une culture partagée autour des enjeux de l’éducation à la société numérique et autour de la nécessité d’apprendre "le numérique" et "avec le numérique".

eP3C est porté par [Pascal Huguet](https://lapsco.fr/la-direction.html), Directeur du [laboratoire de psychologie sociale et cognitive (LAPSCO)](https://lapsco.fr) (UMR 6024 UCA-CNRS), en partenariat étroit avec le [Rectorat de Clermont-Ferrand](https://www.ac-clermont.fr/), notamment avec Marie- Claude Borion, chargée de mission à la délégation académique au Numérique et Nicolas Rocher IPR, représentant des corps d'inspection. Deux entreprises régionales ([maskott](https://www.maskott.com/) et [Perfect memory](https://perfect-memory.com)) ont également collaboré à l'élaboration de ce projet, qui a impliqué quelques 8000 élèves de l'académie dans 10 établissements scolaires, et est le fruit d'une réflexion concertée entre les différents partenaires avec une implication forte des corps d'inspection et des chefs d'établissements qui ont participé à une grande expérimentation sur la diversification des contextes d'apprentissage. 

Pendant les 4 ans de son déroulement les équipes pédagogiques ont été associées à la construction des protocoles de recherche. 

L'académie a soutenu fortement la candidature en s'engageant auprès des acteurs et en dégageant des heures  pour les enseignants référents dans les établissements.

L'objectif est d’optimiser l’utilisation des technologies numériques pour diversifier les contextes d’apprentissage au service de la réussite de tous les élèves. Concrètement, il s'agit de présenter un même objet d'apprentissage (par exemple un théorème, un principe de physique ou un problème de biologie) et des exercices afférents selon différentes modalités (des plus formelles aux plus ludiques) pour en augmenter la compréhension par tous les élèves, le tout au sein d'un système de tutorat intelligent (STI) capable de recommandations en fonction des actions , des erreurs et des succès de chaque élève. Les données collectées, très nombreuses, seront rigoureusement analysées pour tester l'efficacité des actions entreprises dans ce cadre. Le projet concerne tous les niveaux du collège au lycée.

[Plus d'informations](https://www.ac-clermont.fr/recherche/type/actualites?keywords=ep3c)

<hr/>

## Pour ce projet 

Entre 2017 et 2021

* j'ai défini les besoins et l'architecture technique
* j'ai recruté un développeur pour la conception de la plateforme
    * [Ruben Martinez](https://www.linkedin.com/in/ruben-martinez-8337a469/)
* j'ai encadré le développeur recruté tout au long du développement de la plateforme
* j'ai joué le rôle de coordianteur entre
    * le [rectorat de Clermont-Ferran](http://www.ac-clermont.fr/) notamment pour la partie hébergement
    * [Maskott](https://www.maskott.com/) pour la récupération des données en provenance du STI
    * [Perfect memory](https://perfect-memory.com) pour la partie organisation des connaissances sur les supports de cours
    * les chercheurs du [LAPSCO](https://www.lapsco.fr/) pour la mise en place des questionnaires
* je suis **co auteur de plusieurs <a href="/pages/recherche.html"><span class="label label-success">articles de recherche</span></a>**