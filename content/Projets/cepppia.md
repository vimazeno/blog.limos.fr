Title: Cepppia  <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
Date: 2016-10-15 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
tags: www, santé, mining


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        <span class="sr-only">100% complete</span>
    </div>
</div>

est un projet issu des travaux du groupe santé [du Conseil de Développement du Grand Clermont](https://www.legrandclermont.com/conseil-de-developpement). Il est porté par le [Centre Hospitalier Universitaire (CHU) de Clermont-Ferrand](https://www.chu-clermontferrand.fr/) et co-financé par l'Union Européenne. 

Il propose une ambition nouvelle en santé, par la mise en œuvre du concept de **médecine 4P** : **Prédictive**, **Préventive**, **Personnalisée et Participative**. 

Ce projet est articulé autour d’un **questionnaire sur les habitudes alimentaires et comportementales** des personnes sondées et une partie analyse de données collectées permettant d’automatiser et d’enrichir la détection des profils à risque.

<hr />

## Pour ce projet

De 2016 à 2018

* j'ai défini les besoins et l'architecture technique sécurisée basée sur, une analyse de risque respectant les contraintes du cahier des charges HDS.
* j'ai rédigé les spécifications techniques pour l’appel d’offre pour un hébergement certifié HDS et participé au choix final du prestataire
* j'ai mis en conformité la plateforme avec [cadre légal concernant l'hébergement et le traitement des données de santé](https://www.cnil.fr/fr/thematiques/sante)
* j'ai encadré un ingénieur en charge du développement logiciel
    * Carlos Cepeda
* j'ai coordoné le déploiement de l'applicatif en production avec le prestataire d'hébergement de données de santé [IBO](https://www.ibo.fr/)
