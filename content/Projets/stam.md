Title: Stam  <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
Date: 2019-10-06 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, métro, Feder


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        <span class="sr-only">100% complete</span>
    </div>
</div>

## STAM

Stam est un projet [FEDER](https://www.europe-en-france.gouv.fr/fr/fonds-europeens/fonds-europeen-de-developpement-regional-FEDER) piloté par un consortium composé d’acteurs du monde de la métrologie et de chercheurs ayant pour vocation la conception d’une plateforme de révision de mesures réalisées à l’aide d’outils connectés.

## L’ENJEU

Pour comprendre tout l’enjeu derrière ce projet, il faut d’abord se départir d’un apriori trèsrépandu: une mesure est exacte. En effet, la mesure comporte toujours un biais induit par des phénomènes physiques ou humains; dans certaines conditions ce biais a un impact non négligeable(programme spatial par exemple) et c’est ainsi que diverses méthodologies pour quantifier les incertitudes de mesure ont vu le jour (voir le Guide sur l’Incertitude de Mesure).Toutefois, ces méthodologies reposent sur des approches manuelles d’inventaire des causes d’incertitudes. Elles sont donc assez laborieuses à mettre en oeuvre et pas assez conscientes du contexte opératoire qui est le leur.C’est de là qu’est née l’idée de concevoir une plateforme recueillant à la volée des mesures et des données de contexte venant d’objet connectés. Cela permettrait dans un premier temps de faciliter la saisie de la valeur lue et d’autre part de prendre en compte dans la phase de révision de la mesure diverses informations de contexte (hygrométrie relative,répétabilité inter-opérateur). Ces informations de contexte sont agrégées sous forme de convolutions de lois de distribution pour modéliser la dispersion de l’erreur.

## MISE EN OEUVRE

Chacune des parties prenantes se charge d’un des aspects techniques: 

* [Deltamu](https://deltamu.com) précise les spécifications
* [Phiméca](phimeca.com) se charge de l’implémentation des outils de calcul
* [Perfect memory](https://perfect-memory.com) propose un outil documentaire basé sur la sémantique
* Le [LIMOS](https://limos.fr/) dresse l’architecture globale et propose un POC de la plateforme

<hr />

## Pour ce projet (de 2019 à 2021)

* j'ai défini les besoins et l'architecture technique
* j'ai recruté un développeur pour la conception de la plateforme
    * [<i class="fab fa-linkedin"></i> Papa Mamadou Ndiaye](https://www.linkedin.com/in/papa-mamadou-ndiaye-15338984/)
* j'ai encadré ce développeur pour la réalisation d'une maquette de la plateforme
* j'ai joué le rôle de coordinateur entre les différents acteurs