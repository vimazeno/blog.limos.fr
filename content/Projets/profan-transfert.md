Title: Profan-Transfert  <i class="fas fa-spinner fa-spin"></i>
Date: 2023-03-30 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, éducation


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
        <span class="sr-only">30% complete</span>
    </div>
</div>

<img src="https://profan-transfert.limos.fr/ProFAN.svg" style="width:400px;padding:20px" />
### un enjeu pour l'enseignement, un enjeu pour la science


L'ambition première de ProFan Transfert est d'adosser la formation à la recherche scientifique de manière pragmatique.

Cette ambition est actuellement incarnée, pour chaque participant à l'expérimentation, par une plateforme numérique ayant pour but de fluidifier toutes les étapes nécessaires à l'enseignement: la formation des enseignants, la conception d'activités pédagogiques conformes à ces formations, et finalement le bon déroulement des activités ainsi construites, avec les apprenants.

En s'attaquant à la formation professionnelle, Profan-Transfert cible un sous ensemble de l'enseignement secondaire orienté de fait vers la pratique. Ce sous ensemble représente à la fois un enjeu majeur pour les métiers de demain, un contexte propice à la numérisation de l'enseignement et une population d'enseignants et d'apprenants significative, qui permettra de valider ou non les hypothèses que le projet sous-tend. 

L'organisation en cohorte incrémentale par année, permet une montée en charge progressive. Chaque année, de nouvelles fonctionnalités sont proposées à la première cohorte historique. Les nouveaux utilisateurs entrant dans le programme au fil des années suivantes, suivent alors le chemin de formation, d'élaboration et de mise en application, tracé et consolidé par la cohorte précédente.

La plateforme est pensée pour faciliter la collaboration, la diffusion et le partage d'informations. Accessible par un simple navigateur et sécurisée par une gestion de permissions fine, elle permet à chaque utilisateur, de consulter toutes les ressources qui lui sont accessibles, et de produire lui même son propre contenu. Les contenus sont organisés par profils d'utilisateur et par académie, mais accessibles en lecture par tous, afin de favoriser les synergies.

La mise à disposition de contenu est gérée par un produit de partage de fichiers open source (https://owncloud.com), les échanges entre utilisateurs par un produit de discussion open source (https://www.discourse.org), et le reste de la plateforme a été intégralement développé par le LIMOS (https://limos.fr) dans un esprit de réutilisabilité. La souveraineté induite par ces choix technologiques, permet une grande réactivité, ainsi qu'une grande liberté quant aux évolutions des fonctionnalités tout au long de l'expérimentation. Les activités pédagogiques présentées aux apprenants peuvent notamment faire l'objet de développements particuliers: par filière, voire par discipline.

La souveraineté de la plateforme ProFan Transfert permet surtout l'extraction du surplus comportemental de tous les utilisateurs, c'est à dire toutes leurs actions de consultation et d'édition de contenus. C'est sans doute le caractère le plus innovant de l'expérimentation. L'exploitation sans partage des faits et gestes numériques des utilisateurs est le modèle économique des BigTech et autres GAFAM depuis maintenant plus de 20 ans. ProFan Transfert, conscient de la valeur intrinsèque de ce type d'information, propose une exploitation scientifique de ces données, en les contextualisant et en les portant à la connaissance de la communauté scientifique et plus particulièrement, à l'attention des chercheurs en sciences psychologiques et cognitives. Les données seront accessibles dans un entrepôt de données, conforme au cadre légal sur les données personnelles, conçu lui aussi par le LIMOS.

Ainsi ProFan transfert propose à la fois des outils pour les enseignants, des outils pour les apprenants et des outils pour les chercheurs, qui permettront d'amorcer une boucle de rétroaction entre la science et l'enseignement, afin de les faire avancer en synergie.

* Plus de détails sur le projet ProFAN-Transfert: [https://www.enseignementsup-recherche.gouv.fr/fr/bo/23/Hebdo13/MENE2308139N.htm](https://www.enseignementsup-recherche.gouv.fr/fr/bo/23/Hebdo13/MENE2308139N.htm)
* Lien vers la plateforme: [https://profan-transfert.limos.fr/](https://profan-transfert.limos.fr/)

<hr />

## Pour ce projet (depuis 2023)

* j'ai défini les besoins et l'architecture technique
* j'ai recruté 4 développeurs pour la conception de la plateforme
    * [<i class="fab fa-linkedin"></i> Timothé Bertrand](https://www.linkedin.com/in/timoth%C3%A9-bertrand-%F0%9F%A6%80-1a178b200/)
    * [<i class="fab fa-linkedin"></i> Théo Lecoublet](https://www.linkedin.com/in/th%C3%A9o-lecoublet/)
    * [<i class="fab fa-linkedin"></i> Stéphane Guillet](https://www.linkedin.com/in/stephane-guillet/)
    * Axel Mascaro
* j'encadre ces 4 développeurs
* j'ai joué le rôle de coordinateur entre
    * la mission Monteil, les chercheurs du [LAPSCO](https://www.lapsco.fr/) et du [LP3C](https://www.lp3c.fr/en/presentation/) pour la mise en place des fonctionnalités de la plateforme
* je suis **co auteur de plusieurs <a href="/pages/recherche.html"><span class="label label-success">articles de recherche</span></a>** en tant que membre du consortium ProFan
