Title: ECCIPE  <i class="fas fa-spinner fa-spin"></i>
Date: 2021-01-06 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, éducation


<div class="progress">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
        <span class="sr-only">50% complete</span>
    </div>
</div>

Pour le moment eccipe est un projet autonome, développé en collaboration étroite avec le [LP3C](https://www.lp3c.fr). A terme il fera parti intégrante de l'écosystème applicatif du projet <a href="/profan-transfert.html"><span class="label label-success">ProFan-Transfert</span></a>.

Ce projet a pour objectif de concevoir et évaluer l’efficacité d’un outil numérique d’entraînement au travail en équipe sur le développement des compétences collaboratives utilisable en présentiel et/ou à distance. Il s’adresse aux publics en formation initiale et en formation continue.

Le programme de recherches menées sur différents terrains d’études (lycées, universités, écoles d’ingénieurs, …) devra permettre de répondre à deux questions :

1) comment accompagner/guider les apprenants pendant les activités collaboratives pour favoriser des processus d’équipe efficaces au service du développement de compétences collaboratives (Thèse de Sacha DROUET) ?

2) quels types de feed-back faut-il délivrer aux apprenants après une séance de travail en équipe pour conduire des séances de débriefing susceptibles de faciliter le développement de ces compétences (Thèse d’Albane PECHARD) ?

* Plus de détails sur eccipe: [https://www.lp3c.fr/projets-finances/eccipe/](https://www.lp3c.fr/projets-finances/eccipe/)
* Lien vers la plateforme: [https://eccipe.limos.fr/](https://eccipe.limos.fr/)

<hr />

## Pour ce projet (depuis 2021)

* j'ai défini les besoins et l'architecture technique
* j'ai recruté un développeur pour la conception de la plateforme
    * [<i class="fab fa-linkedin"></i> Lise Demourgues](https://www.linkedin.com/in/lise-d-93935b15a/)
* j'ai encadré ce développeur pour la réalisation d'une maquette de la plateforme
