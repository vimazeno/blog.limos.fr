Title: Profan  <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
Date: 2017-01-06 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, éducation


<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        <span class="sr-only">100% complete</span>
    </div>
</div>

![ProFan](images/projets/ProFAN.jpg)

Ce projet fait parti du [Programme d'investissements d'avenir (PIA)](https://www.education.gouv.fr/bo/16/Hebdo41/MENB1628228N.htm?cid_bo=108403). 

Ce projet se donne pour ambition, de promouvoir et de qualifier, par la nature de leurs effets, de nouveaux contextes d'apprentissage et d'enseignement afin de favoriser l'acquisition de compétences nouvelles pour répondre aux exigences des métiers du futur. 

Cette expérimentation est centrée sur une plateforme numérique qui permettra l'organisation et la bonne tenue de l'expérimentation ainsi que la collecte de données exploitables par la recherche en sciences cognitives dans 79 lycées professionnels, sur 3 années scolaires.

<hr />

## Pour ce projet (de 2017 à 2021)

* j'ai défini les besoins et l'architecture technique
* j'ai recruté 2 développeurs pour la conception de la plateforme
    * [<i class="fab fa-linkedin"></i> Benoit Petitcollot](https://www.linkedin.com/in/beno%C3%AEt-petitcollot-60bbb6a6/?originalSubdomain=fr)
    * [<i class="fab fa-linkedin"></i> Ruben Martinez](https://www.linkedin.com/in/ruben-martinez-8337a469/)
    * Carlos Cepeda
* j'ai encadré jusqu'à 3 développeurs, ainsi qu'un stagiaire
* j'ai joué le rôle de coordinateur entre
    * les rectorats, le service des sytèmes d'information (SI) et le service d'analyse statistiques de l'éducation nationale pour la récupération des données concernant les lycéens, l'authentification des lycéens et professeurs et le déploiement de l'application
    * les chercheurs du [LAPSCO](https://www.lapsco.fr/) et du [LP3C](https://www.lp3c.fr/en/presentation/) pour la mise en place d'une dizaine de questionnaires et d'une demi douzaine outils d’évaluation
* je suis **co auteur de plusieurs <a href="/pages/recherche.html"><span class="label label-success">articles de recherche</span></a>** en tant que membre du consortium ProFan
* Le succès de cette expérimentation m'a également permis de piloter les projets <a href="/eccipe.html"><span class="label label-success">ECCIPE</span></a> et <a href="/profan-transfert.html"><span class="label label-success">ProFan-Transfert</span></a>
