Title:  <i class='fa fa-flask' aria-hidden='true'></i> Recherche
Date: 2021-03-09 10:27
Category: <i class='fa fa-flask' aria-hidden='true'></i> Recherche
Number: 1


Je fais partie du <a href="https://hal.science/search/index/q/*/authFullName_s/Profan+Consortium">ProFAN Consortium</a>

## 2024

* Rudmann O., Batruch A., Paolo Visintin E., Sommet N., Bressoux P., Darnon C., Bouet M., Bressan M., Brown G., Cepeda C., Cherbonnier A., Demolliens M., De Place A.-L., Desrichard O., Ducros T., Goron L., Hemon B., Huguet P., Jamet E., Martinez R., Mazenod V., Mella N., Michinov E., Michinov N., Ofosu N., Pansu P., Peter L., Petitcollot B., Poletti C., Régner I., Riant M., Robert A., Sanrey C., Stanczak A., Toumani F., Vilmin S., Vives E., Butera F.. **Cooperative learning reduces the gender gap in perceived social competences. A large-scale nationwide longitudinal experiment**. *Journal of Educational Psychology*. <span class="label label-info">en cours d'édition</span> <a href="/profan.html"><span class="label label-success">ProFAN</span></a>

* Mathilde Riant, Anne-Laure de Place, Pascal Bressoux, Anatolia Batruch, Marinette Bouet, Marco Bressan, Genavee Brown, Fabrizio Butera, Carlos Cepeda, Anthony Cherbonnier, Céline Darnon, Marie Demolliens, Olivier Desrichard, Théo Ducros, Luc Goron, Brivael Hémon, Pascal Huguet, Eric Jamet, Ruben Martinez, Vincent Mazenod, Nathalie Mella, Estelle Michinov, Nicolas Michinov, Nana Ofosu, Laurine Peter, Benoît Petitcollot, Céline Poletti, Isabelle Régner, Anaïs Robert, Ocyna Rudmann, Camille Sanrey, Arnaud Stanczak, Farouk Toumani, Simon Vilmin, Emilio Paolo Visintin, Eva Vives, Pascal Pansu. **Does the Jigsaw method improve motivation and self-regulation in vocational high schools?**.
*Contemporary Educational Psychology, Volume 77, 102278, ISSN 0361-476X*. [https://doi.org/10.1016/j.cedpsych.2024.102278](https://doi.org/10.1016/j.cedpsych.2024.102278) <a href="/profan.html"><span class="label label-success">ProFAN</span></a>

* Vives E., Poletti C., Butera F., Huguet P., ProFAN Consortium, Régner I.. **Learning with Jigsaw : A systematic Review gathering all the pieces of the Puzzle more than 40 years later**. *Review of Educational Research* ([https://doi.org/10.3102/00346543241230064](https://doi.org/10.3102/00346543241230064)) ([hal-04500096](https://hal.science/hal-04500096)) <a href="/profan.html"><span class="label label-success">ProFAN</span></a>
 

## 2023

*  Johann Chevalère, Loreleï Cazenave, Robin Wollast, Mickaël Berthon, Ruben Martinez, Vincent Mazenod, Marie-Claude Borion, Delphine Pailler, Nicolas Rocher, Rémi Cadet, Catherine Lenne , Norbert Maïonchi-Pino, Pascal Huguet. **The influence of socioeconomic status, working memory and academic self-concept on academic achievement**. *European Journal of Psychology of Education, 2023, 38, pp.287-309*. ([10.1007/s10212-022-00599-9](https://dx.doi.org/10.1007/s10212-022-00599-9)). ([hal-03707470](https://hal.science/hal-03707470)) <a href="/ep3c.html"><span class="label label-success">eP3C</span></a>

## 2022

* Johann Chevalère, Loreleï Cazenave, Mickaël Berthon, Ruben Martinez, Vincent Mazenod, Marie-Claude Borion, Delphine Pailler, Nicolas Rocher, Rémi Cadet, Catherine Lenne, Norbert Maïonchi-Pino, Pascal Huguet. **Compensating the socioeconomic achievement gap with computer‐assisted instruction**. *Journal of Computer Assisted Learning, 2022, 38 (2), pp.366-378*. ⟨[10.1111/jcal.12616](https://dx.doi.org/10.1111/jcal.12616)⟩. ⟨[hal-03428446v2](https://hal.science/hal-03428446v2)⟩ <a href="/ep3c.html"><span class="label label-success">eP3C</span></a>


* Jm Monteil, A Séré, M Demolliens, P Huguet, A Batruch, M Bouet, M Bressan, P Bressoux, G Brown, F Butera, C Cepeda, A Cherbonnier, C Darnon, Al de Place, O Desrichard, T Ducros, L Goron, B Hemon, E Jamet, R Martinez, V Mazenod, N Mella , E Michinov, N Michinov, N Ofosu, P Pansu, L Peter, B Petitcollot, C Poletti, I Régner, M Riant, A Robert, O Rudmann, C Sanrey, A Stanczak, F Toumani, S Vilmin, E Visintin, E. Vives. **Rapport ProFAN Partager l’expertise. L’interdépendance positive, un levier pour de nouvelles compétences ?** *remis en Juin 2022 au Ministère de l’éducation, de la jeunesse et des sports et au Ministère de l’enseignement supérieur, de la recherche et de l’innovation*. ([hal-04021297](https://hal.science/hal-04021297)) <a href="/profan.html"><span class="label label-success">ProFAN</span></a>

## 2021

*  Yannick Miras , Jerry Lonlac, Beauger Aude, Benjamin Legrand , Delphine Latour , Karen K. Serieyssol , Marlène Lavrieux , Paul M Ledger, Vincent Mazenod , Jean-Luc Peiry, Engelbert Mephu Nguifo. **Tracking plant, fungal and algal diversity through a data mining approach: towards an improved analysis of holocene lake Aydat (Puyde-Dôme, France) dynamics and ecological legacies.** *Revue des Sciences Naturelles d'Auvergne, 2021*. ([hal-03547371](https://hal.science/hal-03547371)) <a href="/mobipaleo.html"><span class="label label-success">Mobipaleo</span></a>

* Johann Chevalère, Loreleï Cazenave, Mickaël Berthon, Ruben Martinez, Vincent Mazenod, Marie-Claude Borion, Delphine Pailler, Nicolas Rocher, Rémi Cadet, Catherine Lenne, Norbert Maïonchi-Pino, Pascal Huguet. **Computer-assisted instruction versus inquiry-based learning: The importance of working memory capacity**. *PLoS ONE 16(11): e0259664. [https://doi.org/10.1371/journal.pone.0259664](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0259664)* <a href="/ep3c.html"><span class="label label-success">eP3C</span></a>

*  Laurine Peter, Nicolas Michinov, Maud Besançon, Estelle Michinov, Jacques Juhel, Genavee Brown, Eric Jamet, Anthony Cherbonnier, Profan Consortium. **Revisiting the Effects of Gender Diversity in Small Groups on Divergent Thinking: A Large-Scale Study Using Synchronous Electronic Brainstorming**. *Frontiers in Psychology, 2021, pp.12:723235*. ⟨[10.3389/fpsyg.2021.723235](https://dx.doi.org/10.3389/fpsyg.2021.723235)⟩. ⟨[hal-03401056](https://hal.science/hal-03401056)⟩ <a href="/profan.html"><span class="label label-success">ProFAN</span></a>


* Arnaud Stanczak, Céline Darnon, Anaïs Robert, Marie Demolliens, Camille Sanrey, Pascal Bressoux, Pascal Huguet, Céline Buchs, Fabrizio Butera & <strike>consortium ProFAN</strike>. **Journal of Educational Psychology, 114(6), 1461–1476**. ([https://doi.org/10.1037/edu0000730](https://doi.org/10.1037/edu0000730)). -([hal-03441734](https://hal.science/hal-03441734)) <a href="/profan.html"><span class="label label-success">ProFAN</span></a>



* Mella N., Pansu P., Batruch A., Bouet M., Bressoux P., Brown G., Butera F., Cepeda C., Darnon C., Demolliens M., De Place AL, Ducros T., Goron L., Huguet P., Jamet E., Martinez R., Mazenod V., Michinov N., Peter L., Petitcollot B., Poletti C., Régner I., Riant M., Robert A., Sanrey C., Stanczak A., Toumani F., Visintin E., Vives E., Desrichard O.. **An exploratory network analysis of socio-emotional competencies, school adjustment and school performance in adolescence**. *Journal of Educational Psychology, Volume 12 - 2021* ([https://doi.org/10.3389/fpsyg.2021.640661](https://doi.org/10.3389/fpsyg.2021.640661.)) ([full article](https://www.frontiersin.org/journals/psychology/articles/10.3389/fpsyg.2021.640661/full)) <a href="/profan.html"><span class="label label-success">ProFAN</span></a>


## 2020

* Yannick Miras , Jerry Lonlac, Beauger Aude, Benjamin Legrand , Delphine Latour , Karen K. Serieyssol , Marlène Lavrieux , Paul M Ledger, Vincent Mazenod , Jean-Luc Peiry, Engelbert Mephu Nguifo. **Refining Holocene lake dynamics and detecting early human-induced ecological legacies: towards improved analysis of past biodiversity through a data mining approach** ([researchgate.net](https://www.researchgate.net/publication/341286528_Patterning_Holocene_lake_dynamics_and_detecting_early_Prehistoric_human_impacts_targets_of_an_improved_integration_of_multivariate_ecological_indicators_thanks_to_the_data_mining_approach)) <a href="/mobipaleo.html"><span class="label label-success">Mobipaleo</span></a>


## 2018

* Lonlac Konlac J., Miras Y., Beauger A., Mazenod V., Peiry J.L.. **An Approach for Extracting Frequent (Closed) Gradual Patterns Under Temporal Constraint**. *2018 IEEE International Conference on Fuzzy Systems (FUZZ-IEEE)*, 2018. ⟨[hal-01924145](https://hal.archives-ouvertes.fr/hal-01924145v1)⟩  <a href="/mobipaleo.html"><span class="label label-success">Mobipaleo</span></a>

* Ariane Tichit, Pascal Lafourcade, Vincent Mazenod. **Les monnaies virtuelles décentralisées sont-elles des dispositifs d'avenir ?** *Political Economy, Association d’Économie Politique*, 2018. ⟨[hal-01995938](https://hal.archives-ouvertes.fr/hal-01995938v1)⟩

## 2017

* Ariane Tichit, Pascal Lafourcade, Vincent Mazenod . **Les monnaies virtuelles décentralisées sont-elles des outils d’avenir ?**. ⟨[halshs-01467329](https://hal.archives-ouvertes.fr/halshs-01467329v1)⟩
