Title: <i class="fa fa-briefcase" aria-hidden="true"></i> &agrave; propos
Date: 2021-03-09 10:27
Category: <i class="fa fa-briefcase" aria-hidden="true"></i> &agrave; propos
slug: index
lang: fr
save_as: index.html
url: index.html
Number: 2

## <i class="fas fa-desktop"></i> Ingénieur de Recherche Informatique au CNRS <a class="expand btn btn-primary float-right">version courte</a>

### <i class="fas fa-quote-left"></i>You can't defend. You can't prevent. The only thing you can do is detect and respond.<i class="fas fa-quote-right"></i> <span style="float:right;font-size: smaller; padding-top: 15px;">*Bruce Schneier*</span>


<hr />

<center>

#### En poste au [LIMOS](htttps://limos.fr), UMR ([CNRS](https://www.cnrs.fr)/[UCA](https://uca.fr)/[EMSE](https://www.mines-stetienne.fr/)), hébergée par l'[ISIMA](htttps://isima.fr)

</center>

<hr />

* Je supervise <a name="cri" class="toggle"><i class="fas fa-users-cog"></i> le service informatique commun CRI ISIMA / LIMOS  (9 personnes)</a> 
    * <i id="cri" class="fas fa-users-cog"></i> [service informatique commun CRI ISIMA / LIMOS](https://doc.isima.fr/support/cri)
        * composé de 
            * 4 ingénieurs 
            * 1 assistant ingénieur
            * 3 techniciens
            * 1 CDD
        * animation et gestion de l'équipe 
            * blog du [CRI ISIMA / LIMOS](https://cri.isima.fr)
        * suivi de tous les projets de l'équipe
            * infrastructure (matériels et réseaux)
        * gestion du budget
        * interlocuteur [UCA](https://uca.fr) / [INP](https://www.clermont-auvergne-inp.fr/)
        
<hr />

* J'industrialise et gère l'exploitation d'un <a name="cloud" class="toggle"><i class="fas fa-cloud-upload-alt"></i> cloud souverain</a> (un vrai!) et l'administration de différents <a name="server" class="toggle"><i class="fas fa-server"></i> services</a>
    * <i id="cloud" class="fas fa-cloud-upload-alt"></i> exploitation de notre cloud (vraiment!) souverain
        * proxmox
        * <strike>ansible</strike>
        * <strike>vault</strike>
        * <strike>consul</strike>
        * <strike>terraform</strike>
        * <strike>packer</strike>
        * pour différentes raisons cette pile logicielle est appelée à évoluer dans les prochaines années
    * <i id="server" class="fas fa-server"></i> administration de services
        * [My](https://my.isima.fr) un développement [<i class="fab fa-symfony"></i> symfony 6](https://symfony.com/), métier, permettant de gérer une partie des systèmes d'information de l'ISIMA et du LIMOS
        * [gestionnaire de ticket](https://support.isima.fr/) pour le support ([gestsup](https://gestsup.fr/))
        * hébergement web mututalisé ([ispconfig](https://www.ispconfig.org/))
        * forges logicielles, l'une [orientée recherche](https://gitlab.limos.fr), l'autre [orientée pédagogie](https://gitlab.isima.fr) ([gitlab](https://about.gitlab.com)
        * [partage de notes](https://hedgedoc.isima.fr) ([hedgedoc](https://hedgedoc.org)/[codimd](https://github.com/hackmdio/codimd))
        * clavardage avec ([mattermost](https://mattermost.com/))
        * sauvegarde et restauration ([backuppc](https://backuppc.github.io/backuppc/))
        * documentations collaboratives, l'une [à destination des utilisateurs](https://doc.isima.fr/), l'autre à destination des administrateurs ([mkdocs](https://www.mkdocs.org/))
        * système de page perso ([apache](https://httpd.apache.org/docs/2.4/howto/public_html.html))
<hr />

* Je gère différents <a name="projects" class="toggle"><i class="fas fa-flask"></i> projets recherche</a>
    * <i id="projects" class="fas fa-flask"></i> projets recherche
        
        * [ProFan-Transfert](/profan-transfert.html) <i class="fas fa-spinner fa-spin"></i>
        * [ECCIPE](/profan-transfert.html) <i class="fas fa-spinner fa-spin"></i>
        * [ARBORéa](/arborea.html) <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
        * [Stam](/stam.html) <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
        * [ep3c](/ep3c.html) <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
        * [Profan](/profan.html) <i class="fas fa-check-circle text-success" aria-hidden='true'></i>
        * [mobipaléo](/mobipaleo.html) <i class="fas fa-spinner fa-spin"></i>
        * [CEPPPIA](/cepppia.html) <i class="fas fa-check-circle text-success" aria-hidden='true'></i>

    * Pour ces projets recherche, j'encadre 4 développeurs, et interviens tout au long du <a name="projects-tasks" class="toggle"></i><i class="fas fa-tasks"></i> cycle de vie des projets
        * <i id="projects-tasks" class="fas fa-tasks"></i> cycle de vie des projets
            * défintion des besoins
            * défintion de l'architecture 
            * recrutement et animation d'un (ou d'une équipe de) développeur(s)
            * gestion de l'hébergement et du déploiement
            * expertises techniques
            * suivi de projets
            * revue de code et recette
            * mise en place des référentiels légaux (RGPD, données de santé, ...)
            * mise à disposition des données (entrepôt)
<hr />

* Je participe au développement de projets, recherche ou interne à l'équipe, avec différentes <a name="techno" class="toggle"><i class="fas fa-code-branch"></i> technologies</a>
    * <i id="techno" class="fas fa-code-branch"></i> développement
        * <i class="fab fa-php"></i> php / <i class="fab fa-symfony"></i> symfony
        * <i class="fab fa-python"></i> python
        * <i class="fas fa-terminal"></i> bash
        * <i class="fab fa-js"></i> javascript
        * <i class="fab fa-html5"></i> html
        * <i class="fab fa-css3-alt"></i> css
        

<hr />

* Je suis impliqué dans la chaîne fonctionnelle <a name="ssi" class="toggle"><i class="fas fa-shield-alt"></i> SSI</a>
    * <i id="ssi" class="fas fa-shield-alt"></i> SSI
        * correspondant Sécurité des Systèmes d'Informations (CSSI) pour le [LIMOS](htttps://limos.fr) et l'[ISIMA](htttps://isima.fr)
        * membre de la CRSSI de [DR7 à Lyon](https://www.rhone-auvergne.cnrs.fr/)
        * membre de la Cellule Audit & Veille SSI à l'[UCA](https://uca.fr)
        * pilotage de la politique de chiffrement et anti-virus au [LIMOS](https://limos.fr) et à l'[ISIMA](htttps://isima.fr)
<hr />

* Je donne aussi quelques <a name="school" class="toggle"><i class="fas fa-graduation-cap"></i> cours</a> à l'[ISIMA](htttps://isima.fr)
    * <i id="school" class="fas fa-graduation-cap"></i> cours à l'[ISIMA](htttps://isima.fr)
        * <i class="fas fa-shield-alt"></i> [Sécurité des application Web](/zz2-f5-securite-logicielle-securite-des-applications-web.html)
        * <strike><i class="fas fa-sad-cry"></i>  [Privacy & Cryptologie](/zz2-f5-privacy-crypto.html)</strike>
        * <i class="fas fa-user-secret"></i> [Ethique & informatique: Je n'ai rien à cacher](/zz3-je-nai-rien-a-cacher.html)
        * <i class="fas fa-history"></i> Histoire de l'informatique <i class="fas fa-spinner fa-spin"></i>
<hr />

* J'interviens dans différents contextes sur <a name="talks" class="toggle"><i class="fas fa-comment"></i> diverses thématiques</a>
    * <i id="talks" class="fas fa-comment"></i> interventions
        * 08/10/2024 pour les [JoSy Proxmlox Virtual Environment (PVE)](https://indico.mathrice.fr/event/542/overview): Terraform, Devopx, PVE <span class="label label-warning">à venir</span>
        * 07/06/2022 pour les [journées Proxmox VE / Ceph 2022](https://indico.mathrice.fr/event/327/): [Proxmox VE & DevOps](https://webtv.uca.fr/video/c82cf242-bbb0-427a-b5d6-b44c700f9409?time=4h27m12s)
        * 15/01/2021 pour [La Montagne](https://www.lamontagne.fr/): [Les cyberattaques, retour sur ce virus qui touche les organismes de santé en France en plein Covid-19](https://www.lamontagne.fr/paris-75000/actualites/les-cyberattaques-retour-sur-ce-virus-qui-touche-les-organismes-de-sante-en-france-en-plein-covid-19_13900627/)
        * 27/11/2020 pour [Subatech](http://www-subatech.in2p3.fr/fr/): [sécurité et navigateur](/slides/privacy/browser.html)
        * 01/12/2020 pour [Le GRIN](https://legrin.fr/): [ÉPIDÉMIE(S)#25 - Peut-on encore compter sur le progrès?](https://podcast.ausha.co/epidemie-s/epidemie-s-25-peut-on-encore-compter-sur-le-progres) (avec [Yves Caseau](https://www.linkedin.com/in/ycaseau/))
        * 23/05/2019 pour [Clermont'ech](https://www.clermontech.org): [vault](https://www.clermontech.org/talks/api-hour-42/Vincent-Mazenod-Vault.html)
        * 18/12/2108 pour [le lycée Sidoine Apollinaire](https://www.lycee-sidoine-apollinaire.fr/): [Je n'ai rien à cacher / RGPD](/privacy/jnarac.html)
        * 15/05/2018 pour [pint of science](/slides/privacy/surfer_couvert.html): [surfez couvert!](/slides/privacy/surfer_couvert.html)
        * 11/03/2018 pour [l'esc Clermont](https://www.esc-clermont.fr/) : [Conférence – table ronde : Monnaies virtuelles, Initial Coin Offerings (ICOs) et Blockchain : Innovations en finance majeures ?](https://www.esc-clermont.fr/conference-table-ronde-monnaies-virtuelles-initial-coin-offerings-icos-et-blockchain-innovations-en-finance-majeures/)
        * 30/09/2017 pour [Clermont'ech](https://www.clermontech.org): [mets ta cagoule, ça va hacker](https://www.youtube.com/watch?v=oOGuGn2qWhk)
        * 16/05/2017 pour [La Montagne](https://www.lamontagne.fr/): [Interview dans La Montagne au sujet du ransomware WannaCry](/images/presse/wannacry.jpg)
        * 13/04/2017 pour [ARAMIS](https://aramis.resinfo.org): [Dev & Ops : les outils du dialogue - 9ème journée Aramis 2017](https://webcast.in2p3.fr/video/bonnes_pratiques_en_developpement_web)
        * 23/03/2017 pour [le lycée de chamalières](https://www.lyceedechamalieres.fr/): [Je n'ai rien à cacher / RGPD](/privacy/jnarac.html)
        * 07/07/2016 pour [Clermont'ech](https://www.clermontech.org): [Drupalgeddon](https://www.clermontech.org/talks/api-hour-21/3-vincent-mazenod-drupalgeddon.html)
        * 19/04/2016 pour [la Mairie des Martres de Veyre](http://www.mairie-lesmartresdeveyre.fr/): [Les enjeux de confidentialité et sécurité bien exposés par Vincent Mazenod](https://www.lamontagne.fr/martres-de-veyre-63730/actualites/les-enjeux-de-confidentialite-et-securite-bien-exposes-par-vincent-mazenod_11877490/)
        * 12/03/2015 pour [AUDACeS](http://audaces.asso.st/): [Séminaire technique: Sécurité Logicielle Web (avec Thomas LALLART)](https://indico.in2p3.fr/event/11263/)
        
        <!-- * 16/12/2014 pour [Clermont'ech](https://www.clermontech.org): [la PSSI de la nation](https://www.youtube.com/watch?v=jdWDdY2IdEw) -->
<hr />

* Je suis membre actif de l'association <a name="audaces" class="toggle"><i class="far fa-file-code"></i></a> [AuDACES, réseau régional de DevLOG](http://audaces.asso.st/) depuis 2015 et j'ai été coordinateur de ce réseau de 2018 à 2023
    * <i id="audaces" class="far fa-file-code"></i> événements
        * co-organisateur des [journées Proxmox VE / Ceph 2022](https://indico.mathrice.fr/event/327/)
        * organisateur de 3 journées Kubernete du 06 au 08 février 2019