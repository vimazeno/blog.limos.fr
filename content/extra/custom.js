$( document ).ready(function() {
    
    $('a.expand').click(function(event) {

        if($(this).text() == 'version longue') {
            $(this).text('version courte');
            $('a.toggle').each(function() {
                $("#" + $(this).attr('name')).closest('li').show();        
            });
        }
        else {
            $(this).text('version longue');
            $('a.toggle').each(function() {
                $("#" + $(this).attr('name')).closest('li').hide();        
            });
        }

    });

    $('a.toggle').click(function() {
        $("#" + $(this).attr('name')).closest('li').toggle();
    });

    $('a.customizable').click(function() {
        if($('#username').val() != "") {
            $(this).attr("href", $(this).attr("href") + "?" + $('#username').val());
        }
    });

    

});