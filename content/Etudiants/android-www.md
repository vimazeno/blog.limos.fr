Title: android-www
Date: 2017-09-20 10:30
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, android
Status: draft

## Contexte

L'idée première est de spoofer la page de login d'un Hotspot WIFI, c'est à dire de servir à partir d'un téléphone Android une page web imitant en tout point celle d'un hotspot wifi afin de collecter les identifiants des utilisateurs à proximité qui s'y connecteront.

Ce projet consiste essentiellement à configurer un serveur web sur un téléphone android qui soit accessible à partir d'un périphérique à proximité.

## Rendu attendu

Vous devez rendre un dépot git avec au minimum

* un code source implémentant les fonctionnalités demandées
* un apk permettant de déployer le service
* un fichier `README.md` documentant l'installtion et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [Android](https://fr.wikipedia.org/wiki/Android)

## Points à considérer

* la faisabilité d'un tel projet
* les pré-requis
    * root
    * librairies
* la sécurité
* la facilité de déploiment de l'application
* les possibilités d'interactions
    * possibilité de technologies server-side
    * bases de données ...

## Liens

* https://gbatemp.net/threads/android-app-for-self-hosting-exploit-5-5-1.425496/
