Title: ZZ3 - Ethique du numérique
Date: 2020-10-22 08:00
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: cours, éthique

[TOC]

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">FYI</h3>
  </div>
  <div class="panel-body">
    <ul>
      <li>Tous les slides sont fait avec <a href="https://github.com/hakimel/reveal.js">reveal.js</a>
        <ul>
          <li>ils sont exportables en pdf en ajoutant <code>?print-pdf#</code> à l'url (à coller juste après le <code>.html</code>) et en passant par l'impression dans un fichier du navigateur chrome ou (mieux) <a href="https://www.chromium.org/">chromium</a>
            <ul>
              <li>plus de détails sur l'<a href="https://github.com/hakimel/reveal.js/#pdf-export">export PDF de reveal</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

### une contre histoire de l'utopie numérique

* [<i class="fas fa-desktop"></i> une contre histoire de l'utopie numérique](slides/privacy/chun.html)


### Je n'ai rien à cacher

* [<i class="fas fa-desktop"></i> je n'ai rien à cacher](slides/privacy/jnarac2.html)

### Liens

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Pour acheter des livres ...</h3>
  </div>
  <div class="panel-body">
    plutôt que d'acheter sur amazon pensez local et commandez via le site <a href="https://www.librairielesvolcans.com/">de la scop des volcans</a>:
    <ul>
      <li>c'est le même prix</li>
      <li>ils peuvent tout obtenir dans un délais raisonnable en commandant à partir du site</li>
      <li>ils vous préviennent par mail ou SMS quand la commande est arrivée</li>
      <li>il faut juste aller chercher la commande chez des gens qui ont le sourire, et qui sont les <a href="https://www.librairielesvolcans.com/l-histoire-de-la-scop/ssh-4823">actionnaires de la boutique pour la quelle ils travaillent</a></li>
    </ul>
  </div>
</div>

#### Novlangue

* [<i class="fab fa-wikipedia-w"></i> Novlangue](https://fr.wikipedia.org/wiki/Novlangue)
* [<i class="fas fa-newspaper"></i> Quand la "vidéoprotection" remplace la "vidéosurveillance" (*Le monde*)](https://www.lemonde.fr/societe/article/2010/02/16/quand-la-videoprotection-remplace-la-videosurveillance_1306876_3224.html)
* [<i class="fab fa-youtube"></i> sur la manipulation des mots (*Franck Lepage*)](https://www.youtube.com/watch?v=mD-G5lHXviY)
* [<i class="fab fa-youtube"></i> Le mot qui a remplacé "Hiérarchie" (*Franck Lepage*)](https://www.youtube.com/watch?v=IUB1XsT5IQU)
* [<i class="fab fa-youtube"></i> La langue de bois décryptée avec humour ! (*Franck Lepage*)](https://www.youtube.com/watch?v=oNJo-E4MEk8)

#### Histoire du numérique

* [<i class="fas fa-newspaper"></i> Chronologie du réseau Internet](http://www.tiki-toki.com/timeline/embed/137139/6372410394/#vars!date=1954-07-27_20:20:56!)
* [<i class="fas fa-newspaper"></i> Une histoire d'Internet (*Laurent Chemla*)](http://www.chemla.org/textes/hinternet.html)
* [<i class="fas fa-newspaper"></i> Déclaration d’indépendance du Cyberespace (*John P. Barlow*)](http://editions-hache.com/essais/barlow/barlow2.html)
* [<i class="fas fa-newspaper"></i> Une nouvelle déclaration d’indépendance du cyberespace (*Libération*)](http://www.liberation.fr/amphtml/debats/2018/02/09/une-nouvelle-declaration-d-independance-du-cyberespace_1628377)
* [<i class="fas fa-podcast"></i> Une histoire de… l'Internet (*Julien Le Bot*)](https://www.radiofrance.fr/franceculture/podcasts/serie-une-histoire-de-l-internet)
* [<i class="fas fa-book"></i> Aux sources de l'utopie numérique (*Fred turner*)](http://cfeditions.com/Turner/)
* [<i class="fas fa-book"></i> Les Rêves cybernétiques de Norbert Wiener(*Pierre Cassou-Noguès*)](https://www.seuil.com/ouvrage/les-reves-cybernetiques-de-norbert-wiener-pierre-cassou-nogues/9782021090284)
* [<i class="fas fa-book"></i> Internet, année zéro (*Jonathan Bourguignon*)](https://www.editionsdivergences.com/livre/internet-annee-zero)
* [<i class="fas fa-book"></i> L'utopie déchue (*Félix Tréguer*)](https://www.fayard.fr/sciences-humaines/lutopie-dechue-9782213710044)
* [<i class="fas fa-book"></i> Cyberstructure (*Stéphane Bortzmeyer*)](https://cfeditions.com/cyberstructure/)
* [<i class="fab fa-youtube"></i> Jurassic Web, Une préhistoire des réseaux sociaux](https://www.youtube.com/watch?v=mLEbi2MAoL8)
* [<i class="fab fa-youtube"></i> Une contre histoire de l'Internet](https://www.youtube.com/watch?v=MUTABXD8f24)
* [<i class="fab fa-youtube"></i> The Net: The Unabomber, LSD & the Internet](https://www.youtube.com/watch?v=GY6fb59XFbQ)
* [<i class="fab fa-youtube"></i> Comprendre ce que représente le mythe de la « frontière électronique » (*Stéphanie Ouillon*)](https://video.passageenseine.fr/videos/watch/555339ff-ef64-4e18-af40-6bd79b78008b)
* [<i class="fab fa-youtube"></i> Halt and Catch Fire (série télévisée)](https://fr.wikipedia.org/wiki/Halt_and_Catch_Fire_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e))
* [<i class="fab fa-youtube"></i> Internet VS minitel 2.0 (*Benjamin Bayard*)](https://cfeditions.com/cyberstructure/)

#### Souveraineté

* [<i class="fas fa-newspaper"></i> Code is law / le code fait loi (*Lawrence Lessig*)](https://framablog.org/2010/05/22/code-is-law-lessig/)
* [<i class="fas fa-book"></i> Richard Stallman et la révolution du logiciel libre - Une biographie autorisée (*Sam Williams, Richard Stallman &Christophe Masutti*)](https://framabook.org/docs/stallman/framabook6_stallman_v1_gnu-fdl.pdf)
* [<i class="fab fa-youtube"></i> L'éthique des hackers (*Steven Levy*)](https://www.editions-globe.com/lethique-des-hackers/)
* [<i class="fas fa-film"></i> The Internet's Own Boy - l'histoire d'Aaron Schwartz](https://www.youtube.com/watch?v=7ZBe1VFy0gc)

#### Microtravail

* [<i class="fab fa-youtube"></i> Invisibles: Les travailleurs du clic](https://www.france.tv/slash/invisibles/saison-1/)

#### Intelligence artificielle

* [<i class="fab fa-youtube"></i> L'asservissement par l'Intelligence Artificielle ? (*Éric Sadin chez ThinkerView*)](https://www.youtube.com/watch?v=VzeOnBRzDik)
* [<i class="fab fa-youtube"></i> Conférence ChatGPT : Une évolution technique majeure (*Vincent Guigue*)](https://webtv.uca.fr/video/2c0d336a-eb51-4e22-9415-0da2ef0eb3a6)
* [<i class="fab fa-youtube"></i> De quoi ChatGPT est-il VRAIMENT capable ? (*Monsieur Phi*)](https://www.youtube.com/watch?v=R2fjRbc9Sa0)

#### Big Tech

* [<i class="fas fa-book"></i> Le mythe de l'entrepreneur: Défaire l'imaginaire de la Silicon Valley (*Anthony Galluzzo*)](https://www.editionsladecouverte.fr/le_mythe_de_l_entrepreneur-9782355221972)
* [<i class="fas fa-book"></i> Steve Jobs (*Walter Isaacson*)](https://fr.wikipedia.org/wiki/Steve_Jobs_(livre))
* [<i class="fas fa-film"></i> Steve Jobs (Danny Boyle)](https://fr.wikipedia.org/wiki/Steve_Jobs_(film))
* [<i class="fas fa-film"></i> Silicon Valley (série télévisée)](https://fr.wikipedia.org/wiki/Silicon_Valley_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e))


#### Surveillance

* [<i class="fab fa-wikipedia-w"></i> Fichage en france](https://fr.wikipedia.org/wiki/Fichage_en_France)
* [<i class="fas fa-newspaper"></i> Loi de Programmation Militaire 2014-2019 :  Eléments d’évaluation du risque législatif lié à l’article 13](http://ifrei.org/tiki-download_file.php?fileId=59)
* [<i class="fab fa-youtube"></i> Tous surveillés - 7 milliards de suspects !](https://www.arte.tv/fr/videos/083310-000-A/tous-surveilles-7-milliards-de-suspects/)
* [<i class="fab fa-youtube"></i> Rien à cacher](https://www.youtube.com/watch?v=djbwzEIv7gE)
* [<i class="fas fa-film"></i> Citizen four - (*Laura Poitras*)](https://www.youtube.com/watch?v=ABBPRd7ARYM)
* [<i class="fas fa-film"></i> Le cinquième pouvoir](https://fr.wikipedia.org/wiki/Le_Cinqui%C3%A8me_Pouvoir)
* [<i class="fas fa-film"></i> Underground : L'Histoire de Julian Assange](https://fr.wikipedia.org/wiki/Underground_:_L%27Histoire_de_Julian_Assange)
* [<i class="fas fa-film"></i> Snowden](https://fr.wikipedia.org/wiki/Snowden_(film))
* [<i class="fas fa-book"></i> surveillance:// (*Tristan Nitot*)](http://standblog.org/blog/pages/Surveillance)
* [<i class="fas fa-book"></i> Attentifs ensemble! (*Jérôme Thorel*)](http://www.editionsladecouverte.fr/catalogue/index-Attentifs_ensemble__-9782707174215.html)
* [<i class="fas fa-book"></i> Menaces sur nos libertés (*Julian Assange / Jacob Appelbaum / Müller-Maguhn / Jérémie Zimmermann*)](https://korben.info/menace-sur-nos-libertes-assange.html)
* [<i class="fas fa-book"></i> Guide d'autodéfense numérique](https://guide.boum.org/)
* [<i class="fas fa-book"></i> Mémoires Vives (*Edward Snowden*)](https://fr.wikipedia.org/wiki/M%C3%A9moires_vives_(livre))
* [<i class="fas fa-book"></i> L’Âge du capitalisme de surveillance (*Shoshana Zuboff*)](https://www.zulma.fr/livre/lage-du-capitalisme-de-surveillance/)
* [<i class="fas fa-book"></i> Le bug humain (*Sébastien Bohler*)](https://fr.wikipedia.org/wiki/Le_Bug_humain)

#### Piraterie

* [<i class="fab fa-youtube"></i> TPB AFK: The Pirate Bay Away From Keyboard](https://www.youtube.com/watch?v=eTOKXCEwo_8)

#### Sécurité

* [<i class="fas fa-university"></i> ANSSI](https://www.ssi.gouv.fr/)
* [<i class="fab fa-youtube"></i> Les marchands de peur / La bande à Bauer et l'idéologie sécuritaire (*Mathieu Rigouste*)](https://www.editionslibertalia.com/catalogue/a-boulets-rouges/Les-Marchands-de-peur)
* [<i class="fas fa-film"></i> Cybertraque](https://fr.wikipedia.org/wiki/Cybertraque_(film))


#### Société

* [<i class="fab fa-wikipedia-w"></i> Luddisme](https://fr.wikipedia.org/wiki/Luddisme)
* [<i class="fab fa-wikipedia-w"></i> Comité pour la liquidation ou la destruction des ordinateurs (CLODO)](https://fr.wikipedia.org/wiki/Comit%C3%A9_pour_la_liquidation_ou_la_destruction_des_ordinateurs)
* [<i class="fab fa-wikipedia-w"></i> Jello Biafra](https://fr.wikipedia.org/wiki/Jello_Biafra)
* [<i class="fas fa-book"></i> Cybernétique et société (*Norbert wiener*)](http://www.seuil.com/ouvrage/cybernetique-et-societe-norbert-wiener/9782757842782)
* [<i class="fas fa-book"></i> Le cimetière de Prague (*Umbero Eco*)](https://www.grasset.fr/livres/le-cimetiere-de-prague-9782246783893)
* [<i class="fas fa-book"></i> La société du risque (*Ulrich Beck*)](https://editions.flammarion.com/Catalogue/champs-essais/philosophie/la-societe-du-risque)
* [<i class="fas fa-book"></i> Surveiller et punir (*Michel Foucault*)](https://fr.wikipedia.org/wiki/Surveiller_et_punir)
* [<i class="fas fa-book"></i> Sapiens : Une brève histoire de l'humanité (*Yuval Noah Harari*)](https://www.albin-michel.fr/ouvrages/sapiens-edition-limitee-9782226445506)


### Evaluation

* Questionnaire papier à remplir en fin de cours