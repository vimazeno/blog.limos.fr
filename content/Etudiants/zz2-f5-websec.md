Title: ZZ2 F5 - Securité logicielle - sécurité des applications web
Date: 2024-09-03 10:55
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: cours

[TOC]

## 📚 Supports de cours

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">😎 Personnaliser les slides</h3>
  </div>
  <div class="panel-body">
    en entrant votre nom d'utilisateur uca ici 
    <input type="text" placeholder="username" name="username" id="username" />, 
    tous les références à la VM perso (la vulnérable) dans les slides ci-après, seront <strong>personnalisées avec le fqdn de votre VM</strong>, vous permettant d'appliquer les attaques directement depuis les slides
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">💡 Version PDF</h3>
  </div>
  <div class="panel-body">
    <ul>
      <li>Tous les slides sont fait avec <a href="https://github.com/hakimel/reveal.js">reveal.js</a>
        <ul>
          <li>ils sont exportables en pdf en ajoutant <code>?print-pdf#</code> à l'url (à coller juste après le <code>.html</code>) et en passant par l'impression dans un fichier du navigateur chrome ou (mieux) <a href="https://www.chromium.org/">chromium</a>
            <ul>
              <li>plus de détails sur l'<a href="https://github.com/hakimel/reveal.js/#pdf-export">export PDF de reveal</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">💪 Contributions</h3>
  </div>
  <div class="panel-body">
    <ul>
      <li>
        n'hésitez pas à me signaler des liens morts et / ou à en proposer de nouveaux via une <a href="https://gitlab.isima.fr/vimazeno/blog.limos.fr/-/issues">issue</a> ou directement via un <a href="https://gitlab.isima.fr/vimazeno/blog.limos.fr/-/merge_requests">PR/MR</a>
      </li>
    </ul>
  </div>
</div>


### Contexte

<ul>
  <li>
    <a href="slides/1337/lab.html" 
      class="customizable">
      Lab
    </a>
  </li>
  <!-- li>
    <a href="slides/privacy/sovereignty.html" 
      class="customizable">
      Souveraineté
    </a>
  </li>
  <li>
    <a href="slides/privacy/passwords.html"
      class="customizable">
      Mots de passes
    </a>
  </li -->
  <li>
    <a href="slides/1337/http.html"
      class="customizable">
      HTTP
    </a>
  </li>
  <li>
    <a href="slides/1337/browser.html"
      class="customizable">
      Browser
    </a>
  </li>
  <li>
    <a href="slides/1337/js.html"
      class="customizable">
      Javascript
    </a>
  </li>
  <li>
    <a href="slides/privacy/tls.html#/0/52"
      class="customizable">
      HTTPS
    </a>
    <ul>
      <li>
        <a href="slides/1337/heartbleed.html"
          class="customizable">
          Heartbleed
        </a>
      </li>
    </ul>
  </li>
  <!-- li>
    <a href="slides/privacy/tracking.html"
      class="customizable">
      Tracking
    </a>
  </li -->
</ul>

### Vulnérabilités communes

<ul>
  <li>
    <a href="slides/1337/authentication.html"
      class="customizable">
        Authentification
    </a>
    <ul>
      <li>
        <a href="slides/1337/bruteforce.html"
          class="customizable">
          Brute force
        </a>
      </li>
      <li>
        <a href="slides/1337/session.html"
          class="customizable">
          Session
        </a>
      </li>
    </ul>
  </li>
  <li>
    <a href="slides/1337/cmdi.html"
      class="customizable">
      Command injection
    </a>
    <ul>
      <li>
        <a href="slides/1337/shellshock.html"
          class="customizable">
          Shellshock
        </a>
      </li>
    </ul>
  </li>
  <li>
    <a href="slides/1337/fi.html"
      class="customizable">
        File inclusion
    </a>
  </li>
  <li>
    <a href="slides/1337/upload.html"
      class="customizable">
      Upload
    </a>
  </li>
  <li>
    <a href="slides/1337/xss.html"
      class="customizable">
      XSS
    </a>
    <ul>
      <li>
        <a href="slides/1337/csp.html"
          class="customizable">
          CSP
        </a>  
      </li>
      <li>
        <a href="slides/1337/sop.html"
          class="customizable">
          SOP/CORS
        </a>  
      </li>
    </ul>
  </li>
  <li>
    <a href="slides/1337/csrf.html"
      class="customizable">
      CSRF
    </a>
    <!-- ul>
      <li>
        <a href="slides/1337/captcha.html"
          class="customizable">
          Recaptcha <i class="fas fa-hammer"></i>
        </a>
      </li>
    </ul -->
  </li>
  <li>
    <a href="slides/1337/sqli.html"
      class="customizable">
      SQLi
    </a>
    <ul>
      <li>
        <a href="slides/1337/drupalgeddon.html"
          class="customizable">
          Drupalgeddon
        </a>
      </li>
    </ul>
  </li>
</ul>

<!--
### Pentesting

* [Collecter](slides/1337/gathering.html)
* [Détecter](slides/1337/detecting.html)

### Se protéger

* [Top10](slides/1337/top10.html)
* [anticiper](slides/1337/anticiper.html)
-->

<hr />

## 📂 présentation des VMs

* **Kali** est notre VM offensive, son OS est [Kali 2](https://www.kali.org) et possède de nombreux outils pour la sécurité informatique.
    * C'est à partir de cette VM que vous suivrez le cours et lancerez toutes les attaques
      * nom d'utilisateur: kali
      * mot de passe: kali

* **DVWA** est la VM qui héberge l'application vulnérable [DVWA (Damn Vulnerable Web Application)](https://github.com/digininja/DVWA).
    * C'est sur cette VM que nous allons tester toutes nos attaques
    * nom d'utilisateur: kali
    * mot de passe: kali
    * nom d'utilisateur DVWA: admin
    * mot de passe DVWA: password

* **debian** héberge la vulnérabilité drupalgeddon présentée en fin de cycle de cours
    * nom d'utilisateur: kali
    * mot de passe: kali

* **proxy** héberge la vulnérabilité heratbleed présenté en début de cours
    * nom d'utilisateur: kali
    * mot de passe: kali

* **debian11** est une fresh install de debian 11, qui vous permettra d'installer DVWA 
    * nom d'utilisateur: kali
    * mot de passe: kali
    * installer DVWA en une ligne `wget -O - https://shorturl.at/xE2qF | bash`

* N.B. [https://perso.limos.fr/mazenod/slides/1337/exploits](https://perso.limos.fr/mazenod/slides/1337/exploits) contient des scripts malicieux que vous utiliserez également pour certaines attaques

Vous trouverez ci après comment redéployer tout cela sur n'importe quelle machine

<hr />

## 💻 Recréer l'environnement de cours dans VirtualBox

* testé avec [VirtualBox 7.0](https://www.virtualbox.org/wiki/Downloads) sous [Ubuntu jammy](http://releases.ubuntu.com/jammy/)

### Créer un réseau NAT

```bash
vboxmanage  natnetwork add --netname natwebsec --network "172.16.76.0/24" --enable --dhcp off
```

### Télécharger les images OVA

voir [https://owncloud.isima.fr/s/Gf2KNZKVWQVxEpa](https://owncloud.isima.fr/s/Gf2KNZKVWQVxEpa)

* kali.ova
* dvwa.ova
* debian.ova
* proxy.ova
* debian11

<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">📢 FYI</h3>
  </div>
  <div class="panel-body">
    il y a environ 7 Go d'images, n'hésitez pas à vous les faire passer via des clés USB
  </div>
</div>

### Importer les images OVA

```bash
vboxmanage import kali.ova
vboxmanage import dvwa.ova
vboxmanage import debian.ova
vboxmanage import proxy.ova
vboxmanage import debian11.ova
```

### Configurer le réseau pour chaque vm

```bash
vboxmanage modifyvm kali --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm dvwa --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm debian --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm proxy --nic1 natnetwork --nat-network1 natwebsec
vboxmanage modifyvm debian11 --nic1 natnetwork --nat-network1 natwebsec
```

![réseau vm](images/etudiants/vm-network.png)

### (fix) En cas de réseau injoignable sur proxy

si

```bash
ping 172.16.76.145 # ping sur kali
```

renvoie

```bash
connect: Network is unreachable
```

vérifier le numéro de votre interface réseau

```bash
student@proxy:~$ ifconfig -a

eth2      Link encap:Ethernet  HWaddr 08:00:27:ae:b5:20
          inet adr:172.16.76.143  Bcast:172.16.76.255  Masque:255.255.255.0
          adr inet6: fe80::a00:27ff:feae:b520/64 Scope:Lien
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          Packets reçus:24 erreurs:0 :0 overruns:0 frame:0
          TX packets:32 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 lg file transmission:1000
          Octets reçus:4789 (4.7 KB) Octets transmis:4679 (4.6 KB)

lo        Link encap:Boucle locale
          inet adr:127.0.0.1  Masque:255.0.0.0
          adr inet6: ::1/128 Scope:Hôte
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          Packets reçus:54 erreurs:0 :0 overruns:0 frame:0
          TX packets:54 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 lg file transmission:0
          Octets reçus:4076 (4.0 KB) Octets transmis:4076 (4.0 KB)
```

par exemple ce numéro peut être eth2 (comme ci dessus) au lieu de eth0

il faut alors modifier le fichier /etc/network/interfaces en fonction

```bash
student@proxy:~$ sudo vi /etc/network/interfaces

# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth2
iface eth2 inet static
        address 172.16.76.143
        netmask 255.255.255.0
        gateway 172.16.76.1
```

puis activer l'interface réseau

```bash
student@proxy:~$ sudo ifup eth2
```

réessayer

```bash
ping 172.16.76.145 # ping sur kali
```

Ce bug est dû à la numérotation fantaisiste d'Ubuntu des interfaces réseau ...


### Liste des vms / noms de domaine (/etc/hosts)

```
# SecLab

## proxy
172.16.76.143 proxy secured heart.bleed fo.ol #proxied version of dum.my

# debian
172.16.76.144 debian good.one go.od targ.et mutillid.ae d.oc dum.my spip sp.ip
172.16.76.144 drup.al hackable-drupal.com drupal wordpre.ss bl.og wp wordpress

# kali
172.16.76.145 kali bad.guy hack.er 1337.net

# dvwa
172.16.76.146 dvwa dvwa.com dv.wa

# debian11
172.16.76.147 debian11

# host
172.16.76.1   us.er
```

## 🎓 Evaluation

### 📝 Examen écrit en fin de session (10 points)

* Durée: 1h00
* Support: interdit

### 📢 Présentation de groupe au début de chaque cours (8 points)

* Chaque présentation durera 15 minutes et devra présenter l'une des catgories de vulnérabilité du [OWASP Top 10:2021](https://owasp.org/Top10/fr/) c'est à dire
      * présentation des risques
      * proposition et démonstration d'un scénrio d'attaque
        * DVWA est interdit car le cours le traite déjà
      * proposition de mesure à prendre

  * **Après** la présentation vous m'enverrez par mail
    * le support de présentation (ppt, url, ...)
    * l'url de l'exploit et / ou le code nécessaire à l'exploitation (zip / ou repo git)

#### Calendrier FISE 

* [10/09/2024 10h00]
* [17/09/2024 10h00] [A01:2021-Broken Access Control](https://owasp.org/Top10/A01_2021-Broken_Access_Control/)
* [24/09/2024 10h00] [A02:2021-Défaillances cryptographiques](https://owasp.org/Top10/fr/A02_2021-Cryptographic_Failures/)
* [01/10/2024 10h00] [A03:2021-Injection](https://owasp.org/Top10/fr/A03_2021-Injection/)
* [11/10/2024 08h00] [A04:2021-Conception non sécurisée](https://owasp.org/Top10/fr/A04_2021-Insecure_Design/)
* [15/10/2024 10h00] [A05:2021-Mauvaise configuration de sécurité](https://owasp.org/Top10/fr/A05_2021-Security_Misconfiguration/)
* [05/11/2024 10h00] [A06:2021-Composants vulnérables et obsolètes](https://owasp.org/Top10/fr/A06_2021-Vulnerable_and_Outdated_Components/)
* [08/11/2024 10h00] [A07:2021-Identification et authentification de mauvaise qualité](https://owasp.org/Top10/fr/A07_2021-Identification_and_Authentication_Failures/)
* [12/11/2024 10h00] [A08:2021-Manque d'intégrité des données et du logiciel](https://owasp.org/Top10/fr/A08_2021-Software_and_Data_Integrity_Failures/)
* [19/11/2024 10h00] [A09:2021-Carence des systèmes de contrôle et de journalisation](https://owasp.org/Top10/fr/A09_2021-Security_Logging_and_Monitoring_Failures/)
* si un groupe souhaite travailler sur [A10:2021-Falsification de requête côté serveur](https://owasp.org/Top10/fr/A10_2021-Server-Side_Request_Forgery_%28SSRF%29/) il est possible d'échanger avec une autre vulnérabilité
* **Vous vous organiserez avec le fichier Excel [exposés FISE 2024-2025](https://ucafr.sharepoint.com/:x:/s/websec/EbmHrZpxRnZIvKnwg_3yJ08BtGkOfpk59B7gvDEW7jkSYA?e=fCEolV)**

#### Calendrier FISA

* [03/09/2024 13h30]
* [10/09/2024 13h30] [A01:2021-Broken Access Control](https://owasp.org/Top10/A01_2021-Broken_Access_Control/)
* [17/09/2024 13h30] [A02:2021-Défaillances cryptographiques](https://owasp.org/Top10/fr/A02_2021-Cryptographic_Failures/)
* [24/09/2024 13h30] [A03:2021-Injection ](https://owasp.org/Top10/fr/A03_2021-Injection/)
* [05/11/2024 13h30] [A04:2021-Conception non sécurisée](https://owasp.org/Top10/fr/A04_2021-Insecure_Design/)
* [12/11/2024 13h30] [A05:2021-Mauvaise configuration de sécurité](https://owasp.org/Top10/fr/A05_2021-Security_Misconfiguration/)
* [19/11/2024 13h30] [A06:2021-Composants vulnérables et obsolètes](https://owasp.org/Top10/fr/A06_2021-Vulnerable_and_Outdated_Components/)
* [26/11/2024 10h00] [A07:2021-Identification et authentification de mauvaise qualité](https://owasp.org/Top10/fr/A07_2021-Identification_and_Authentication_Failures/)
* [26/11/2024 13h30] [A08:2021-Manque d'intégrité des données et du logiciel](https://owasp.org/Top10/fr/A08_2021-Software_and_Data_Integrity_Failures/)
* [03/12/2024 10h00] [A09:2021-Carence des systèmes de contrôle et de journalisation](https://owasp.org/Top10/fr/A09_2021-Security_Logging_and_Monitoring_Failures/)
* [03/12/2024 13h30] [A10:2021-Falsification de requête côté serveur](https://owasp.org/Top10/fr/A10_2021-Server-Side_Request_Forgery_%28SSRF%29/)
* [14/12/2024 13h30]

* **Vous vous organiserez avec le fichier Excel [exposés FISA 2024-2025](https://ucafr.sharepoint.com/:x:/s/websec/Ecs8_fKFoZ9GsGYqjuXligoB-QFi2hvJr11qOHH-AW53-w?e=3EzoW7)**
  
### 🔨 Note technique (2 points)

* l'url `http://vm-<username>.local.isima.fr` devra renvoyer un code HTTP 200
* l'url `http://vm-<username>.local.isima.fr/websec` devra renvoyer un code HTTP 401
* l'url `http://kali:kali@vm-<username>.local.isima.fr/websec` devra renvoyer un code HTTP 200

vérifiez à tout moment que vous aurez le maximum de points avec les [websec-checker](https://gitlab.isima.fr/vimazeno/websec-checker)


<!-- ### 🔐 bonus piraterie (+3 points)

Une faille est cachée sur le Système d'information de l'ISIMA. Le premier qui me l'envoie par mail à [vincent.mazenod@isima.fr](mailto:vincent.mazenod@isima.fr) verra sa note de présentation ET sa note d'examen finale augmentée de **3 points**.

Vous avez droit à autant de proposition que vous le souhaitez, car il y a fort à parier qu'il y a plus d'une faille dans le SI de l'ISIMA. Chaque faille valide pourra vous rapporter des points en plus également.

<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Soignez votre éthique</h3>
  </div>
  <div class="panel-body">
    Si le défi est de trouver des vulnérabilités, l'idée n'est pas de vous encourager à les exploiter ... N'oubliez pas que vous êtes soumis à <a href="https://doc.isima.fr/support/chartes">différentes obligations légales</a>
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Retrouvez un indice supplémentaire chaque semaine</h3>
  </div>
  <div class="panel-body">
    <ol>
      <li>La faille est accessible depuis l'extérieur, pas besoin du <a href="https://doc.isima.fr/acces-distant/vpn">VPN</a></li>
    </ol>
  </div>
</div> -->

<!--
### 🔥 audit de sécurité sur 2 heures

#### thenetwork

le réseau thenetwork regroupe des personnes ayant suivi des formations d'excellence, dispensées par le réseau thenetwork lui-même. thenetwork a ainsi bâti au fil des années un réseau d'experts efficaces et loyaux les uns envers les autres.

thenetwork est un réseau extrêment fermé, qui se préoccupe beaucoup de la confientialité de ses données et de celles de ses utilisateurs. Rien est accessible sans un compte utilisateur hormis la page d'accueil.

thenetwork possède une partie publique permettant à chaque membre

* de laisser ses contacts
* d'afficher les formations thenetwork qu'il a suivi
* de rechercher parmis les autres membres du réseau
* de se mettre sur liste rouge et de disparaître complètement du moteur de recherche publique
* de mettre un ou plusieurs contacts sur liste rouge pour les réserver à la seule équipe du réseau thenetwork

thenetwork possède une interface d'adminstration permettant

* d'accéder à toutes les fiches des membres
* de saisir les résultats aux formations

Vous avez été missionné pour effectuer un rapport sur la sécurité de l'interface web du réseau thenetwork. Ce document est à destination  de la direction de thenetwork, et de l'équipe technique qui maintient l'appli.

L'équipe technique a mis à votre disposition une machine virtuelle

- disponible sur les Vms des postes de TP
- installable via la rubrique [Recréer l'environnement de cours dans VirtualBox](/zz2-f5-securite-logicielle-securite-des-applications-web.html#recreer-lenvironnement-de-cours-dans-virtualbox)

Vous êtes en boîte blanche, les accés suivants sont donnés

- admin:pipo -> super utilisateur
- cwally:pipo -> simple utilisateur
- mwally:pipo -> simple utilisateur

#### livrables

Vous devez rendre un rapport exposant 3 vulnérabilités de l'appli thenetwork

Pour chacune de ces vulérabilités vous devrez
- la nommer
- mettre en évidence la portion de code vulnérable
- expliquer comment exploiter cette vulnérabilité
- expliquer le scénario le pire qui puisse arriver
- formuler quelques recommandations
    - d'urgences pour patcher asap
    - à plus long terme permettant d'améliorer la sécurité sur ce point

N.B. un BUG ou une incohérence dans les données n'est pas nécessairement une faille de sécurité (seules les failles doivent figurer dans le rapport)

# Barême

Chaque vulnérabilité est notée sur 6. Il reste donc 2 points qui seront attribués en cas
- d'exploitation particulèrement poussée
- complément de recommandations concernant le serveur et sa configuration
- découverte d'une quatrième faille (j'en ai vu au moins une autre ...)

Amusez vous bien :-*
-->

<!-- 
### Projet

#### jwt

Vous implémenterez un exemple de protection d'API REST via [JWT](https://jwt.io/) dans l'un des langages au choix

* php
* nodejs
* python

Vous implémenterez 2 services et un client

* un service de génération et de validation de JWT
* un service accessible via REST avec un JWT valide
* un client capable d'obtenir un JWT et de l'utiliser pour accéder légitimement à l'API REST

Dans un premier temps vous implémenterez des JWT avec mot de passe (chiffrement symétrique)
Dans un second temps vous implémenterez des JWT avec des paires de clés publiques / privées (chiffrement asymétrique)

**N.B.** "l'api rest" peut se résumer à une url /protected qui renvoie un json en méthode GET. L'implémentation d'une API REST ne fait pas parti du sujet!

Dans votre rapport vous analyserez

* les avantages et les inconvénients, notamment au niveau de la sécurité, de chacune des deux approches
* les conséquences des choix d'implémentation sur
    * la validation d'un JWT
    * l'enrolement des clients
    * la gestion d'une compromission côté serveur

#### Rendu

Un repo Gitlab sur [https://gitlab.isima.fr](https://gitlab.isima.fr), par binôme, ayant la forme suivante

* symetric
    * ...
    * README.md
* asymetric
    * ...
    * README.md
* README.md

* symetric/README.md et asymetric/README.md contiennent respectivement la marche à suivre pour pouvoir déployer l'implémentation avec des JWT avec mot de passe et avec des clés publiques / privées

* le fichier README.md contient votre rapport

* vous mettrez l'utilisateur gitlab Vincent Mazenod comme membre en tant que maintainer
* vous m'enverrez l'url du repo dans un message mail chiffré avec GPG
* le tout avant le 22 mars 23h59

#### Critères de notation

* Qualité de l'implémentation, notamment sur l'aspect sécurité (si je passe au travers des sécurités mises en place c'est 0)
* Qualité de la documentation de déploiement (si je n'arrive pas à installer le projet en local sur ma machine c'est 0)
* Qualité de l'analyse (le rapport n'est pas forcément long il s'agit de présenter tous les scénarios d'utilisation et d'attaque en expliquant les avantages et inconvénients de chacune des deux implémentations)

## Mini projet en binôme

* [Enoncé](https://drive.mesocentre.uca.fr/f/d9e76a8e45934a069890/?dl=1)

* [Enoncé](https://drive.mesocentre.uca.fr/f/54bdd1a80c184bbcb63e/?dl=1)

* Rendu le 25/03/2019 à 23h59 dernier délais

    * à [vincent.mazenod@uca.fr](mailto:vincent.mazenod@uca.fr)

      * ```[TP websec]``` dans le sujet du mail ... sinon je vous perds ;)

    * Tous les fichiers nommés en NOMETUDIANT1_NOMETUDIANT2_nomfichier.ext

 -->

## Evaluation du cours

Vous avez aimé ou vous avez détesté ce cours ... [donnez moi votre avis et aidez moi à l'améliorer (en tout anonymat)](https://docs.google.com/forms/d/1avtXTo4vmbGZJA22hatzFjjfo40CPEIHkc9D9tzX7eM)


## See also

* [faire son propre seclab](https://blog.mazenod.fr/faire-son-propre-seclab.html)
