Title: Security Dashboard
Date: 2017-09-20 10:00
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, sécurité
Status: draft

## Contexte

Ce projet vise à réaliser un tableau de bord permettant d'évaluer simplement l'état de la sécurité d'un ensemble de sites ou services web hétérogènes.

Ce tableau de bord prend en entrée une simple liste d'urls, l'idée est d'automatiser au maximum l'audit sécurité de chaque url.

Chaque url de la liste est inspectée à intervalles réguliers afin de déterminer les technologies qu'elle utilise et leur version respective - à la manière du service [Wappalyzer](https://wappalyzer.com/).

Pour chaque technologie:

* [(CVE) Common Vulnerabilities and Exposures](https://fr.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures) sera interrogé afin de déterminer si la version de chaque technologie utilisée par l'url présente une vulnérabilité connue

* la dernière version de chaque technologie sera déterminée (à partir du site ou du dépôt officielle)

Le tableau de bord présentera une rapport clair et priorisé (par indice de criticité) des technologies à mettre à jour par url

* l'inspection automatique des technologies pourra être complétée / corrigée manuellement

* un système d'alerte mail à partir d'un degré d'un niveau de criticicté paramétrable sera également mis en place

## Résultat attendu

un dépôt sur [https://gitlab.isima.fr](https://gitlab.isima.fr) contenant

* le code source implémentant les fonctionnalités demandées
* un fichier ```Vagrantfile``` permettant de tester l'application via ```vagrant up```
* un fichier ```deploy.php``` permettant de déployer l'application
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [PHP](http://php.net/manual/fr/intro-whatis.php), [MySQL](https://www.mysql.com/fr/), [Symfony](https://symfony.com/), [Deployer](https://deployer.org/), [Vagrant](https://www.vagrantup.com/), API, [webscraping](https://fr.wikipedia.org/wiki/Web_scraping)

## Points à considérer

* la facilité de déploiement

* l'ergonomie
    * facilité de configuration
    * simplicité d'utilisation

## Liens

* https://github.com/AliasIO/Wappalyzer
* https://github.com/cve-search/cve-search
* https://stackoverflow.com/questions/14866528/how-can-i-grab-the-latest-stable-repo-version-from-the-github-api
