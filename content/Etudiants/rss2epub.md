Title: RSS2Epub
Date: 2017-09-20 10:40
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, www
Status: draft

## Contexte

RSS2Epub est un service qui permet de convertir une page web au format epub afin d'en optimiser l'affichage sur une liseuse numérique.

Dans sa version finale RSS2Epub proposera

* une gestion d'utilisateur
* la possibilité à chacun de gérer ses propres flux rss.

## Rendu attendu

Vous devez rendre un dépot git avec au minimum

* un code source implémentant les fonctionnalités demandées
* un fichier ```Vagrantfile``` permettant de tester l'application via ```vagrant up```
* un fichier ```deploy.php``` permettant de déployer l'application
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Points à considérer

* nettoyage de toutes les parties
    * publicitaires
    * navigation
    * commentaires

* provisioning
    * le service doit être déployable simplement sur un virtual host

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [PHP](http://php.net/manual/fr/intro-whatis.php), [MySQL](https://www.mysql.com/fr/), [Symfony](https://symfony.com/), [Deployer](https://deployer.org/), [Vagrant](https://www.vagrantup.com/)

## Liens

* https://fr.kobo.com/help/fr-FR/article/938/utiliser-l-application-pocket-sur-votre-tablette-kobo
* https://pandoc.org/
