Title: gitlab-issue
Date: 2017-09-20 10:25
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, www, git
Status: draft

## Contexte

[GitLab](https://about.gitlab.com/) est une forge logicielle.

La version [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce) présente [une API REST](https://docs.gitlab.com/ee/api/README.html).

Il s'agit ici d'implémenter un système de feedback [utilisant la partie de l'API consacrée à la gestion de tickets](https://docs.gitlab.com/ee/api/issues.html) sous forme d'un bundle [symfony](https://symfony.com/).

le bundle devra

* gérer l'authentification par utilisateur gitlab
* gérer l'authentification avec une seule clé d'api permettant à des utilisateurs anonyme de déposer aussi une issue
* gérer les issues par utilisateurs
* s'intégrer intelligemment à une application symfony

## Résultat attendu

un dépôt sur [https://gitlab.isima.fr](https://gitlab.isima.fr) contenant

* le code source implémentant les fonctionnalités demandées
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [PHP](http://php.net/manual/fr/intro-whatis.php), [Symfony](https://symfony.com/), [Vagrant](https://www.vagrantup.com/), [GitLab](https://about.gitlab.com/)

## Points à considérer

* la qualité du code notamment en suivant les bests practices symfony
* la sécurité
* la facilité de déploiment dans une application
* l'intégration du bundle dans une application symfony
