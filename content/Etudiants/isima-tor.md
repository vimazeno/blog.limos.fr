Title: ISIMA Tor
Date: 2017-09-20 10:50
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, www, Tor
Status: draft

## Contexte

le site https://isima.fr souhaite être présent sur le réseau Tor.

Une première phase consistera en la mise en place d'un site web afin qu'il soit visible sur le réseau Tor, dans un environnement que vous maquetterez.

Dans une seconde phase la mise en ligne sera maquettée dans un environnement [ispconfig](https://www.ispconfig.org/)

Le maquettage pourra être réalisé avec [Vagrant](https://www.vagrantup.com/) et le provisioning avec [ansible](https://www.ansible.com/)

## Résultat attendu

un dépôt sur [https://gitlab.isima.fr](https://gitlab.isima.fr) contenant

* le code source implémentant les fonctionnalités demandées
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [Vagrant](https://www.vagrantup.com/), [ansible](https://www.ansible.com/)

## Points à considérer

* sécurité
* reproductibilité et industrialisation
* compréhesion des protocoles utilisés

## Liens

* http://www.bortzmeyer.org/blog-tor-onion.html
