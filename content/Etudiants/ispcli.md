Title: ispcli
slug: ispcli
lang: fr
Date: 2017-09-20 10:20
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, www
Status: draft

## Contexte

[ISPConfig](https://www.ispconfig.org/) est un outil de gestion de serveur (web, mail, dns) via une interface web.

[une API SOAP](http://docs.ispconfig.org/development/remote-api/)  est disponible depuis la version 3.

Ce projet vise à implémenter un client d'api en ligne de commande.

A titre d'exemple [python-gitlab](http://python-gitlab.readthedocs.io/en/stable/cli.html) est un client d'API pour gitlab.

Les commandes à implémenter concernent toutes les actions de

* configuration du serveur
* gestion des clients
* gestion des hotes web
* gestion des utilisateurs ssh
* gestion des bases de données
* gestion des utilisateurs bases de données
* gestion des entréees DNS
* gestion des comptes mail

Seule la partie gestion de la virtualisation pourra être mise de côté.

## Résultat attendu

un dépôt sur [https://gitlab.isima.fr](https://gitlab.isima.fr) contenant

* le code source implémentant les fonctionnalités demandées
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [PHP](http://php.net/manual/fr/intro-whatis.php), [SOAP](https://fr.wikipedia.org/wiki/SOAP), [ISPConfig](https://www.ispconfig.org/)

## Points à considérer

* l'ergonomie
    * facilité de configuration
    * simplicité d'utilisation
    * commandes intuitives
    * possibilité de batcher certaines opérations
