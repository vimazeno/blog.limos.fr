## Browsers

![Browsers](images/browsers/main-desktop-browser-logos.png "Browsers")


## Stats

[![Stats 08/2024](images/browsers/stats-browser-08-2024.png "Stats 08/2024")](https://en.wikipedia.org/wiki/Usage_share_of_web_browsers)


## Browser

<quote>
Un navigateur web est un logiciel conçu pour consulter et afficher le World Wide Web. Techniquement, c'est au minimum un client HTTP.
</quote>

* [Navigateur web](https://fr.wikipedia.org/wiki/Navigateur_web)
* [moz://a > L’histoire des navigateurs web](https://www.mozilla.org/fr/firefox/browsers/browser-history/)

Dans la plupart des cas un navigateur embarque un interpréteur [javascript](js.html): ce qui induit quelque garde fous ...