## web sec lab

![web sec lab](images/lab/lab.jpg "web sec lab")

### h4PpY H4Ck1nG


## <i class="fa-solid fa-poo-storm"></i> environnement

* [http://dv.wa](http://dv.wa) 
    * la vulnérable
* [https://perso.limos.fr/mazenod/slides/1337/exploits](https://perso.limos.fr/mazenod/slides/1337/exploits) 
    * la malicieuse
* [kali](https://www.kali.org/) 
    * l'attaquante


## <i class="fa-solid fa-poo-storm"></i> environnement

* [http://proxy](http://proxy) 
    * la passerelle percée
* [http://debian](http://debian) 
    * la mututalisée moisie
* [http://debian11](http://debian11) 
    * la fresh
* [http://thenetwork](http://thenetwork) 
    * la white box pour l'audit de sécurité (*FISA MAYBE*)
        * https://www.digitalocean.com/community/tutorials/how-to-use-sshfs-to-mount-remote-file-systems-over-ssh


## VirtualBox

[![VirtualBox](images/lab/virtualbox.png)](https://www.virtualbox.org/)


## Kali

[![Kali](images/lab/kali.svg)<!-- .element style="width: 50%" -->](https://www.kali.org/)

* [<i class="fa-solid fa-download"></i> get kali](https://www.kali.org/get-kali)
* [<i class="fa fa-video-camera"></i> tongues of kali](https://www.youtube.com/watch?v=dH9wCRQFVR0) <- rien à voir ;)


## Keyboard Mapping

![French keyboard](images/lab/mapping.png)<!-- .element style="width: 70%" -->


## Connexion

![Connexion](images/lab/connexion.png)

* username:password
    * kali:kali


## Vim

![vim](images/lab/vim.png)<!-- .element style="width: 40%" -->

#### [<i class="fa-solid fa-gift"></i> survival cheatsheet](https://ryanstutorials.net/linuxtutorial/cheatsheetvi.php)


## Definitive Keyboard Mapping

![Definitve French keyboard](images/lab/keyboard.png)

* `sudo vi /etc/default/keyboard`
    * replace `"us"` 
    * by `"fr"`


## sudo without password

![sudo without password](images/lab/sudo.png)

* `sudo vi /etc/sudoers` 
    * replace `%sudo   ALL=(ALL:ALL) ALL` 
    * by `%sudo ALL=(ALL) NOPASSWD:ALL`
* see although `/etc/group`


## burp suite

* next / next / accept / ...
* Proxy -> proxy settings

![proxy settings](images/lab/burp.png)<!-- .element style="width: 80%" -->


## toggle FF proxy

install [Proxy Switcher and Manager](https://addons.mozilla.org/fr/firefox/addon/proxy-switcher-and-manager/)

![ff proxy settings / step 1](images/lab/ff-proxy-step-1.png)<!-- .element style="width: 30%" -->


## toggle FF proxy

![ff proxy settings / step 2](images/lab/ff-proxy-step-2.png)<!-- .element style="width: 50%" -->


## toggle FF proxy

![ff proxy settings / step 3](images/lab/ff-proxy-step-3.png)


## toggle FF proxy

![ff proxy settings / step 4](images/lab/ff-proxy-step-4.png)


## toggle FF proxy

![ff proxy settings / step 5](images/lab/ff-proxy-step-5.png)<!-- .element style="width: 80%" -->


## toggle FF proxy

![ff proxy settings / step 6](images/lab/ff-proxy-step-6.png)


## Web developer addons

install [Web developer](https://chrispederick.com/work/web-developer/)

![ff web developer](images/lab/ff-web-developer.png)


## PHP

[![stats par language](images/lab/stats-php.png)](https://w3techs.com/technologies/overview/programming_language)


## DVWA

* [https://github.com/digininja/DVWA](https://github.com/digininja/DVWA)
* htaccess to protect vm
* security cookie
    * stocké dans la session


## Debian

[![debian](images/lab/debian.gif)<!-- .element style="width: 30%" -->](https://www.osboxes.org/debian)


## @sh4rpf0rc3

[https://sharpforce.gitbook.io](https://sharpforce.gitbook.io/cybersecurity/mon-blog/a-propos-de-moi)

# <i class="fa-solid fa-thumbs-up"></i> 


# 📚 les supports

* [Support de cours](/zz2-f5-securite-logicielle-securite-des-applications-web.html#supports-de-cours)
    * 👋 Attention il n'y a que l'essentiel pour les attaques 
        * 👂 Beaucoup d'infos en cours (parfois demandées à l'exam) 
        * 🙌 n'hésitez pas à poser des questions 

* 😎 peronnalisable pour **votre** confort 


# 🎓 l'évaluation

- 📝 [exam écrit d'1h sans support / 10 points](/zz2-f5-securite-logicielle-securite-des-applications-web.html#examen-ecrit-en-fin-de-session-10-points)
- 📢 [présentation de groupe au début de chaque cours  / 8 points](/zz2-f5-securite-logicielle-securite-des-applications-web.html#presentation-de-groupe-au-debut-de-chaque-cours-8-points)
    - ca commence la **SEMAINE PROCHAINE**
        - [calendrier FISE](/zz2-f5-securite-logicielle-securite-des-applications-web.html#calendrier-fise)
        - [calendrier FISA](/zz2-f5-securite-logicielle-securite-des-applications-web.html#calendrier-fisa)
- 🔨 [Note technique / 2](/zz2-f5-securite-logicielle-securite-des-applications-web.html#note-technique-2-points)