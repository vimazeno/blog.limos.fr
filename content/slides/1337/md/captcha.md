## <i class="fa fa-medkit"></i> [reCAPTCHA - Google](https://www.google.com/recaptcha/intro/index.html)

[http://dvwa.com/vulnerabilities/captcha](http://dvwa.com/vulnerabilities/captcha)

* Security level: low
  * action configurée en 2 étapes
    * Etape 1
      * validation du captcha
    * Etape 2
      * exécution de l'action
  * en modifiant le paramètre step à 2 on bypass le captcha


## <i class="fa fa-medkit"></i> [reCAPTCHA - Google](https://www.google.com/recaptcha/intro/index.html)

* Security level: medium
  * action configurée en 3 étapes
    * Etape 1
      * validation du captcha
    * Etape 2
      * Confirmation de l'action
    * Etape 3
      * exécution de l'action
  * en modifiant le paramètre step à 2 et en ajoutant Change=Change on bypass le captcha


## <i class="fa fa-medkit"></i> Se préserver

* Côté client
  * [RequestPolicy](https://addons.mozilla.org/fr/firefox/addon/requestpolicy) protège mais peut empêcher certains sites de fonctionner
  * [CsFire](https://addons.mozilla.org/fr/firefox/addon/csfire) protège un peu en enlevant toute information d'authentification pour les requêtes cross-site
  * [NoScript](https://addons.mozilla.org/fr/firefox/addon/noscript) va prémunir des scripts en provenance de sites non sûrs
  * [Self-Destructing Cookies](https://addons.mozilla.org/fr/firefox/addon/self-destructing-cookies) permet de réduire la fenêtre d'attaque en supprimant les cookies dès qu'ils ne sont plus associé à un onglet actif
