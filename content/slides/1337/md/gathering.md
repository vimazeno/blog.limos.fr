# collecter

# <i class="fa fa-puzzle-piece" aria-hidden="true"></i>


# On ne touche pas à la cible!

!["ne pas toucher"](images/gathering/ne-pas-toucher.jpeg "ne pas toucher")


## leakable people

* Connaître les dev, les admins, les webmestres, les directeurs, les cadres, les employés, les prestataires
* Interroger
  * les moteurs de recherche
  * les réseaux sociaux
* Ingénierie sociale (social engineering)
  * [Kevin Mitnick](https://fr.wikipedia.org/wiki/Kevin_Mitnick)
    * [L'art de la supercherie](http://www.amazon.fr/Lart-supercherie-r%C3%A9v%C3%A9lations-c%C3%A9l%C3%A8bre-plan%C3%A8te/dp/2744015709)
    * [Cybertraque](http://www.allocine.fr/film/fichefilm_gen_cfilm=23117.html)

Note:
- ingénierue social
    - hors scope du cours
- kevin mitnik
    - a accédé illégalement aux bases de données des clients de Pacific Bell
    - a pénétré illégalement les systèmes de Fujitsu, Motorola, Nokia, Sun Microsystems et aussi du Pentagone
    - est le premier hacker à figurer dans la liste des dix criminels les plus recherchés par le FBI aux États-Unis finalement arrêté en 1995
        - reconverti en expert sécurité


## IP

* [REGIONAL INTERNET REGISTRIES](https://www.arin.net/knowledge/rirs.html)

  * [AFRINIC](http://www.afrinic.net/) Africa, portions of the Indian Ocean
  * [APNIC](http://www.apnic.net/) Portions of Asia, portions of Oceania
  * [LACNIC](http://www.lacnic.net/en/) Latin America, portions of the Caribbean
  * [ARIN](https://www.arin.net/) Canada, Caribbean and North Atlantic islands, and USA
  * [RIPE CC](https://www.ripe.net/) Europe, the Middle East, Central Asia
    * [IP de l'UCA](https://apps.db.ripe.net/search/query.html?searchtext=193.49.119.31#resultsAnchor)


## Noms de domaines

* whois des gestionnaires de premier niveau
  * [ICANN](https://whois.icann.org/fr)
    * contrôle des tld (top-level domains)
    * generic top-level domains (gTLD) depuis 2011
  * [AFNIC](https://www.afnic.fr/fr/produits-et-services/services/whois/)
    * .fr ou .re ainsi que .tf, .yt, .pm et .wf
* whois de registar
  * [Gandi.net](https://www.gandi.net/whois?lang=fr)
* ligne de commande `whois`

Note:
- donne des points d'entrées
    - personnes
    - infra
        - hébergement de dédié
        - CERT


## DNS

!["DNS"](images/gathering/dns.png "DNS")<!-- .element width="70%" -->

* [nslookup](https://fr.wikipedia.org/wiki/Nslookup)
* [dig](https://mediatemple.net/community/products/dv/204644130/understanding-the-dig-command)

Note:
- on peut voir les requêtes dans wireshark
- converti les NDD en IP
    - plusieurs NND sur une IP (multihosting)
        - passer par une autre appli ou une autre interface
            - point d'entrée
        - pas de possibilité de récupérer tous les enregistrements d'un nom de domaine
            - ce sreait bien ca donne même les enregistrements inconnus des moteurs de recherche
                - internes purs, cachés, etc ...
    - sauf si on passe par le transfert de zone
        - c'est un défaut de configuration du serveur autoritaire (y en a pas des masses sur Internet)
            - le transfert est autorisé sur les serveurs dns secondaire connu
        - de toute facon le transfert dns ne fonctionne pas avec un dns secondaire


## DNS leak
### <i class="fa fa-hand-o-right" aria-hidden="true"></i> augmenter la surface d'attaque

* [Fierce Domaine Scanner](http://tools.kali.org/information-gathering/fierce)
    * champs SOA : permier serveur autoritaire
        * adresse mail de celui qui gère les dns
        * tente un transfert de domaine
    * teste les sous domaines en aveugle
    * à partir d'une ip
        * trouve les domaines
    * en perl


## DNS leak

* [DNSrecon](http://tools.kali.org/information-gathering/dnsrecon)
    * même idée
    * en Python
* [IP search using Bing/Azure APIs](http://martin.swende.se/projects/ipsearch.html)
    * basé sur les moteurs de recherche
    * limité à l'indexation de bing

Note:
- on interroge les DNS et les moteurs de recherche
    - toujours pas la cible
- le leak du virtual hosting  ne me paraît pas contournable


## Google-Fu / Google Dork

[Google Hacking Database (GHDB)](https://www.exploit-db.com/google-hacking-database/)

<small>

|           |                       |              |                                            |
|-----------|-----------------------|--------------|--------------------------------------------|
| [""]()    | motif exact           | [site:]()    | dans le ou les noms des domaines spécifiés |
| [-]()     | élimine des résultats | [inurl:]()   | dans l'url                                 |
| [~]()     | approxime             | [intitle:]() | dans la balise title                       |
| [&ast;]() | caractère joker       | [ext:]()     | extension de fichier                       |

</small>

* <a href="#"><strike>link: pages mentionant l'url</strike></a>

[<i class="fa fa-book"></i> Google Hacking for Penetration Testers](http://www.amazon.com/Google-Hacking-Penetration-Testers-Johnny/dp/1597491764)

Note:
- duckduckgo : requete en post / pas d'en tete referer / safe
- cache gogle / way bak machine : info dans le temps et discrète par rapport à la cible
- Google Hacking Data Base GHDB
    - rech par techno
    - exploit DB CVE compliant ? :D  
    - intiltle:"index of" "last modified"   
- chaque moteur a ses commandes


## Veille Offensive

* Forum / News group / Mailing list
* [google groups](https://groups.google.com/forum/#!overview)
* [stackoverflow](http://stackoverflow.com/)
* [github](https://github.com/)
* [Google alerts](https://www.google.com/alerts)

### à automatiser


## Veille Offensive

* technos de prédilection
* style de code
* extraits
* secrets?


## [Shodan](https://www.shodan.io/)

* découverte de l'internet ...
    * en mode combinatoire
      <pre><code data-trim class="bash">/[0-255]\.[0-255]\.[0-255]\.[0-255]:\d*/</code></pre>
    * en analysant les bannières de service
* prise en main rapide
  * [Shodan for penetration testers](https://www.defcon.org/images/defcon-18/dc-18-presentations/Schearer/DEFCON-18-Schearer-SHODAN.pdf)
  * [Les dernières requêtes pour l'inspiration](https://www.shodan.io/explore)


## [Shodan](https://www.shodan.io/)

* Interfaces d'admin
  * sans protection
  * comptes par défaut
    * parfois non modifiables (contrat de maintenance)

* Services vulnérables
    * [Heartbleed](http://www.shodanhq.com/search?q=port%3A443+openssl+1.0.1)

<small>[Hack Like a Pro: How to Find Vulnerable Targets Using Shodan—The World's Most Dangerous Search Engine](http://null-byte.wonderhowto.com/how-to/hack-like-pro-find-vulnerable-targets-using-shodan-the-worlds-most-dangerous-search-engine-0154576/)</small>

Note:
- barrage hydraulique
- cam de surveillance en tout genre
- avec les objetcs connectés
    - le meilleur reste à venir
- la recherche est juridiquement
    - cliquer sur un lien c'est plus flou
        - silencieux envers la cible


## [<i class="fab fa-github"></i> laramies/theHarvester](https://github.com/laramies/theHarvester)

* Domaine en entrée pour en sortie
  * des mails
  * des sous domaines
  * des usernames valides
  * clé GPG
* Utilise Shodan, Google, Bing, Yahoo, twitter, linkedIn, baidu


<!--
## [<i class="fa fa-bitbucket"></i> Recon-ng](https://bitbucket.org/LaNMaSteR53/recon-ng/wiki/Home)

* Scripts rangés par catégories
    * nombreuses API
        * namechk, jigsaw, picasa, youtube, shodan, bing etc  ...
        * sait temporiser les requêtes
* Enregistre toutes les infos dans une bdd locale    
* Même ergonomie que metasploit
-->
## [Maltego](https://www.paterva.com/web6/products/maltego.php)

* Opensource
* Version payante
  * limitation à 12 résultats, compte à réactiver
* Hébergé sur des seveurs non maîtrisés
  * le traffic n'est pas sécurisé
  * pas de self hosting


## [Maltego](https://www.paterva.com/web6/products/maltego.php)

* People
* Groups of people (social networks)
* Companies
* Organizations
* Web sites
* Internet infrastructure such as:
  * Domains
  * DNS names
  * IP addresses
* Documents and files


## [Maltego](https://www.paterva.com/web6/products/maltego.php)

![Maltego](images/gathering/maltego.jpg "Maltego")


<!-- 
## [FOCA](https://www.elevenpaths.com/labstools/foca/)
* Glanneur de méta données /fichier par domaine
  * ip d'imprinante
  * username stocké dans les documents libre office
    * l'OS est donnée aussi
    * Google pour la récup des fichiers sur la cible
      * traitement des méta local
#### <i class="fa fa-windows"></i> Windows Only
-->
### A ce stade on ne s'est toujours pas connecté à la cible

* Si on veut préserver son anonymat
  * Tor
  * vpn
  * proxy
  * [cachedview](http://cachedview.com/)
  * [wayback machine](https://archive.org/web/)
  * [google translate](https://translate.google.fr/m/translate?hl=fr)


## <i class="fa fa-medkit"></i> Se protéger

* Essayer des outils de collectes régulièrement sur son périmètre
  * interroger les moteurs de recherche
* Sensibiliser ses utilisateurs
* Faire une analyse de risque
  * mise en place d'une graduation de la confidentialité de l'information
    * publique
    * secret


## <i class="fa fa-medkit"></i> Se protéger

* Configuration correcte d'apache
  * Directory listing
  * Home page per user
* Configuration correcte des DNS
* Caché n'est pas protégé
  * renommer le dossier `wp-admin`
    * ne suffit probablement pas
