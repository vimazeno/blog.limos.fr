## OWASP
* depuis janvier 2001
* fondation Américaine
    * [à but non lucratif](http://en.wikipedia.org/wiki/501%28c%29_organization#501.28c.29.283.29)
* en France
    * Association loi 1901
    * participe au [clusif - Club de la Sécurité de l’Information Français](http://www.clusif.asso.fr/)
    * Ludovic Petit & [Sebastien Gioria ](https://twitter.com/spoint)
      * [L'OWASP, l'univers, et le reste](https://air.mozilla.org/talks-owasp-afup-firefoxos-security-mozilla-firefoxos-what-is-owasp-by-sebastien-gioria/)

<br /><br />

<iframe src="https://air.mozilla.org/talks-owasp-afup-firefoxos-security-mozilla-firefoxos-what-is-owasp-by-sebastien-gioria/video/" width="640" height="380" frameborder="0" allowfullscreen></iframe>


## OWASP - Indépendance

* composé d'experts indépendants
* indépendant des fournisseurs de solution 
* indépendant des gouvernements
* Les projets sont opensource
* nombreux adhérents
  * entreprises
  * institutions
  * individus
    * [liste des membres](https://docs.google.com/spreadsheets/d/1FQEj2xQb1uTxZMXshPs0suy1Bkb5iYCbHH_vrzHMVa4/edit)
      * [join now](http://myowasp.force.com/memberappregion)
        * <strike>adhésion gratuite</strike> donation obligatoire 50$ minimum


## OWASP - Projets

!["OWASP Flagship mature project"](images/Flagship_banner.jpg "OWASP Flagship mature project")
!["OWASP Lab medium level project"](images/Lab_banner.jpg "OWASP Lab medium level project")
!["OWASP Low activity project"](images/Low_activity.jpg "OWASP Low activity project")
!["OWASP Incubator new projects"](images/Incubator_banner.jpg "OWASP Incubator new projects")


## OWASP - Quelques projets

* Automated Security Verification
    * Vulnerability Scanners
    * Statis Analysis Tools
    * fuzzing (test de données aléatoires en entrée d'un programmme)
* Manual Security Verification
    * Penetration Testing Tools
    * Code Review Tools
* Security Architecture
    * ESAPI
* Secure coding
    * AppSec Libraries
    * ESAPI Reference implementation
    * Guards and Filters


## OWASP - Quelques projets

* AppSec Management
    * Reporting Tools
* AppSec Education
    * Flawed Apps (application trouée)
    * Learning Environments
    * Live CD
    * SiteGenerator (generateur de sites  avec vulnérabilités)
* Canaux de diffusion
    * [wiki](https://www.owasp.org/index.php/Main_Page)
    * [Podcast](https://www.owasp.org/index.php/OWASP_Podcast)
    * [Conference](https://www.owasp.org/index.php/Category:OWASP_AppSec_Conference)
    * [Lists](https://lists.owasp.org/mailman/listinfo)
    * [Livres](https://www.owasp.org/index.php/Category:OWASP_Books)
    * [OWASP github](https://github.com/OWASP)


## Owasp projects architecture

* [OWASP Top Ten project](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project)
* [OWASP Testing Guide](https://www.owasp.org/index.php/OWASP_Testing_Project) -> v4 in progress
   * [OWASP Risk Rating Methodology](https://www.owasp.org/index.php/OWASP_Risk_Rating_Methodology)
* [OWASP Code Review Guide](https://www.owasp.org/index.php/Category:OWASP_Code_Review_Project)
* [OWASP Developer Guide](https://www.owasp.org/index.php/OWASP_Guide_Project)
* [OWASP Application Security Desk Reference](https://www.owasp.org/index.php/Category:OWASP_ASDR_Project)
* Projects
    * [OWASP Cheat Sheets](https://www.owasp.org/index.php/Cheat_Sheets)
    * [OWASP Application Security Verification Standard (ASVS)](http://www.owasp.org/index.php/ASVS)
    * [OWASP Top Ten Project](http://www.owasp.org/index.php/Top_10)
    * [OWASP Code Review Guide](http://www.owasp.org/index.php/Category:OWASP_Code_Review_Project)
    * [OWASP Testing Guide](http://www.owasp.org/index.php/Testing_Guide)
    * [OWASP Legal Project](http://www.owasp.org/index.php/Category:OWASP_Legal_Project)


## S4UC3 PLZ

* [OWASP Where we are.. Where are we going](https://docs.google.com/presentation/d/1ZgY25F0F7QgScMlB1X7LAa70LtyJql8XqcYdR4suPUo/edit#slide=id.g1fd9ad1e_0_10)
* [[MSTD10] - Une méthode d'évaluation de la sécurité Web - 1/1](http://www.microsoft.com/france/vision/mstechdays10/Webcast.aspx?EID=413f809d-abbc-467d-a930-5e2d7da27fef)
* [Web Application Firewalls (WAF)](http://www.cert-ist.com/documents/Document_Cert-IST_000333.pdf)
* Autres
    * [PCI SSC ‐ Payment Card Industry Security Standards Council](https://www.pcisecuritystandards.org)
        * [PCI - DSS Data Payment Card Industry Security Standard](https://www.pcisecuritystandards.org/security_standards/documents.php)
        * [french version](http://fr.pcisecuritystandards.org/minisite/en/) 
    * [Web Application Security Consortium Project page](http://projects.webappsec.org)
    * [Imperva - Resources](http://www.imperva.com/resources/overview.html)
    * [MITRE ‐ Common Weakness Enumeration – Vulnerability](Trends, http://cwe.mitre.org/documents/vuln‐trends.html)
    * [CLUSIF](http://www.clusif.asso.fr/)
        * [CLUSIF - ouvrages](http://www.clusif.asso.fr/fr/production/ouvrages/)
        * [CLUSIR - Rhônes-Alpes](http://www.clusir-rha.fr/)
