# Brute Force

## AKA burning cpu


### <i class="fas fa-cogs"></i> force brute

* par dictionnaires
  * liste d'utilisateurs + liste de mots de passe
    * optimisable avec
      * de la probabilité
      * [des dictionnaires au hasard](https://dazzlepod.com/site_media/txt/passwords.txt)
      * de l'ingénieurie sociale
      * du flaire etc ...


### <i class="fas fa-cogs"></i> force brute

* par recherche
    * combinatoire
* hybride
  * décliner via des règles les propositions d'un dictionnaire
  * ["leetspeakation"](https://fr.wikipedia.org/wiki/Leet_speak) automatique
  * [John the Ripper](http://www.openwall.com/john/) permet de générer des mots de passes dérivant de parties du username


### <i class="fa-solid fa-bomb"></i> Brute force / low

extract rockyou.txt

```bash
cd /usr/share/wordlists
sudo gzip -d rockyou.txt.gz
```

```bash
export security=low
export PHPSESSID=esiff3kfto23f7uit3vr90jks4
hydra -l admin \
  -P /usr/share/wordlists/rockyou.txt \
  dv.wa \
  http-form-get "/vulnerabilities/brute/:username=^USER^&password=^PASS^&Login=Login:H=Cookie\: PHPSESSID=${PHPSESSID}; security=${security};:Username and/or password incorrect."
```


### <i class="fa-solid fa-bomb"></i> Brute force / medium

* `sleep(2)` : Un peu plus long.

```shell
export security=medium
export PHPSESSID=esiff3kfto23f7uit3vr90jks4
```

```shell
patator  http_fuzz  method=GET  follow=0  accept_cookie=0  --threads=1 --rate-limit=2 timeout=10 \
  url="http://dv.wa/vulnerabilities/brute/?username=admin&password=FILE0&Login=Login" \
  0=/usr/share/wordlists/rockyou.txt \
  header="Cookie: security=${security}; PHPSESSID=${PHPSESSID}" \
  resolve="dv.wa:172.16.76.146" \
  -x ignore:fgrep='Username and/or password incorrect.'
```

* [patator](https://github.com/lanjelot/patator)
  * resolv= -> [buggy version](https://bytemeta.vip/repo/lanjelot/patator/issues/174)


### <i class="fa-solid fa-bomb"></i> Brute force / high

capturer la soumission du formulaire
![capturer la soumission du formulaire dans Proxy](images/authentication/dvwa-auth-high-capture-request.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

envoyer la requête dans *Intruder*
![envoyer la requête dans Intruder](images/authentication/dvwa-auth-high-send-intruder.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

* onglet *Intruder*
  * *attack type* -> *pitchfork*
  * ne laisser que password et user_token en paramètres
![Intruder configuré](images/authentication/dvwa-auth-high-intruder-clean.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

* onglet *Payloads*
  * *Payload set*: 1 (mot de passe) -> *Simple list*
    * *Payload settings [Simple list]* -> *load* -> */usr/share/wordslists/fasttrack.txt*
![Intruder / mot de passe](images/authentication/dvwa-auth-high-simple-list.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

  * *Payload set*: 2 (token anti CSRF) -> *Recursive grep*
* onglet *Settings*
  * *Grep - Extract* -> *add*
![Intruder / token](images/authentication/dvwa-auth-high-define-grep-extract.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

  * *Payload set*: 2 (token anti CSRF) -> *Recursive grep*
* onglet *Settings*
  * *Redirections* -> *always*
![Intruder / folow redirect](images/authentication/dvwa-auth-high-follow-redirect.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

* onglet *Resource pool*
  * cocher *create new resource pool*
![Intruder / Ressource](images/authentication/dvwa-auth-high-ressources.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Brute force / high

![Intruder / Resolved](images/authentication/dvwa-auth-high-resolved.png)<!-- .element style="width: 90%" --> 


### <i class="fas fa-cogs"></i> force brute offline

* Dépend de la puissance de calcul
* Non furtive
* Reproductible


### <i class="fas fa-cogs"></i> force brute offline

* [OPHCRACK (the time-memory-trade-off-cracker)](http://lasecwww.epfl.ch/~oechslin/projects/ophcrack/)
  * monde windows
  * fichiers SAM
  * fonctionne avec des [dictionnaires](http://ophcrack.sourceforge.net/tables.php)
  * <i class="fa fa-gift"></i> <a href="http://www.newbiecontest.org/index.php?page=epreuve&no=224">obtenir le pass d'un compte windows en moins de 10 minutes</a>
  * [Password Cracking: Lesson 2](http://www.computersecuritystudent.com/SECURITY_TOOLS/PASSWORD_CRACKING/lesson2/)


### <i class="fas fa-cogs"></i> force brute offline

* [John the Ripper](http://www.openwall.com/john/)
  * monde UNIX/BSD
  * fichier /etc/passwd + /etc/shadow
  * mangling rules: leet speak, custom ...

```shell
unshadow /etc/passwd /etc/shadow > mypasswd
```

* [Checking Password Complexity with John the Ripper](http://www.admin-magazine.com/Articles/John-the-Ripper)


### <i class="fas fa-cogs"></i> force brute rainbow table

* [CrackStation](https://crackstation.net/)
  * avec la signature de smithy dans la table user

```shell
5f4dcc3b5aa765d61d8327deb882cf99
```


### <i class="fa fa-medkit"></i> Se protéger

* instaurer des règles de durcissement au moment du choix du mot de passe
  * pas contournable côté client ;)
* mettre un nombre d'essais maximum consécutifs
  * plugin anti brute force
    * wordpress
    * drupal
  * compléter par des restrictions par IP, voir par blocs d'IPs
* sensibilisez vos utilisateurs
