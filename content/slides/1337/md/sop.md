# SOP / CORS / CORB

## a.k.a Same Origin Policy / Cross Origin Resource Sharing / Cross-Origin Read Blocking


### <i class="fa fa-medkit"></i>  **SOP** / Same Origin Policy

* https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy

* restreint les interactions aux ressources de même origine
  * protocole + "://" + hôte + ":" + [port]
    * concerne `<script>`, `<link>`, `<img>`, `<video>`, `<audio>`, `<object>`, `<embed>`, `@font-face`, `<iframe>`


### <i class="fa fa-medkit"></i> **SOP** / Same Origin Policy

protège l'intégrité de la page

```js
fetch('http://bad-guy.com/data.php')
  .then((DOMResponse) => {
    document
      .getElementById("particualr-div")
      .appendChild(DOMResponse);
  });
```

![SOP](images/xss/sop.png "SOP")<!-- .element style="text-align: center" -->


### <i class="fa fa-medkit"></i> **SOP** / Same Origin Policy

protège la confidentialité des sessions

```js
fetch('https://gmail.com')
  .then((sensitiveData) => {
    fetch('https://gmail.com', {
      method: 'POST',
      body: JSON.stringify({
        data: sensitiveData,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    })
  });
```


### <i class="fa fa-medkit"></i> **CORS** / Cross Origin Resource Sharing

* contrôler les accès en mode cross-site
  * concerne l'échange entre fournisseurs de services
* effectuer des transferts de données sécurisés
  * entre sources sûres niveau injection & confidentialité


### <i class="fa fa-medkit"></i> **CORS** / Cross Origin Resource Sharing

le client ajoute automatiquement une en-tête HTTP

```http
Origin: http://www.foo.com
```

le serveur doit ajouter une en tête HTTP d'autorisation pour le domaine

```http
Access-Control-Allow-Origin: http://www.foo.com
```

en-tête d'autorisation pour tous les domaines

```http
Access-Control-Allow-Origin: *
```  


### <i class="fa fa-medkit"></i> **CORS** / Cross Origin Resource Sharing

* autorise tous les verbes HTTP
  * [JSONP](http://igm.univ-mlv.fr/~dr/XPOSE2009/ajax_sop_jsonp/jsonp_presentation.html) n'autorisait que la méthode GET

  * [<i class="fab fa-stack-overflow"></i> Disable firefox same origin policy](http://stackoverflow.com/questions/17088609/disable-firefox-same-origin-policy)

Note:
- l'introduction de cette nouvelle possibilité implique nécessairement que les serveurs doivent gérer de nouvelles entêtes, et doivent renvoyer les ressources avec de nouvelles entêtes également
- doit être supporté par le navigateur
- la valeur * est possible mais risquée
- requêtes simples, pré-vérifiées avec le verbe OPTIONS, avec habilitations en forcant l'envoie du cookie


### <i class="fa fa-medkit"></i> **CORB** / Cross-Origin Read Blocking

contrôle la cohérence du type MIME avec la balise appelante

* https://chromium.googlesource.com/chromium/src/+/master/services/network/cross_origin_read_blocking_explainer.md
* https://dev.to/ms_74/what-is-corb-3m3f