# CSP

## a.k.a. Content Security Policy


### <i class="fa fa-medkit"></i> **CSP**: Content Security Policy

* En-tête renvoyée côté serveur
  * protéger son contenu
  * protéger ses utilisateurs
  * possibilité de reporting
    * quelles tentatives ont été menées

* https://developer.mozilla.org/en-US/docs/Web/Security/CSP/CSP_policy_directives


### <i class="fa fa-medkit"></i> **CSP**: Content Security Policy

```http
Content-Security-Policy:
  script-src 'self' https://apis.google.com; 
  frame-src 'none'
```

* informera le browser que
  * seuls les scripts en provenance de la page elle même et de apis.google.com pourront être exécutés
  * les balises iframes ne doivent pas être interprétées


### <i class="fa fa-medkit"></i> **CSP**: Content Security Policy

![CSP](images/xss/csp.png "CSP")<!-- .element style="text-align: center" -->

* [<i class="fa fa-newspaper-o"></i> Why is CSP Failing? Trends and Challenges in CSP Adoption](mweissbacher.com/publications/csp_raid.pdf)

Note:
- couvre le cas d'un XSS js dans une balise src
- couvre également le cas d'une iframe qui recouvre une page légitime


### <i class="fa-solid fa-bomb"></i> CSP / low

```http
Content-Security-Policy: 
  script-src 'self' https://pastebin.com hastebin.com 
  www.toptal.com example.com code.jquery.com 
```

* écriture d'un fichier js sur l'hôte local via une autre faille [upload](upload.html) par exemple


### <i class="fa-solid fa-bomb"></i> CSP / low

* exécution directement sur [https://pastebin.com/raw/SAB3JTJc](https://pastebin.com/raw/SAB3JTJc)
  * réponse pastebin
    
    ```http
    ...
    X-Content-Type-Options: nosniff
    ...
    ```


### <i class="fa-solid fa-bomb"></i> CSP / low

* ne fonctionne pas depuis Firefox 72 
  * l'entête [X-Content-Type-Options](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options) force la désactivation du [MIME sniffing](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types#mime_sniffing)
    
* https://www.komodosec.com/post/mime-sniffing-xss
* https://medium.com/@puneet29/what-is-mime-sniffing-4f402d303dc8
