## Détecter, évaluer et réagir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/> 

<div style="text-align: center;">
    <img src="/_/gdi/images/detecter.jpg" width="70%"/>
</div>


## Détection <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### sources
* Utilisateurs
  * Helpdesk
    * Incidents informatiques
      * Changements
      * Performances
      * Redémarrage de machines
* Analyses des journaux, outils de surveillance
  * IDS, IPS, sondes, SIEM, etc.
* Métrologie
* ISIRT ([RENATER](https://www.renater.fr/), [CERT-FR](http://cert.ssi.gouv.fr/) [ex CERTA](http://www.cert.ssi.gouv.fr/cert-fr/certfr.html))
* Police, justice, Victimes d’une attaque
* Médias, Sites web ([zataz](http://www.zataz.com/tag/cnrs), [zone-h](http://www.zone-h.org/archive/filter=1/fulltext=1/domain=cnrs), pastebin, etc.)


## Identifier & caractériser <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
* Recueillir les qui, quoi, quand, où, comment, etc.
* En premier lieu, vérifier que ce n'est pas 
   * une erreur de configuration
   * une erreur humaine 
   * un défaut de communication entre personnes ou entités
* Première évaluation, sans parti pris, ni conclusion hâtive
* En cas de doute ne pas hésiter à demander de l'aide (local, régional, national)


## 1<sup>ère</sup> évaluation  <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
* Désigner une personne compétente (formée)
  * identifiée en phase de préparation et lui laisser la main
* Ouverture du kit de gestion des incidents
* Horodatage précis de l'ouverture du journal
* Déterminer "à grosse maille" 
  * ce qui a été compromis
  * l'étendu des dégâts
* Typer et classer l'incident
  * Passer rapidement et directement au confinement
  * Ou escalader auprès de la hiérarchie et/ou en gestion de crise, puis passer rapidement au confinement