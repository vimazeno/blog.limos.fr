## Confiner et acquérir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>

<div style="text-align: center;">
    <img src="/_/gdi/images/inscetoscopes.jpg" width="70%"/>
</div>


## Les outils A2IMP <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>

* A2IMP : Aide à l'Acquisition d'Informations sur une Machine Piratée
* Formation initiale organisée par l'UREC (2006/2007)
* Méthodologie toujours valable (mais prise en compte de l'évolution des systèmes [linux](https://mycore.core-cloud.net/public.php?service=files&t=f141f3741d356ac95bea7b92287111d7) et [windows](https://mycore.core-cloud.net/public.php?service=files&t=9d00808565b9c8fcd865940008e2b4ac))
* [Livecd A2IMP](https://mycore.core-cloud.net/public.php?service=files&t=1357ba7c8c2604a71f9695449fe6b39a) mis à jour
  * Scientific Linux 6.5 avec kernel récent (3.x)
  * Outils à la racine du CD (pour l'acquisition des données volatiles)
    * a2impLinux
    * a2impWin
  * Outils en mode LiveCD (démarrage du système live pour la sauvegarde des partitions)


## Confiner et acquérir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>

<div style="text-align: center;">
    <img src="/_/gdi/images/zalman.jpg" width="70%"/>
</div>

**Faire l'acquisition des données volatiles avant d'isoler la machine du réseau** permet de récupérer plus d'informations sur le contexte

* connexions réseaux, utilisateurs connectés, processus liés aux activité réseaux, cache, mémoire RAM...


## Confiner et acquérir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Collecte des données volatiles
* Monter l'ISO A2IMP 
* Utiliser les binaires de l'ISO uniquement
* Lancer le script d'acquisition correspondant au système
* Sauvegarder les données récupérées sur un support externe

### cas d'une vm
* faire un snapshot


## Confiner et acquérir  <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Arrêter la compromission
* Arrêter la machine
* Deconnecter le cale réseau physiquement

### cas d'une vm
* arrêter la vm
* désactiver la vm via l'interface de l'hyperviseur

### créer une image disque
* rebooter sur l'ISO A2IMP
  * attention à ne pas rebooter sur le système corrompu
* lancer la copie du disque avec signature
  * sur un disque externe