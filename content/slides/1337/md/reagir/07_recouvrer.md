## Recouvrer <img src="/_/images/PDCA/PDCA-C.png" width="20%" align="right"/>

<div style="text-align: center;">
    <img src="/_/gdi/images/walkertexasranger.jpg" width="70%"/>
</div>


## Recouvrer <img src="/_/images/PDCA/PDCA-C.png" width="20%" align="right"/>

* Objectif: s'assurer d'avoir remis en service un système sain
  * Actions
    * Vérifier et valider le comportement du système 
      * Recettes fonctionnelle et technique avant ré-ouverture
      * Ré-ouvrir le service en mode nominal ou dégradé
      * Suivi renforcé du comportement dans les minutes / les heures / les jours (en fonction du contexte) qui suivent la réouverture du service
        * Analyse quotidienne des traces réseaux et systèmes
        * Analyse quotidienne des journaux d’audit
        * Visite quotidienne des urls
        * Revue quotidienne des résultats Google
        * Etc.
* Si nécessaire, mettre en place une supervision à plus long terme