## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>

<div style="text-align: center;">
  <img src="/_/gdi/images/Shaun_of_the_Dead.jpg" width="70%"/>
</div>


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
* [Un problème de sécurité informatique non-traité est répréhensible](#/1/2)
* Décider rapidement avec la hiérarchie et la chaine fonctionnelle des suites à donner
  * Activer la cellule de gestion de crise
  * Activer les plans de continuité et/ou de reprise
  * Demander une analyse externe
  * <a href="http://www.dgdr.cnrs.fr/FSD/securite-systemes/que-faire4.htm">porter plainte en matière de SSI</a> sur le site de la DG
* Conserver les preuves A2IMP faites à l’étape d’acquisition
  * Surtout en cas d'analyse judiciaire et/ou CERT


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
* Communiquer en interne de l’unité
  * En accord avec la hiérarchie !
  * Peut être établi au préalable lors de la phase de préparation
* Communiquer en externe de l’unité
  * En accord avec la hiérarchie !
  * signaler l'incident au niveau régional et national (chaîne SSI)
    * pour les unités multi-établissements
      * la « convention de site » doit préciser qui est pilote SSI

## Déclarer l'incident <!-- .element: class="fragment" -->


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Déclarer l'incident ... à qui?
* Fonctionnaire de sécurité de défense (FSD)
  * Est informé des incidents concernant la protection du potentiel scientifique et technique (PPST)
  * A délégation pour porter plainte en cas d’intrusion
* Direction des affaires juridiques (DAJ)
  * Impliquée dans les dépôts de plainte
  * Intrusion <i class="fa fa-hand-o-right"></i> FSD
  * Autres (vol par exemple) <i class="fa fa-hand-o-right"></i> DAJ
* Correspondant informatique et libertés (CIL)
  * Impliqué lorsqu’il y a violation de données à caractère personnel
* Direction de la communication (DirCom)
  * Impliquée lorsqu’un incident a une exposition médiatique
* Direction de l’audit interne (DAI)
  * Vérification que les procédures sont bien conformes


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Déclarer l'incident ... à qui?
* Délégué régional
  * Délégation pour certains dépôts de plainte (après avis de la DAJ)
  * Impliqué dans la gestion de crise
* Directeur d’unité
  * Systématiquement informé d’un incident dans son unité
  * La sécurité est de sa responsabilité
* Utilisateur
  * A l’obligation de remonter les incidents SSI
* Cellule de crise
* RSSI des autres tutelles
  * Doivent systématiquement être informés d’un incident
  * A charge de réciprocité
* Tenir informer le CERT (ISIRT) qui a signalé l’incident


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Déclarer l'incident ... par qui?
* Le CSSI (ou le RSSI-Région) enregistre l’incident.
* Le CSSI, le RSSI-Région et le RSSI partenaire complètent l’enregistrement.
* Le RSSI CNRS


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Déclarer l'incident ... comment?
* Formulaire de déclaration d'incident sur l'espace de travail collaboratif dédié à la SSI
  * [https://extra.core-cloud.net/collaborations/RSSI-CNRS/SitePages/D%c3%a9clarer%20un%20incident.aspx](https://extra.core-cloud.net/collaborations/RSSI-CNRS/SitePages/D%c3%a9clarer%20un%20incident.aspx)
    * Détails
    * Machines compromises
    * Journal
    * Investigations
    * Impacts et conséquences de l'incident
  * [Aide à la gestion des incidents](https://extra.core-cloud.net/collaborations/RSSI-CNRS/SitePages/Aide%20gestion%20des%20incidents.aspx)

Note:
- ouvrir le formulaire
- dérouler tous les pop up 

## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Investiguer
* Trouver les causes de l'incident afin de revenir à une situation normale
  * En général, un incident a plusieurs causes
    * Une ou plusieurs [menaces](#menace)
      * Une ou plusieurs [vulnérabilités](#vulnerabilite)
        * Techniques
        * Organisationnelles
* Si nécessaire, activer une analyse inforensique poussée (type A3IMP)
  * Coûts
  * Dégâts causés
  * Facteurs juridiques et procédure judiciaire
  * Complexités de l’attaque


## Eradiquer et agir <img src="/_/images/PDCA/PDCA-D.png" width="20%" align="right"/>
### Préparer le retour à la normale
* Revenir à une situation nominale
  * Restauration partielle
  * Eradication des codes malveillants, etc.
  * Ou, réinstallation complète
    * Recommandé, surtout en cas de doute !
    * Importance des sauvegardes et archives !                 
* Et dans tous les cas s’assurer que l’incident ne se reproduise pas
  * Durcir les configurations
  * Changer et durcir les mots de passe et éventuellement les méthodes d'authentification (multifacteur)
  * Mettre à jour les correctifs de sécurité
  * Durcir la topologie réseau (VLAN, routage, etc.)
  * Durcir le filtrage réseau
  * **Avant de ré-ouvrir le service**