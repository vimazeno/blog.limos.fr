## Erreurs à éviter
<div style="text-align: center;">
    <img src= "/_/gdi/images/erreurs_a_eviter.png" width="90%"/>
</div>


## Erreurs à éviter
* Ne pas déclarer, dissimuler les incidents
  * Outil pour la saisie des incidents
    * Pourquoi investir dans la sécurité s’il n’y a jamais d’incident
    * Traiter mais oublier de déclarer l'incident
  * Minimiser la gravité de l'incident
  * Sur-réagir à l'incident
  * Ne pas demander aide et conseils
    * Aucune honte
    * Nécessaire recul
    * Réseau d’experts
  * Ne pas distinguer les incidents liés à la sécurité de l’information de ceux qui ne le sont pas
    * Surcharge des équipes en charge de la SSI: un bourrage d’imprimante n’est pas un incident SSI même, si c’est très important pour l’utilisateur


## Erreurs à éviter
* Vouloir aller trop vite
  * Ne pas prendre les mesures conservatoires et ne pas recueillir les éléments qui serviront à l’analyse et aux preuves
  * Rétablir un service avant d’être sûr d’avoir compris ce qu’il s’est passé et d’avoir comblé les failles
  * Céder à la pression des utilisateurs
* Avoir des descriptions incomplètes ou imprécises des incidents
* Ne pas se rendre compte que l'on dispose d'informations fausses ou incomplètes
* Détruire des preuves
  * Lancer des outils de détection directement sur la machine et non pas sur une copie des informations
  * Réinstaller avant d’avoir recueilli et sauvegardé toutes les informations


## Erreurs à éviter
* Mal communiquer
  * Oublier de prévenir la hiérarchie
  * Trop communiquer
  * Ne pas informer les gens ayant besoin d'en connaitre
* Ne pouvoir contenir et éradiquer l’incident
* Se refaire compromettre par la même faiblesse
* Ne pouvoir rétablir un environnement sain
* Ne pas faire de retour d’expérience
* Si la faille dépasse l'unité, poursuivre l'analyse sur un dispositif distant sans l'autorisation du propriétaire
* Engager une contre-attaque
  * Illégal
  * Imputation très difficile, risque de se tromper de cible
  * L’attaquant est très fort et peut réagir violemment


## Recommandations
* Faire de la veille technologique et recevoir les bulletins d'alertes
* Être préparé
  * Essentiel pour pouvoir réagir correctement en situation de stress, de crise
  * Disposer des outils
    * Les tester régulièrement
    * Les maintenir à jour
  * Connaître / Documenter les procédures
  * S’entraîner, faire des simulations d’incident
  * Tester régulièrement les sauvegardes et les restaurations
  * Avoir des outils automatiques d’installation de machines (facilite la réinstallation)


## Recommandations
* Ne pas attendre d’avoir toutes les informations pour déclarer un incident
  * Indiquer si on n’est pas sûr d’une information
  * Compléter, corriger par la suite
* Traiter la découverte d’une vulnérabilité comme un incident
  * Quasi incident
  * Retour d’expérience, amélioration
* Impliquer la hiérarchie
* Communiquer
  * Délicat mais essentiel
  * Adapter le discours aux différentes parties prenantes
  * Le meilleur communicant n’est pas celui qui a les mains dans le cambouis, faire passer les messages par la direction 
* Maitriser son périmètre informatique