        
## Conclusions

<div style="text-align: center;">
    <img src= "/_/gdi/images/Monsieur-Cyclopede.jpg" width="90%"/>
</div>


## Conclusions
* Bien déterminer le type d'incident
  * pour appliquer la bonne procédure
* Bien caractériser l'incident
  * Pour améliorer l’appréciation des risques
  * Pour une remontée efficace
  * Pour Estimer le coût de la non sécurité
* Traiter aussi les évènements et les vulnérabilités


## Ressources complémentaires
* [DSI pôle ARESU - Sécurité](https://aresu.dsi.cnrs.fr/spip.php?rubrique16)
* [Tous les documents de l'ANF Gestion des incidents](https://extra.core-cloud.net/collaborations/RSSI-CNRS/Formation%20%20Gestion%20des%20Incidents/Forms/AllItems.aspx)
  * [les documents de la DR17 qui ont servis pour la mise à jour de cette présentation (MERCI!)](https://extra.core-cloud.net/collaborations/RSSI-CNRS/Formation%20%20Gestion%20des%20Incidents/Forms/AllItems.aspx?RootFolder=%2Fcollaborations%2FRSSI-CNRS%2FFormation%20%20Gestion%20des%20Incidents%2FFormation%20-%20Gestion%20des%20Incidents%202014%2FDR17&FolderCTID=0x012000859DA60215B6CA43B5033CAD99231941&View={45010E28-1540-4E48-B20B-06496674B54F})
* [ENISA - Good Practice Guide for Incident Management](https://www.enisa.europa.eu/activities/cert/support/incident-management)
* [ISO27035 (Information security incident management)](http://www.iso27001security.com/html/27035.html)
* [PSSIE](http://www.ssi.gouv.fr/IMG/pdf/pssie_anssi.pdf)
* [Objectifs de sécurité [ANSSI]](http://www.ssi.gouv.fr/IMG/pdf/20140310_Objectifs_de_cybersecurite_document_public.pdf)
* [Charte informatique du CNRS](http://www.cil.cnrs.fr/CIL/IMG/pdf/charte_info.pdf)

        
            