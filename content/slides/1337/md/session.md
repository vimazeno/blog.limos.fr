# Weak Session IDs

## aka lack of entropy


## Entropie

* **Thermodynamique**:Grandeur thermodynamique exprimant le degré de désordre de la matière
* **Théorie de l'information**: Quantité moyenne d'information attribuable à un message constitué par un ensemble de signaux, représentant le degré d'incertitude où l'on est de l'apparition de chaque signal


### <i class="fa-solid fa-bomb"></i> Weak Session IDs / low

![visualiser ses cookies](images/session/cookie.png)<!-- .element style="width: 90%" --> 

simple incrémentation


### <i class="fa-solid fa-bomb"></i> Weak Session IDs / low

![sequencer entropy](images/session/sequencer.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Weak Session IDs / low

![sequencer entropy](images/session/sequencer2.png)<!-- .element style="width: 90%" --> 


### <i class="fa-solid fa-bomb"></i> Weak Session IDs / medium

timestamp


### <i class="fa-solid fa-bomb"></i> Weak Session IDs / high

* hash MD5 probable
* [CrackStation](https://crackstation.net/)


### <i class="fa-solid fa-bomb"></i> Weak Session IDs

![free](images/session/free.png)<!-- .element style="width: 90%" --> 