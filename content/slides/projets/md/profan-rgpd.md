# Profan
## RGPD


## RGPD

* Règlement général sur la protection des données
  * en vigueur le 25 mai 2018
  * Dans tous les états membres de l'UE  
  * En France la CNIL devient autorité de contrôle
  * <strike>Déclaration CNIL</strike>
    * tous responsable
        * démonstration permanente du maintien de la conformité


## Périmètre

* Tout service ou sous traitant (y compris hors UE) traitant des données de résidents de l'UE
  * Entreprises
  * Associations
  * Organismes publics
  * Sous traitants


## Objectifs

### Crédibiliser la régulation

* Réclamation auprès de l'autorité de contrôle
* Droit de recours contre le responsable du traitement ou un sous traitant
* Actions collectives ou personnelles
* Sanctions
  * 4% du chiffre d'affaire annuel mondial
  * 20 000 000 €


## Objectifs

### Renforcer la transparence

* Quelles données sont collectées?
* Dans quels buts?
* Pour combien de temps?


## Objectifs

### Faciliter l'exercice des droits
  
* droit à la rectification
* droit à la portabilité
* droit à l'oubli
  * suppression des données personnelles
    * dès qu'elles ne sont plus nécessaires au traitement
    * dès que le consentement de l'utilisateur a été retiré
    * dès que la personne s'y oppose


## Objectifs

### Responsabiliser les acteurs traitant des données

* Obligation de moyens pour la sécurité des données
* Obligation d'information en cas de violation de données à caractère personnel
  * Vis à vis de l'autorité de contrôle
  * Vis à vis des personnes concernées


## Comment?

* DPO (Data Protection Officer) / DPD (Délégué à la protection des données)

  * conformité RGPD
  * Point de contact avec les autorités

* Analyse d'impact (PIA)

  * [un logiciel pour réaliser son analyse d’impact sur la protection des données (PIA)](https://www.cnil.fr/fr/rgpd-un-logiciel-pour-realiser-son-analyse-dimpact-sur-la-protection-des-donnees-pia)


## Profan

* qui est le responsable du traitement
    
    * L'UCA?
    * Le CNRS?
    * Le ministère de l'éducation nationale?

Réponse le 20 mars!


## Profan

* Quels sont les finalités du traitement?
  * Le LAPSCO doit préciser
  * cette information doit faire partier de la communication Profan dès la rentrée prochaine


## Profan

* exercice des droits
  * les fonctionnalités sont implémentées

* Mesure de sécurité
  * elles ont été listées

Plus généralement l'analyse d'impact a été largement entamé pour eP3c et peut être réutilisé pour Profan.


## à faire

* Déterminer le responsable des traitements
* Détailler (toutes) les finalités du projet
  * Les communiquer clairement aux famille
* Adapter l'analyse d'impact à partir du travail réalisé pour eP3C