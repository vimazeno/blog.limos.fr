# OpenStack


* mise à jour
  * maquettes documentées / réutilisables
* étude de l'architecture
  * procédure de démontage / d'amaigrissement
* rationalisation des projets en cours
  * gestion de l'existant
  * IaaS pour les projets à venir

* initiaition à terraform
  * introduction à go
  * notamment sur les providers proxmox / terraform
