# gitlab


## CI/CD variables

https://gitlab.isima.fr/cri/my/settings/ci_cd

* https://docs.gitlab.com/ee/ci/yaml/ (When:manual) pour le déploiement


## personal tokens

niveau user

https://gitlab.isima.fr/profile/personal_access_tokens

* api
  * Grants complete read/write access to the API, including all groups and projects.

* read_user
  * Grants read-only access to the authenticated user's profile through the /user API endpoint, which includes username, public email, and full name. Also grants access to read-only API endpoints under /users.

* read_repository
  * Grants read-only access to repositories on private projects using Git-over-HTTP (not using the API).

### root only

* sudo
  * Grants permission to perform API actions as any user in the system, when authenticated as an admin user.


## feed token

niveau user

https://gitlab.isima.fr/profile/personal_access_tokens

* Your feed token is used to authenticate you when your RSS reader loads a personalized RSS feed or when when your calendar application loads a personalized calendar, and is included in those feed URLs.

* It cannot be used to access any other data.


## deploy tokens

niveau repo

https://gitlab.isima.fr/cri/my/settings/repository#js-deploy-tokens

Deploy tokens allow read-only access to your repository and registry images.


## deploy keys

niveau repo

https://gitlab.isima.fr/cri/my/settings/repository#js-deploy-tokens

Deploy keys allow read-only or read-write (if enabled) access to your repository. Deploy keys can be used for CI, staging or production servers. You can create a deploy key or add an existing one.


## specific runners

https://gitlab.isima.fr/cri/my/settings/ci_cd

## best practice

* bin/setup (APP_TOKEN - gitlab secret variable / fallback ldap) bin/activate
