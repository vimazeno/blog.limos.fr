# ansible

![ansible](images/ansible.png "ansible")<!-- .element width="30%" -->


## ansible

* outil de
  * provisioning
  * gestion de config
  * déploiemenet d'application

* racheté par RedHat en octobre 2015

* outils équivalents
  * [puppet](https://puppet.com/), [chef](https://www.chef.io/#/), [SaltStack](https://www.saltstack.com/) ...


## ansible Φ

* automatisation
* réutilisabilité
* parallélisation
* idempotence

<br />

#### toute intervention manuelle sur un système est une faute ...<!-- .element class="fragment" -->

## ... GRAVE!<!-- .element class="fragment" -->


## ansible

* écrit en python
  * python 2 par défaut
  * marche bien en python 3 <i class="fa fa-hand-o-left" aria-hidden="true"></i>
    * virtualenv

[<i class="fa fa-book" aria-hidden="true"></i> ansible doc](https://docs.ansible.com/)


## ansible

* prérequis
  * sur la machine pilote (mgmt node)
    * ansible (donc python)
  * sur le(s) noeud(s)
    * une connextion ssh ou PowerShell
    * python


## ansible

[![gestion ansible](images/ansible_mgmt.png "gestion ansible")](https://medium.com/formcept/configuration-management-and-continuous-deployment-cd0892dce998)


## terminologie

* **mgmt node** (machine pilote)
  * machine sur laquelle ansible est installé
  * pilote la configuration de toutes les machines de l'inventaire
* **inventory** (inventaire)
  * fichier contenant les ip ou les noms de domaine de toutes les machines à configurer
* **playbook**
  * gère la configuration à déployer sur chaque machine


## terminologie

* **task**
  * fichier où sont définies les actions réalisées par le playbook
* **module**
  * actions plus ou moins complexes, utilisables à partir des **tasks**
  * ansible possède de nombreux [<i class="fa fa-book" aria-hidden="true"></i> modules natifs](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html)
  * il est possible d'écrire ses propres modules.
* **role**
  * permet d'organiser les playbooks en parties claires et réutilisables


## terminologie

* **facts**
  * information collectée par ansible sur le système d'une machine à configurer
  * peuvent être enrichis
* **handlers**
  * similaire aux **tasks** mais appelables à partir d'une **task**
    * typiquement redémarrage de service par exemple


## inventory

* liste des machines accessibles via ssh
  * organisées par groupes
  * possibilité de fixer des configurations
    * pour tous / par groupe / par machine
  * possibilité de déclarer des variables
    * pour tous / par groupe / par machine
* fichier texte au format *ini*

[<i class="fa fa-book" aria-hidden="true"></i> inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)


## inventory

dans `./inventory.ini`

```ini
[criprod]
ansible-test.criprod.isima.fr

[ovh]
ansible-test.ovh.isima.fr

[criprod:vars]
environment                = production

[all:vars]
ansible_python_interpreter = /usr/bin/python3
ansible_user               = limosadm
```


## inventory

```
Host ansible-test.criprod.isima.fr
   User limosadm
   Hostname 192.168.220.243
   IdentityFile ~/.ssh/ids/duncan.isima.fr/limosadm/id_rsa

Host ansible-test.ovh.isima.fr
  User limosadm
  Hostname 10.10.100.2
  IdentityFile ~/.ssh/ids/duncan.isima.fr/limosadm/id_rsa
  ProxyCommand ssh duncan -W %h:%p
```

la connexion ssh doit se faire sans mot de passe sur le système

```shell
export ANSIBLE_HOST_KEY_CHECKING=False
```


## ad-hoc command

```shell
$ ansible criprod --inventory-file=inventory.ini  \
                  -a "/usr/bin/uptime"

other1.isima.fr | CHANGED | rc=0 >>
 13:12:02 up 21 days, 21:11,  2 users,  load average: 0.13, 0.88, 0.13

pvecriprod2.isima.fr | CHANGED | rc=0 >>
 16:18:08 up 41 days, 23:11,  3 users,  load average: 0.63, 0.18, 0.10

py.criprod.isima.fr | CHANGED | rc=0 >>
 15:18:15 up 1 day, 21:51,  1 user,  load average: 0.13, 0.03, 0.01

gitlab-runner1.criprod.isima.fr | CHANGED | rc=0 >>
 15:18:15 up 1 day, 22:14,  1 user,  load average: 0.00, 0.00, 0.00
```


## ad-hoc command

fonctionne avec les modules ansible

```shell
$ ansible ovh --inventory-file=inventory.ini \
              --module-name ping

ansible-test.ovh.isima.fr | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

avec le module raw python n'est pas nécessaire ;)

```shell
ansible ovh --inventory-file=inventory.ini \
-m raw -a "sudo apt update && sudo apt install -y python"
```


## ad-hoc command

* [<i class="fa fa-book" aria-hidden="true"></i> ad-hoc command](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)
  * [Parallelism and Shell Commands](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#parallelism-and-shell-commands)
  * [File Transfer](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#file-transfer)
  * [Managing Packages](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#managing-packages)
  * [Users and Groups](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#users-and-groups)
  * [Deploying From Source Control](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#deploying-from-source-control)
  * [Managing Services](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#managing-services)
  * [Time Limited Background Operations](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#time-limited-background-operations)
  * [Gathering Facts](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#gathering-facts)


## playbook

[<i class="fa fa-book" aria-hidden="true"></i> playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

my-playbook.yml

```yaml
- name: my-playbook # ce que vous voulez
  hosts: ovh # ou all
                 # ou n'importe quel nom de machine
                 # ou n'importe quel nom de groupe
  remote_user: limosadm # prioritaire sur ansible_user de l'inventaire

```


## playbook

```shell
$ ansible-playbook my-playbook.yml \
    --inventory-file=inventory.ini
```

* exécute le playbook
  * sur toutes les machines définies dans `hosts:`
  * en parallèle

```shell
PLAY [my-playbook] ****************************************************************************

TASK [Gathering Facts] ************************************************************************
ok: [ansible-test.ovh.isima.fr]

PLAY RECAP ************************************************************************************
ansible-test.ovh.isima.fr  : ok=1    changed=0    unreachable=0    failed=0
```


## variables

* nommage
  * pas de `-` pas de `.`
  * pas de numérique pure


## variables

`my-playbook.yml`

```yaml
- name: my-playbook
  hosts: ovh
  remote_user: limosadm

  vars:
    awesomevar: awesome
```

`group_vars/all.yml`

```yaml
coolvar: Coool
```


## variables

[<i class="fa fa-book" aria-hidden="true"></i> debug](https://docs.ansible.com/ansible/latest/modules/debug_module.html)

```yaml
  tasks:

    - name: display awesome message
      debug:
        msg: "{{ awesomevar }}"

    - name: display cool message
      debug:
        msg: "{{ coolvar }}"
```


## variables

```shell
PLAY [my-playbook] ****************************************************************************

TASK [Gathering Facts] ************************************************************************
ok: [ansible-test.ovh.isima.fr]

TASK [display awesome message] ****************************************************************
ok: [ansible-test.ovh.isima.fr] => {
    "msg": "awesome"
}

TASK [display cool message] *******************************************************************
ok: [ansible-test.ovh.isima.fr] => {
    "msg": "Coool"
}

PLAY RECAP ************************************************************************************
ansible-test.ovh.isima.fr  : ok=3    changed=0    unreachable=0    failed=0
```


## variables

lit une valeur à partir Vault (<strike>`ansible-vault`</strike>)

```yaml
vars_prompt:
  - name: "name"
    prompt: "what is your name?"
```

les [<i class="fa fa-book" aria-hidden="true"></i> var_prompts](https://docs.ansible.com/ansible/latest/user_guide/playbooks_prompts.html)
permettent de lire les variables à partir de l'entrée standard.


## variables

* role
  * [<i class="fa fa-book" aria-hidden="true"></i> `default`](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html#role-default-variables)
* [<i class="fa fa-book" aria-hidden="true"></i>  inventaire](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#defining-variables-in-inventory)
* [<i class="fa fa-book" aria-hidden="true"></i> `group_vars`, `host_vars`, ou `inventaire.ini`](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#hosts-and-groups)
* [<i class="fa fa-book" aria-hidden="true"></i> playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#defining-variables-in-a-playbook)
* [<i class="fa fa-book" aria-hidden="true"></i> ligne de commande](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#passing-variables-on-the-command-line)

#### [précédence des variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)


## facts

* valeurs collectées par ansible à l'exécution du playbook

```yaml
- name: display hostname
  debug:
    msg: "System {{ inventory_hostname }}"

- name: display os family
  debug:
    msg: "comes from family {{ ansible_os_family }}"

- name: HOSTVARS (ANSIBLE GATHERED, group_vars, host_vars)
  debug:
    msg: "{{ hostvars | to_yaml }}"
```

[<i class="fa fa-book" aria-hidden="true"></i> Variables discovered from systems: Facts](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variables-discovered-from-systems-facts)


## jinja

[<i class="fa fa-book" aria-hidden="true"></i> templating](https://docs.ansible.com/ansible-container/container_yml/template.html)

utilisable partout (playbook, role, tasks, template)


## filters

[<i class="fa fa-book" aria-hidden="true"></i> filters](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html)

```yaml
"{{ item_path[:4] | replace('/', '-') }}"
```

* renvoie le contenu de la variable (un path)
  * sans les 4 derniers caractères
  * avec les `/` remplacés par des `-`

[<i class="fa fa-book" aria-hidden="true"></i> developing filters](https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#developing-particular-plugin-types)


## lookup

[<i class="fa fa-book" aria-hidden="true"></i> lookup](https://docs.ansible.com/ansible/latest/plugins/lookup.html)

```yaml
vars:
  file_contents: "{{lookup('file', 'path/to/file.txt')}}"
```

```yaml
- name: lit un secret dans vault (mais on fera pas comme ça)
  debug:
    msg: "{{ lookup('hashi_vault', 'secret=secret/hi:value token=xxx url=http://myvault')}}"
```

```yaml
- name: lit une variable d'environnement sur le noeud pilote
  debug:
    msg: "{{ lookup('env','PVE_NODE') }}"
```

[<i class="fa fa-book" aria-hidden="true"></i> lookup list](https://docs.ansible.com/ansible/latest/plugins/lookup.html#plugin-list)

[<i class="fa fa-book" aria-hidden="true"></i> developing lookups](https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html#developing-particular-plugin-types)


## task

[<i class="fa fa-book" aria-hidden="true"></i> loop](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html)

```yaml
- user:
    name: "{{ item }}"
    state: present
  loop:
     - testuser1
     - testuser2
  loop_control:
    index_var: key_index
```

* marche avec
  * n'importe quelle variable itérable    
  * [fileglob](https://docs.ansible.com/ansible/latest/plugins/lookup/fileglob.html) - fichiers par pattern
  * [filetree](https://docs.ansible.com/ansible/latest/plugins/lookup/filetree.html) - tous les fichiers récursivement
  * ...


## task

[<i class="fa fa-book" aria-hidden="true"></i> when](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)

[<i class="fa fa-book" aria-hidden="true"></i> conditions](https://docs.ansible.com/ansible/latest/user_guide/playbooks_tests.html)

```yaml
- name: affiche un message sur la vezrison de l'os
  shell: echo "only on Red Hat 6, derivatives, and later"
  when: |
    ansible_facts['os_family'] == "RedHat" 
    and ansible_facts['lsb']['major_release']|int >= 6
```

conditions jinja, à la python:
* not, is, empty, in ...


## ignore_errors

* permet de continuer l'exécution du playbook 
    * même en cas de retour d'erreur d'une commande

```yaml
- name: get mysqladmin status
  shell: mysqladmin status
  ignore_errors: True
```


## failed_when

* permet de forcer à stopper l'exécution du playbook 
    * sur une condition

```yaml
- name: get mysqladmin
  shell: mysqladmin status
  failed_when: "'FAILED' in command_result.stderr"
```

[<i class="fa fa-book" aria-hidden="true"></i> fail - Fail with custom message](https://docs.ansible.com/ansible/latest/modules/fail_module.html)

* parfois ignorer l'erreur ne suffit pas pour continuer
    * il faut ajouter `failed_when`

```yaml
- name: get mysqladmin
  shell: mysqladmin status
  ignore_errors: True
  failed_when: no
```


## [command modules](https://docs.ansible.com/ansible/latest/modules/list_of_commands_modules.html)

* [<i class="fa fa-book" aria-hidden="true"></i> raw](https://docs.ansible.com/ansible/latest/modules/raw_module.html)
  * n'utilise que ssh et pas python
    * permet d'installer python

* [<i class="fa fa-book" aria-hidden="true"></i> command](https://docs.ansible.com/ansible/latest/modules/command_module.html#command-module)
* [<i class="fa fa-book" aria-hidden="true"></i> shell](https://docs.ansible.com/ansible/latest/modules/shell_module.html)
  * comme **command** mais au travers d'un shell

```yaml
- name: redirige la sortie de somescript.sh dans somelog.txt
  shell: somescript.sh >> somelog.txt
  args:
    chdir: somedir/ifcon
    creates: somelog.txt
```


## register

[<i class="fa fa-book" aria-hidden="true"></i> register](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#registering-variables)

dans une variable

```yaml
- name: le service pam est il lancé
  shell: "ps -aux | grep pam"
  register: pam_status
```

dans une liste

```yaml
- name: les services sshd, systemd, dbus sont ils lancés
  shell: "ps -aux | grep {{ item }}"
  register: services_status
  loop:
    - sshd
    - systemd
    - dbus
```


## register (<i class="fa fa-gift" aria-hidden="true"></i>)

[<i class="fa fa-book" aria-hidden="true"></i> Return Values](https://docs.ansible.com/ansible/latest/reference_appendices/common_return_values.html)

```json
"msg": {
        "changed": true,
        "cmd": "ps -aux | grep pam",
        "delta": "0:00:00.011674",
        "end": "2019-01-23 20:27:24.836966",
        "failed": false,
        "rc": 0,
        "start": "2019-01-23 20:27:24.825292",
        "stderr": "",
        "stderr_lines": [],
        "stdout": "limosadm 22227  0.0  0.1 193868  2688 ?        S    20:27   0:00 (sd-pam)\nlimosadm 22359  0.0  0.0   4628   856 pts/0    S+   20:27   0:00 /bin/sh -c ps -aux | grep pam\nlimosadm 22361  0.0  0.0  14856  1100 pts/0    S+   20:27   0:00 grep pam",
        "stdout_lines": [
            "limosadm 22227  0.0  0.1 193868  2688 ?        S    20:27   0:00 (sd-pam)",
            "limosadm 22359  0.0  0.0   4628   856 pts/0    S+   20:27   0:00 /bin/sh -c ps -aux | grep pam",
            "limosadm 22361  0.0  0.0  14856  1100 pts/0    S+   20:27   0:00 grep pam"
        ]
    }
```


## register (<i class="fa fa-gift" aria-hidden="true"></i>)

afficher l'output

```yaml
- name: afficher la sortie de la commande
  debug:
    msg: pam_status.stdout
```

itérer sur une liste

```yaml
- name:  afficher la sortie de chaque commande
  debug:
    msg: "{{ services_status.results[item].stdout }}"
  loop: "{{ range(0, 3)|list }}"
```

ou

```yaml
- name:  afficher la sortie de chaque commande
  debug:
    msg: "{{ item.stdout }}"
  loop: "{{ services_status.results }}"
```


## register (<i class="fa fa-gift" aria-hidden="true"></i>)

```yaml
- name: register foo
  shell: echo "foo"
  register: txt
```

```yaml
- name: register bar
  shell: echo "bar"
  register: txt
  when: False
```

If a task fails or is skipped, **the variable still is registered with a failure or skipped status**


## modules

* [<i class="fa fa-book" aria-hidden="true"></i> file](https://docs.ansible.com/ansible/latest/modules/file_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> lineinfile](https://docs.ansible.com/ansible/latest/modules/lineinfile_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> copy](https://docs.ansible.com/ansible/latest/modules/copy_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> template](https://docs.ansible.com/ansible/latest/modules/template_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> stat](https://docs.ansible.com/ansible/latest/modules/stat_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> get_url](https://docs.ansible.com/ansible/latest/modules/get_url_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> unarchive](https://docs.ansible.com/ansible/latest/modules/unarchive_module.html)


## modules

* [<i class="fa fa-book" aria-hidden="true"></i> package](https://docs.ansible.com/ansible/latest/modules/package_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> user](https://docs.ansible.com/ansible/latest/modules/user_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> systemd](https://docs.ansible.com/ansible/latest/modules/systemd_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> pip](https://docs.ansible.com/ansible/latest/modules/pip_module.html)
* [<i class="fa fa-book" aria-hidden="true"></i> expect](https://docs.ansible.com/ansible/latest/modules/exepect_module.html)
  * `pip install pexpect`
* [<i class="fa fa-book" aria-hidden="true"></i> windows modules](https://docs.ansible.com/ansible/latest/modules/list_of_windows_modules.html)
* [<i class="fa fa-book" aria-hidden="true"></i> ...](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html)


## handlers

```yaml
  handlers:
    - name: restart apache
      service:
        name: apache2
        state: restarted
      listen: "restart apache"

  tasks:
    - name: enable some apache modules
      apache2_module:
        state: present
        name: "{{ item }}"
      notify: restart apache
```


## pre / post tasks

```yaml
pre_tasks:
  - name: update sources
    apt:
      update_cache: yes
    tags: [base]

...

post_tasks:

...
```


## tags

tags au niveau tâches

```yaml
- name: MySQL to listen on all interfaces, not just localhost
      lineinfile:
        dest: /etc/mysql/mariadb.conf.d/50-server.cnf
        regexp: "^bind-address            = 127.0.0.1"
        line: "#bind-address            = 127.0.0.1"
  tags: [database]
```

`always` tag spécial exécuté à tous les coups

```yaml
  pre_tasks:

    - name: update sources
      apt:
        update_cache: yes
      tags: [always]
```


## tags

```shell
$ ansible-playbook my-playbook.yml --list-tags
```

liste tous les tags disponibles dans le playbook

```shell
$ ansible-playbook my-playbook.yml --tags database
```

n'exécute que les tâches du playbook ayant un tag `database`

```shell
$ ansible-playbook my-playbook.yml --skip-tags database
```

exécute toutes les tâches du playbook sauf celles ayant un  tag `database`


## dry run

```shell
$ ansible-playbook my-playbook.yml --check --diff
```

`--check`

simule les tâches à effectuer sans les effectuer (dry-run)

`--diff`

indique ce qui change


## verbosity

```shell
$ ansible-playbook my-playbook.yml -vvv
```

`-v`, `-vv`, `-vvv`, `-vvvv`

pour la verbosité


## limit

```shell
$ ansible-playbook my-playbook.yml --limit=py.isima.fr
```

exécute toutes les tâches du playbook sur py.isima.fr uniquement

<i class="fa fa-hand-o-down" aria-hidden="true"></i>

[<i class="fa fa-book" aria-hidden="true"></i> ansible-playbook](https://docs.ansible.com/ansible/latest/ansible-playbook.html)


## roles

```yaml
- name: my-playbook # ce que vous voulez
  hosts: criprod # ou all
                 # ou n'importe quel nom de machine
                 # ou n'importe quel nom de groupe
  remote_user: limosadm # prioritaire sur ansible_user de l'inventaire

  roles:

    - role: debug # le rôle debug sera exécuté par le playbook
      tags: debug # le tag debug sera ajouté à toutes les tâches du role debug
      my-variable: "pipo" # il ya d'autres endroit où mettre les variables
                          # à suivre ...
    - role: vault-cli # le rôle vault-cli sera exécuté par le playbook
      tags: vault # le tag vault sera ajouté à toutes les tâches du role vault-cli

```


## layout

[<i class="fa fa-book" aria-hidden="true"></i> Directory Layout](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#directory-layout)

[<i class="fa fa-book" aria-hidden="true"></i> Alternative Directory Layout](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#alternative-directory-layout)

[<i class="fa fa-gitlab" aria-hidden="true"></i> cri/ansible-role-boilerplate](https://gitlab.isima.fr/cri/ansible-role-boilerplate)


## roles

* [<i class="fa fa-gitlab" aria-hidden="true"></i> cri/ansible-role-vault](https://gitlab.isima.fr/cri/ansible-role-vault)

* [<i class="fa fa-gitlab" aria-hidden="true"></i> cri/ansible-role-gitlab](https://gitlab.isima.fr/cri/ansible-role-gitlab)

* [<i class="fa fa-gitlab" aria-hidden="true"></i> cri/ansible-role-ispconfig](https://gitlab.isima.fr/cri/ansible-role-ispconfig/)

* [<i class="fa fa-book" aria-hidden="true"></i> debops](https://docs.debops.org/en/master/)

* [<i class="fa fa-github" aria-hidden="true"></i> bau-sec/ansible-openvpn-hardened](https://github.com/bau-sec/ansible-openvpn-hardened)
* [<i class="fa fa-github" aria-hidden="true"></i> ...](https://github.com/)


### skeleton

```shell
$ git clone git@gitlab.isima.fr:cri/ansible-role-boilerplate.git
$ ansible-galaxy init --role-skeleton ansible-role-boilerplate gitlab
```

* dev

```shell
$ ./bin/setup
$ source ./.venv/bin/activate
$ vagrant up
```

* utilisation dans un playbook 

`requirements.yml`

```yaml
- name: vault
  src: git+ssh://git@gitlab.isima.fr/cri/ansible-role-vault.git
  path: ./roles/remotes
```

```
$ ansible-galaxy install -f -r requirements.yml
```


## [<i class="fa fa-book" aria-hidden="true"></i> callback](https://docs.ansible.com/ansible/latest/plugins/callback.html)

```shell
[20:59:19] Install unixODBC | default | CHANGED | 2162ms
[20:59:21] Install Vlogger, Webalizer, and AWstats | default | SUCCESS | 6727ms
[20:59:28] comment awstas croned jobs | default | CHANGED | 1410ms
[20:59:30] Debconf for roundcube-core | default | CHANGED | 1510ms
[20:59:31] Install roundcube and dependencies | default | SUCCESS | 10607ms
[20:59:42] remove the # in front of the first 2 alias line | default | CHANGED | 452ms
[20:59:42] add the line "AddType application/x-httpd-php .php" right after the "<Directory /var/lib/roundcube>" line | default | SUCCESS | 558ms
[20:59:43] change the default host to localhost | default | CHANGED | 587ms
[20:59:44] Install Vlogger, Webalizer, and AWstats | default | SUCCESS | 10966ms
[20:59:55] download jailkit | default | SUCCESS | 862ms
[20:59:55] untar jailkit | default | CHANGED | 1119ms
[20:59:57] untar jailkit | default | CHANGED | 176ms
[20:59:57] build jailkit deb | default | CHANGED | 2655ms
[20:59:59] Install jailkit .deb package | default | CHANGED | 2067ms
[21:00:02] remove jailkit stufff | default | CHANGED | 177ms
[21:00:02] install fail2ban | default | SUCCESS | 1584ms
```


## ansible.cfg

```ini
[defaults]
roles_path = ./ansible/roles/remotes:./ansible/roles/apps:./ansible/roles/commons:./ansible/roles/services
inventory = ./ansible/inventory.ini
filter_plugins = ./ansible/plugins/filter:
lookup_plugins = ./ansible/plugins/lookup:
callback_plugins = ./ansible/plugins/callback:
module_utils = ./ansible/module_utils:
stdout_callback = anstomlog
deprecation_warnings = False

[privilege_escalation]
become: yes
become_user: root
become_method: sudo
```


## [<i class="fa fa-book" aria-hidden="true"></i> set_fact](https://docs.ansible.com/ansible/latest/modules/set_fact_module.html) & pre_tasks

```yaml
criprod:
  pvecriprod1:
    api_users:
      - proxmoxapi
      - vimazeno
```

```yaml
- name: provisionner l'environnement du noeud (pour y accéder plus facilement dans les roles)
  set_fact:
    _pve: "{ 'cluster': '{{ lookup('env','PVE_CLUSTER') }}', 'node': '{{ lookup('env','PVE_NODE') }}', 'host': '{{ lookup('env','PVE_HOST') }}'}"

- name: provisionner les utilisateurs d'api pve uniquement du noeud (pour y accéder plus facilement dans les roles)
  set_fact:
    api_users: "{ 'api_users': {{ hostvars[inventory_hostname][_pve.cluster][_pve.node]['api_users'] }}}"

- name: fusionner l'environnement du noeud (pour y accéder plus facilement dans les roles)
  set_fact:
    pve: "{{ _pve | combine(api_users) }}"
```

[<i class="fa fa-gitlab" aria-hidden="true"></i> réorganiser les variables dans une pre_task](https://gitlab.isima.fr/cri/stack/blob/master/ansible/pre-tasks/set-pve-vars.yml)


## extend

[<i class="fa fa-book" aria-hidden="true"></i> developing plugins](https://docs.ansible.com/ansible/latest/dev_guide/developing_plugins.html)

[<i class="fa fa-book" aria-hidden="true"></i> developing modules](https://docs.ansible.com/ansible/latest/dev_guide/developing_modules.html)
