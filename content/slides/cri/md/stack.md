# stack

![stack](images/aws.png "aws")<!-- .element width="30%" -->

aka **B** rew **W** ired **S** tack


## soyons honnête!

Tout repose sur

* [proxmox-provisionner - pulse](https://github.com/Telmate/terraform-provider-proxmox/pulse)
* [proxmox-api - pulse](https://github.com/Telmate/proxmox-api-go/pulse)

et sur les produits [HashiCorp](https://github.com/hashicorp)


## Directory Layout

```shell
+ ansible/
  + plugins/        # utilisé pour le callback anstomlog <3
  + roles/          # roles ansible (commun)
+ bin/              # toutes les commandes de la stack
+ docs/             # documentation
+ packer/           # préparation des templates pve
+ workspaces/       # répertoire pour les workspaces
  + wk/             # répertoire du workspace wk
    + ansible/      # vars et playbooks ansible de wk
    + terraform/    # ressources terraform de wk
    - config.yml    # config de wk
    - ssh_config    # config ssh de wk
```

# GROUND0

## PRA

Après la catastrophe on a remnté

- un réseau opérationnel
- des noeuds proxomox non configurés (fresh install)
- un serveur vault restauré
- une copie du repo de la stack et des rôles minimaux nécessaires
    - pve
    - gitlab
    - backuppc
        - la sauvegarde de tous les services à restaurer avec backuppc

