# INP/ISIMA / LIMOS
## Bienvenue


## DOC

* https://doc.isima.fr
* https://doc.cri.isima.fr (accès restreint aux membres du CRI)


## SUPPORT

* https://support.isima.fr
* https://dsi.uca.fr/assistance
    * https://support.uca.fr
* DIL INP

### CRI

* 7 membres infra / services / support (A115/A109)
* 1 membres dédié projets recherche (F201)
* 1 membres dédié HPC
* 1 membres dédié électronique



## BATIMENTS


## AUTHENTIFICATION

* identifiants UCA
    * 1 login
    * 1 mot de passe 
        * à changer
    * https://ent.uca.fr/compte/
        * RH
        * scola

* annuaire synchrone à l'ISIMA/LIMOS
    * Active Directory (DC + rodc + OpenLdap synchronisé)
    * compte uca pour TOUS les services
    * comptes invités locaux
    * groupes utilisateurs
        * permissions sur les services
    * changement de mot de passe via l'ENT UCA immédiat partout

* identifiants UCA pour tous les services UCA / ISIMA / LIMOS


## MAIL

* @isima.fr, @limos.fr
    * une seule boite pour 2 alias
    * webmail SOGO 
        * https://mail.isima.fr === https://mail.limos.fr
        * agenda
        * carnet d'adresse
        * possibilité de forward
* @etu.uca.fr
    * alias @etu.isima.fr
* @uca.fr
    * mail universitaire 
    * boite distinct de @[isima|limos].fr
* @clermont-auvergne-inp.fr
    * pour le personnel INP
* @cnrs.fr
    * pour le personnel CNRS


## HOME

* dirs.local.isima.fr
    * nfs + kerberos
    * monter sur perso.isima.fr
* ssh ucausername@dirs.local.isima.fr
    * SEUL CE QUI EST ICI EST EN LIEU SUR!


## FIXE ET LAPTOP

* gérer par le CRI et avec accès au réseau ISIMA/LIMOS
    * un fix + un laptop pour les enseignants chercheurs
    * un fixe ou un laptop pour les doctorant

* machines perso, téléphone, tablette
    * connexion WIFI via https://eduroam.org/
        * identifiant UCA

* sous windows 
    * P:// monte votre home de dirs
            * SEUL ENDROIT SUR POUR SAUVER CES DONNEES

* TOUS LES POSTES SONT CHIFFRES 
    * sans aucune exception fixe et laptop

* possibilité de prêt selon les stocks


## MACHINES PEDA

* PC Triple boot
    * Windows
    * Kubuntu
    * OS exam 

* PC spécialisés
    * Physique
    * Robotique
    * Electronique

* Tx
    * GONA DIE!

* Accès libre


## ACCES SERVEURS VMS

* ssh
* guacamole
* xRDP


## SERVEURS PEDA

* serveurs UNIX
    * ADA / Turing
        * shared monte votre home de dirs
            * SEUL ENDROIT SUR POUR SAUVER CES DONNEES

* serveurs Windows
    * TSE1
        * P:// monte votre home de dirs
            * SEUL ENDROIT SUR POUR SAUVER CES DONNEES

* résintallables à tout moment
* résintallés le 7 juillet 2023
    * AUCUN BACKUP!


## VMs PEDA

* accessible via
    * ssh 
        * lire les messages
    * guacamole
        * problème de raccourcis clavier
    * xRDP


## SERVEURS HPC

* Helene Toussaint
    * helen.toussaint@limos.fr
* https://hpc.isima.fr


## SERVICES ISIMA/LIMOS

* https://hedgedoc.isima.fr
* https://guacamole.isima.fr
* https://my.isima.fr
    * vpn
        * cri (accès aux seuls membres)
        * profs (enseignants / chercheurs et administration)
        * étudiants
    * marche / arrêt de VMs péda

* https://gitlab.isima.fr (péda)
* https://gitlab.limos.fr (recherche)


## SERVICES UCA

## SERVEURS PROJETS


## IMPRESSIONS


## RESEAU


## HYPERVISION