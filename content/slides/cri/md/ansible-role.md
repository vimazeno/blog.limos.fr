## boilerplate

[ansible-role-boilerplate](https://gitlab.isima.fr/cri/ansible-role-boilerplate)

* Tous les rôles dervaient se tester aussi simplement que ça, en standalone, sans avoir peur de rien casser
* En pratique il faut parfois y réfléchir un peu
    * path vault
    * point de montage
    * dépendance à des services existants


## Directory Layout

[Directory Layout (ansible best practice)](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#directory-layout)

```shell
+ tasks/            #
  - main.yml        #  <- tasks file can include smaller files if warranted
+ handlers/       #
  - main.yml      #  <- handlers file
+ templates/      #  <- files for use with the template resource
  - ntp.conf.j2   #  <- templates end in .j2
+ files/          #
  - bar.txt       #  <- files for use with the copy resource
  - foo.sh        #  <- script files for use with the script resource
+ vars/           #
  - main.yml      #  <- variables associated with this role
+ defaults/       #
  - main.yml      #  <- default lower priority variables for this role
+ meta/           #
  - main.yml      #  <- role dependencies
+ library/        # roles can also include custom modules
+ module_utils/   # roles can also include custom module_utils
+ lookup_plugins/ # or other types of plugins, like lookup in this case
```


## Directory Layout Bonus

* Vagrantfile -> cross OS
* vagrant.rb
* role.yml
* .gitignore
* README.md


## TODO

pacakger les box vagrant de manière synchrone avec les templates pve