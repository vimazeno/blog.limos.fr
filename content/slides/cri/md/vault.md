## Vault en bref!

![vault](../cri/images/vault.png "vault")<!-- .element width="30%" -->

#### Des secrets, des apps, des tokens, des équipes, un séquestre 


## Vault

* By HashiCorp
  * [écrit en go](https://github.com/hashicorp/hcl)
  * cross plateform
  * [hcl](https://github.com/hashicorp/hcl)
  * intégration avec [consul](https://www.consul.io) & [terraform](https://www.terraform.io/)
* [auditable](https://www.vaultproject.io/docs/commands/audit/enable.html)
* [documenté](https://www.vaultproject.io/docs/)
* ... couteau suisse


## Vault

* Un binaire [vault](https://releases.hashicorp.com/vault/) qui fait
  * serveur  
    * [<i class="fa fa-book" aria-hidden="true"></i> api](https://www.vaultproject.io/api/overview)
    * UI web
  * cli 

```shell
$ export $VAULT_ADDR=https://10.0.0.1
```


## Configuration

`/etc/vault/vault.hcl`

```
backend "file" {
  path = "/var/lib/vault"
}
ui = true
disable_mlock = true
listener "tcp" {
  address     = "10.0.0.1:443"
  tls_cert_file = "/etc/certs/vault.crt"
  tls_key_file  =  "/etc/certs/vault.key"
  tls_disable   = 0
}
```


## initialisation SSS 
### Shamir's Secret Sharing

```shell
$ vault operator init -key-shares=3 -key-threshold=2
```

```shell
Unseal Key 1: 6gAO3lmAhIaHzDAdkK256g2B2Dpeqy+z4jqQCJBID3d8
Unseal Key 2: TO7DDRQXSPC3IQylPEBPjPGAGAGMzjEkjT/FL62m7UUd
Unseal Key 3: CYfEbt83jYsQFcSErHT4Y5NCsrEtfFUE6tjQZKfP632K

Initial Root Token: s.78MykQO2b5qcy03rtoNwmhr1

Vault initialized with 3 key shares and a key threshold of 2. Please securely
distribute the key shares printed above. When the Vault is re-sealed,
restarted, or stopped, you must supply at least 2 of these keys to unseal it
before it can start servicing requests.

Vault does not store the generated master key. Without at least 2 key to
reconstruct the master key, Vault will remain permanently sealed!
```


## Authentification

!["authentification"](../cri/images/vault-auth.png "authentification")


## Authentification

par token

```shell
$ vault login token=<root-or-app-token>
```

equivalent à 

```shell
$ export VAULT_TOKEN token=<root-or-app-token>
```

par ldap

```shell
$ vault login -method=ldap username=mazenovi
```

stocke le token dans `~/.vault-token` et `$VAULT_TOKEN`


## Policy (ACL)

```
path "cri/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
```

écriture

```shell
$ vault policy write cri `/etc/vault/cri.hcl`
```
application à un groupe ldap

```shell
$ vault write auth/ldap/groups/cri policies=cri
```

création de token à partir de la policy

```shell
$ vault token create -policy=cri
```


## [<i class="fa fa-book" aria-hidden="true"></i> Secrets engines](https://www.vaultproject.io/docs/secrets/)

!["secrets engines"](../cri/images/vault-secrets-engines.png "secrets engines")


## workflow

```shell
$ vault secrets list
$ vault kv list cri/
$ vault kv get cri/test
$ vault kv get -format=json cri/test
$ vault kv get -format=json cri/test | jq .data
$ vault kv get -format=json cri/test | jq .data.data.root
$ vault kv put cri/test password2=$(date | sha256sum)
$ vault kv patch cri/test password1=$(date | sha256sum)
$ vault delete cri/test
```


## avec ansible

* lookup natif [hashi_vault](https://docs.ansible.com/ansible/latest/plugins/lookup/hashi_vault.html)
  * lecture uniquement
  * pas de support natif pour kv2 à ce jour

* module "community" [hashivault](https://github.com/TerryHowe/ansible-modules-hashivault)
  * Reading and Writing
    * supporte kv2
  * Initialization, Seal, and Unseal
  * Policy
  * User Management


## UI

!["UI"](../privacy/images/vault/web.png "UI")


!["carnets mots de passe"](../privacy/images/vault/password_management.jpg "carnets mots de passe")<!-- .element width="80%" -->
