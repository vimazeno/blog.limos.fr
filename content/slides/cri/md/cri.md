## service CRI ISIMA / LIMOS

![service CRI ISIMA / LIMOS](https://support.isima.fr/upload/logo/cri_.png)<!-- .element width="30%" -->

### Vincent Mazenod 

[vincent.mazenod@isima.fr](maitlo:vincent.mazenod@isima.fr)
#### responsable de service


## 🔧 service CRI ISIMA / LIMOS

* [Missions](https://doc.isima.fr/support/cri#3-missions) / [organisation](https://doc.isima.fr/cri#1-organisation-du-support)
* [Composition](https://doc.isima.fr/support/cri#2-composition)
    * 7 membres support (A109 /A115)
* ### 📘 [https://doc.isima.fr](https://doc.isima.fr)
* 💁 Support 
    * 🎫 [tickets incidents/demandes](https://support.isima.fr)
        * [https://support.isima.fr](https://support.isima.fr)
    * 🏨 Bureau A109 
    * 📞 Téléphone  0473405262


## 📝 Inscription (1/2)

* [https://inscription.uca.fr](https://inscription.uca.fr)
    * se munir de son numéro CVEC 
        * [https://cvec.etudiant.gouv.fr](https://cvec.etudiant.gouv.fr)
    * procédure d’inscription en fonction du cursus
        * Primo entrants:
            * **n°OPI date de naissance** + **mail personnel**
        * Anciens étudiants:
            * **n°étudiant** + **date de naissance** + **mail personnel**


## 📝 Inscription (2/2)

* 💵 Règlement financier pour finaliser l'inscription 
    * Aurion Web (INP)

* 😥 Pas de compte = pas de service

    * ⏰ S'inscrire avant le 11 septembre!

* 💬 Questions Scola:
    * 📨 [scolarite@isima.fr](mailto:scolarite@isima.fr) 
    * 🏨 Bureau A010 


## 🔌 Compte UCA 

* 🏦 ENT ([https://ent.uca.fr](https://ent.uca.fr)) 
    * mon compte > mot de passe
    * notes 
    * emplois du temps
    * cours en ligne
* 📨 [Mail](https://doc.isima.fr/services/mail/uca) ([https://mail.uca.fr](https://mail.uca.fr)) 
    * prenom.nom@etu.uca.fr 
    * prenom.nom@etu.isima.fr (alias)


## 🔖 Services UCA

* 📶 [WiFi (Eduroam)](https://doc.isima.fr/locaux/wifi) ([https://cat.eduroam.org](https://cat.eduroam.org))
* 📹 [Teams](https://doc.isima.fr/services/visio/teams) ([https://teams.microsoft.com](https://teams.microsoft.com))
* 📁 Drive 
    * [drive UCA (Seafile)](https://doc.isima.fr/services/stockage/seafile) [https://drive.uca.fr/](https://drive.uca.fr/)
    * [OneDrive (1To)](https://doc.isima.fr/services/stockage/owncloud)
* 🔍 [Catalogue de services UCA](https://dsi.uca.fr/catalogue-de-services)


## 🔐 Mots de passe

* ça ne se prête pas
* ça ne se laisse pas traîner à la vue de tous
* ça ne s'utilise qu'une fois
* si ça casse on remplace immédiatement
* un peu d'originalité ne nuit pas
* la taille compte
* il y a une date de péremption
* mieux vaut les avoir avec soi


## 💉 question d'hygiène!

![preservatif](images/preservatif-darvador.jpg)<!-- .element width="30%" -->

* [CNIL / Authentification par mot de passe : les mesures de sécurité élémentaires](https://www.cnil.fr/fr/authentification-par-mot-de-passe-les-mesures-de-securite-elementaires)
* [ANSSI / Recommandations relatives à l'authentification multifacteur et aux mots de passe](https://www.ssi.gouv.fr/guide/recommandations-relatives-a-lauthentification-multifacteur-et-aux-mots-de-passe/)


##  🏦 [Locaux](https://doc.isima.fr/locaux/salles)

![bâtiments](images/batiments.png "batiments")


## 🖥 [Salles informatiques](https://doc.isima.fr/pedagogie/salles)

* Salles PC :
    * Dual boot Windows / Linux
* Salles Spécialisées :
    * systèmes et réseaux
    * physique
    * électronique
    * circuits
    * robotique
    * réalité virtuelle


## 🖥 [Salles informatiques](https://doc.isima.fr/pedagogie/salles)

* Salles Tx :
    * D013 et 💻 D018  (libre accès de 7h30 à 18h45 en semaine)
* Laboratoire de langue
    * G116 (Bâtiment Pôle Commun)


## 🖥 [Serveurs pédagogiques](https://doc.isima.fr/pedagogie/serveurs)

* TSE1 (MS Windows server 2016R2)
* Ada (GNU/Linux Debian 12)
* Turing (GNU/Linux Debian 12)
* Exam (GNU/Linux CentOS 6 isolé)
* 🔌 Connexion possible
    * [Ligne de commande (ssh/kerberos)](https://doc.isima.fr/services/acces-distant/ssh/kerberos)
    * [Bureau distant (RDP)](https://doc.isima.fr/services/acces-distant/rdp)
    * [Bureau distant (Guacamole)](https://doc.isima.fr/services/acces-distant/guacamole/)


## 🖥 [VM Perso](https://doc.isima.fr/pedagogie/vm)

* Debian 12 / Cinnamon
* 🔌 Connexion possible
    * [Ligne de commande (ssh/kerberos)](https://doc.isima.fr/services/acces-distant/ssh/kerberos)
    * [Bureau distant (RDP)](https://doc.isima.fr/services/acces-distant/rdp)
    * [Bureau distant (Guacamole)](https://doc.isima.fr/services/acces-distant/guacamole/)

* 😴 Eteinte toutes les nuits à 2h00 du matin
    * ▶ start / ◼ stop
        * https://my.isima.fr/vm


## 🕵🏼 [Authentification](https://doc.isima.fr/authentification)

* 🔌 Compte UCA pour **tous les services**
    * [Bureau distant (Guacamole)](https://doc.isima.fr/services/acces-distant/guacamole/), 
        [Forges logicielles](https://doc.isima.fr/services/gitlab), 
        [Gestionnaire de ticket](https://doc.isima.fr/services/gestsup) ...
    * ouverture de session
    * [impressions](https://doc.isima.fr/services/imprimantes)

* 😥 Pas de compte = pas de service
    * ⏰ S'inscrire avant le 11 septembre!


## 🖨 [Impressions](https://doc.isima.fr/services/imprimantes/#impression-pour-les-etudiants)

 * Facturées directement sur la carte [IZLY](https://dsi.uca.fr/catalogue-de-services/impression-izly)
 * Toutes les imprimantes de l'UCA sont utilisables
    * File d'impression itinérante **FollowMe**
    * le copieur de la salle D013 
        * permet de recharger son compte [IZLY](https://dsi.uca.fr/catalogue-de-services/impression-izly)


## 🌃 [Accés distant](https://doc.isima.fr/services/acces-distant/)

* Connexion de puis l'extérieur
    * [VPN](https://doc.isima.fr//services/acces-distant/vpn)
        * 🔌 Connexion possible
            * [Ligne de commande (ssh/kerberos)](https://doc.isima.fr/services/acces-distant/ssh/kerberos)
            * [Bureau distant (RDP)](https://doc.isima.fr/services/acces-distant/rdp)
    * [Bureau distant (Guacamole)](https://doc.isima.fr/services/acces-distant/guacamole/)

Pour les 🖥 [Serveurs pédagogiques](https://doc.isima.fr/pedagogie/serveurs), 🖥 [Salles informatiques](https://doc.isima.fr/pedagogie/salles), 🖥 [VMs Persos](https://doc.isima.fr/pedagogie/vm)


## 💾 [Données (~/shared)](https://doc.isima.fr/stockage/shared)

* 5Go
* != drive.uca.fr
* dirs.local.isima.fr
    * [Ligne de commande (ssh/kerberos)](https://doc.isima.fr/services/acces-distant/ssh/kerberos)
    * nfs
    * samba
* Montage automatique PARTOUT
    * Répertoire `~/shared` sous GNU/Linux et MacOS
    * Montage réseau `P:\\` sous MS Windows 


## 💀 WARNING!!

* 🤦 Tout peut être réinstallé à tout moment
    * Les salles machine
    * Les serveurs péda
    * Votre VM Perso
    
* 🙇🏼 `~/shared` et `P:\\` est le seul répertoire sauvegardé
    * Tout le reste peut disparâitre
        * A tout moment!!


## 🕸 Services

* [Forges logicielles (Gitlab)](https://doc.isima.fr/services/gitlab): https://gitlab.isima.fr
* [Prise de notes (Hedgedoc)](https://doc.isima.fr/services/hedgedoc): https://hedgedoc.isima.fr
* [Page web personnel](https://doc.isima.fr/services/hebergement-web/perso): https://perso.isima.fr/~login
* Une plateforme de Cloud Computing 
    * contacter le [service CRI ISIMA/LIMOS](https://doc.isima.fr/support/cri)
* Plateforme HPC (accès restreint)
    * contacter [Hélène Toussaint](mailto:helene.toussaint@isima.fr)
    * module ED d'apprentissage au LIMOS


## 🎁 Logiciels

* Microsoft Office et Office 365
    * https://doc.isima.fr/Imagine/office/
* Azure dev tools for teaching
    * https://doc.isima.fr/Imagine/imagine/


## 👋 [Cadres légaux](https://doc.isima.fr/support/cadres)

* [règlement intérieur de l'ISIMA](https://ent.uca.fr/moodle/mod/page/view.php?id=263490)
* [Charte générale à l'usage des ressources numériques / UCA](https://www.uca.fr/medias/fichier/deliberation-ca-2017-10-27-21-charte-des-usages-numeriques-et-des-administrateurs-techniques-annexe_1509372123880-pdf)
* Cadre national [ANSSI](https://www.ssi.gouv.fr/en/), [PSSIE](https://www.ssi.gouv.fr/entreprise/reglementation/protection-des-systemes-dinformations/la-politique-de-securite-des-systemes-dinformation-de-letat-pssie/)
    * Sanctions disciplinaires et pénales
* 🙏 Soyez "fair"   
    * si vous trouvez une vulnérabilité, 😎 signalez la au [service CRI ISIMA/LIMOS](https://doc.isima.fr/support/cri)


##  💬 Des questions ????

![](https://media.giphy.com/media/hSKk6hx9OzomRvFRcJ/giphy.gif)<!-- .element style="margin: 20px" -->

📘 **https://doc.isima.fr** 🎫 **https://support.isima**

🤗 **[Typos et suggestions](https://gitlab.isima.fr/vimazeno/blog.limos.fr)**
