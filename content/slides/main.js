
"use strict";

Reveal.addEventListener( "ready", (event) => {
    
    let user = ''
    
    if(document.location.href.indexOf("?") != -1) {
        user = document.location.href.substring(
            document.location.href.indexOf("?")+1,
            document.location.href.indexOf("#")
        );
    }

    console.log(user);

    let as = document.getElementsByTagName("a");

    for (let a of as) {

        let link = a.href;

        if(
            a.classList == '' 
            && a.href != '' 
            && !a.href.includes('javascript:') 
        ) {

            //console.log(link);

            let url = new URL(link);
                
            if (
                url.origin == "http://dv.wa"
                && user != ""
            ) {
                a.href = "http://vm-" + user + ".local.isima.fr" + url.pathname + url.search;
                a.innerHTML = "http://vm-" + user + ".local.isima.fr" + url.pathname + url.search;
                a.target = "_blank";
            }
        }
    }

    for (let code of document.getElementsByTagName("code")) {

        if (user != "") {
            code.innerHTML = code.innerHTML.replaceAll("dv.wa", "vm-" + user + ".local.isima.fr");
        }

    }
    
});