# Sécurité & navigateurs

![browsers](images/browsers/browsers.jpg)


## Les mots de passes

1. ça ne se prête pas
2. ça ne se laisse pas traîner à la vue de tous
3. ça ne s'utilise qu'une fois
4. si ça casse on remplace immédiatement
5. un peu d'originalité ne nuit pas
6. la taille compte
7. il y a une date de péremption
8. mieux vaut les avoir toujours avec soi


### Question d'hygiène!

![preservatif](images/passwords/preservatif-darvador.jpg)<!-- .element width="30%" -->

[CNIL / Authentification par mot de passe : les mesures de sécurité élémentaires](https://www.cnil.fr/fr/authentification-par-mot-de-passe-les-mesures-de-securite-elementaires)


## [<i class="fa fa-firefox" aria-hidden="true"></i> Firefox](https://www.mozilla.org/fr/firefox/new/)

* Stocke les mots de passes en clair
  * *Préférences > Sécurité > Identifiants enregistrés*

![/o\](images/passwords/password.firefox.png)<!-- .element style="width: 500px" -->


## [<i class="fa fa-chrome" aria-hidden="true"></i> Chrome](https://www.google.fr/chrome/browser/desktop/)

* Stocke les [mots de passe en ligne](https://passwords.google.com/settings/passwords)
  * non [souverain](sovereignty.html)
    * comme [LastPass](https://www.lastpass.com/fr), [Dashlane](https://www.dashlane.com/), [iCloud](https://www.icloud.com/), ...

![/o\](images/passwords/password.google.png)


## [KeePassXC](https://keepassxc.org/) <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>

* Stocke les mots de passes dans une bases données chiffrée
  * un mot de passe à retenir
* Portage officieux de keepass sous [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
    * [KeepassXC – A cross-platform community fork of KeepassX](https://news.ycombinator.com/item?id=13468261)
    * cross-platform (<i class="fa fa-windows" aria-hidden="true"></i> [Windows](http://slides/slides/privacy/passwords.html#/0/11), <i class="fa fa-apple" aria-hidden="true"></i> [Mac OS X](http://slides/slides/privacy/passwords.html#/0/11), <i class="fa fa-linux" aria-hidden="true"></i> [Linux](http://slides/slides/privacy/passwords.html#/0/11), <i class="fa fa-android" aria-hidden="true"></i> [Android](http://slides/slides/privacy/passwords.html#/0/14))


### [<i class="fa fa-firefox" aria-hidden="true"></i> <i class="fa fa-chrome" aria-hidden="true"></i> KeePassXC-Browser add-on](https://keepassxc.org/docs/keepassxc-browser-migration/)

* Pré-requis
  * [KeePassXC](https://keepassxc.org/) avec l'intégration aux navigateurs activée
* Cherche un couple login mot de passe dans [KeePassXC](https://keepassxc.org/) à partir de l'url consultée
* Sauvegarde les nouvelles entrées dans [KeePassXC](https://keepassxc.org/)


## communication sur le web

![HTTP=carte postale](images/browsers/en-clair.png)


## communication chiffrée sur le web

![HTTPS=enveloppe cachetée avec sceau](images/browsers/chiffrement-symetrique.png)


![images/borwsers/pernaud.jpg](images/browsers/pernaud.jpg)


![images/borwsers/phishing_login.png](images/browsers/phishing_login.png)<!-- .element style="width: 80%" -->


## Schéma d'url

![schéma d'url](../1337/images/http/urls.png "schéma d'url")<!-- .element style="width: 100%" -->


## urls trompeuses

* <a href="http://33.32.323.22:8080/cart?add=1345">pay with paypal</a>
* https://www.visa.com:1337@33.32.323.22:8080/cart?add=1345
* https://visa.com:UserSession=56858&useroption=879@42.216.64.464
* https://www.visa.com@33.32.323.22
* https://www.v-i-s-a.com


## prévisualisation des urls

![images/browsers/previews.png](images/browsers/previews.png)


## se préserver

* bien gérer ses mots de passes
  * [https://haveibeenpwned.com/](https://haveibeenpwned.com/)
* n'utiliser que des services [S] (https,imaps, smtps, ...)
* vérifier le nom de domaine du service
* prévisualiser les liens


## cookie

![images/browsers/cookie.png](images/browsers/cookie.png)

source: [Zoom sur les cookies](https://c-marketing.eu/zoom-sur-les-cookies/)


## first-party cookies

* ouverture de session
* authentification d’un membre
* paniers de commande sur un e-shop
* recherches sur un site


## 3rd-Party cookies (externes)

* boutons de partages de réseaux sociaux
* publicités installées via des régies et plateformes publicitaires
* outils de statistique


## 3rd-Party cookies 
#### données récoltées

* Les données personnelles si accessible (âge, sexe, localisation)
* Les sites Web visités depuis lesquels est généré le cookie
* Les sous-pages visitées sur ce site Web
  * recherche
  * click
* Le temps passé sur la page et les sous-pages
* publicités cliquées


## tracking-cookie

[![images/browsers/ad.png](images/browsers/ad.png)](images/browsers/ad.png)

source: [Zoom sur les cookies](https://c-marketing.eu/zoom-sur-les-cookies/)


## tracking

![images/browsers/tracking-cookie.png](images/browsers/tracking-cookie.png)

source: [Zoom sur les cookies](https://c-marketing.eu/zoom-sur-les-cookies/)

tester son navigateur: [https://coveryourtracks.eff.org](https://coveryourtracks.eff.org/)


## [RGPD](https://www.cnil.fr/fr/cookies-et-traceurs-que-dit-la-loi)

L'article 5de la directive 2002/58/CE modifiée en 2009 pose le principe : 

* **first-party cookies** consentement préalable de l'utilisateur avant le stockage et ou l'accès aux informations dans le navigateurs
* **3rd-Party cookies** sauf si ces actions sont strictement nécessaires à la fourniture d'un service de communication


## se préserver

* [No Script](https://noscript.net/)
* [AdBlock Plus](https://adblockplus.org/)
* [Tor](https://www.torproject.org/)