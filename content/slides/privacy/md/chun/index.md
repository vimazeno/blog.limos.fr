<!-- .slide: data-background="images/chun/white_rabbit.png" style="color: white;" class="trsp bottom" -->
#### Une contre histoire de l'utopie numérique
connaître le passé pour comprendre le présent

Note:
- je ne suis pas historien
- l'histoire officielle
    - les jeunes connaissent peu l'histoire
    - et les vieux, qui ont vécu cette histoire 
        - sont réputés moins "maitriser" l'outil 
    - elle est souvent racontée de manière incomplète ou faussement objective
        - proposition d'une grille de lecture 
            - idéologique et alternative
            - technocritique: ni luddite, no techno béa
                - métier jacquard / les canuts à Lyon / les luddites au UK
                - clodo
            - c'est une contre histoire 
                - c'est volontairement conspi
                - c'est un hommage
                    - kill your idols    
- chargée de ma propre idéologie
    - j'ai essayé sourcer au mieux
        - livres
        - liens
        - vidéos
        - posdcasts
- sans obligation d'achat
- je ne répète pas je donne les réponses aux questions qui sont posées sur la feuille que vous avez prises
- j'ai plein de trucs à dire
- je vais aller vite
- donc je pointe du doigt les bavards et ils sortent discrètement
    - merci
- Maintenant je vous invite à me suivre dans le terrier du lapin blanc
    - on verra si on ressort transformer de ce voyage au pays des merveilles

----

## (Res)sources

* [🎥 "white Rabbit " par Jefferson Airplane](https://www.youtube.com/watch?v=WANNqr-vcx0)
    * [🔗 Grace Slick](https://fr.wikipedia.org/wiki/Grace_Slick#Une_personnalité_explosive)
    * [🔗 Summer of love](https://en.wikipedia.org/wiki/Summer_of_Love)
* [📖 Aux sources de l'utopie numérique](https://www.librairielesvolcans.com/livre/9782376620242-aux-sources-de-l-utopie-numerique-de-la-contre-culture-a-la-cyberculture-stewart-brand-un-homme-d-influence-fred-turner/)
* [📖 Internet année zéro](https://www.librairielesvolcans.com/livre/9791097088347-internet-annee-zero-de-la-silicon-valley-a-la-chine-naissance-et-mutations-du-reseau-jonathan-bourguignon/)
* [📖 L'utopie déchue](https://www.librairielesvolcans.com/livre/9782213710044-l-utopie-dechue-une-contre-histoire-d-internet-xve-xxie-siecle-treguer-felix/)
* [📖 Histoire illustrée de l'informatique](https://www.librairielesvolcans.com/livre/9782759827046-histoire-illustree-de-l-informatique-3e-edition-emmanuel-lazard-pierre-mounier-kuhn/)
* [🔗 Timeline of Computer History](https://www.computerhistory.org/timeline/1933/)
* [🎧 Contre-histoire de la philosophie](https://open.spotify.com/playlist/0xTgnsqPoZWzbMm7bZzqje)
* [📖 Antimanuel de la philosophie](https://www.librairielesvolcans.com/livre/9782842917418-antimanuel-de-philosophie-michel-onfray/)

----

## (Res)sources

* [🎥 Histoire de l'informatique (première partie)](https://www.youtube.com/watch?v=dJdiSN9q5QE)
* [🎥 Histoire de l'info (seconde et dernière partie)](https://www.youtube.com/watch?v=NNxAKALRePo)
* [🎥 Une brève histoire de l'informatique, de 1945 à nos jours](https://www.youtube.com/watch?v=dcN9QXxmRqk)
* [🎬 Halt and Catch Fire](https://fr.wikipedia.org/wiki/Halt_and_Catch_Fire_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e))
* [🔗 Histoire & culture numérique](https://edu.ge.ch/site/ressources-enseignement-histoire/category/histoire-et-numerique/histoire-de-linformatique/

---

<!-- .slide: data-background="images/chun/mem_info.png" -->
## de quoi parle-t-on?<!-- .element: class="fragment" -->

![informatique](images/chun/informatique.jpg)<!-- .element: class="fragment" -->

Note:
- J'ai mis plus de 25 ans à le lire comme ça 
- traiter automatiquement l'information
    - à une vitesse > celle de l'humain 
    - ordinateur n'a aucune intelligence
        - on est d'accord la dessus?
- quand a ton commencer à vouloir traiter l'information automatiquement?

---

#### -500 - Le [Boulier](https://fr.wikipedia.org/wiki/Boulier#:~:text=Le%20boulier%20est%20un%20abaque,repr%C3%A9sentation%20du%20nombre%2037%20925.)

![boulier](images/chun/boulier.jpg)

Note:
- je remonte au boulier histoire qu'on ait bien toute l'histoire, pas que celle d'il y a 15 ans
    - une éternité j'imagin pour vous
- cet instrument était utilisé par des peuples très largement séparés 
    - Étrusques, les Grecs, les Égyptiens, les Indiens, les Chinois et les Mexicains 
    - on peut penser qu'il a été inventé indépendamment dans différents endroits
- toujours utilisé en Chine pour le calcul de grand nombre
    - concours

---

#### 1642 - La [Pascaline](https://fr.wikipedia.org/wiki/Pascaline)

![pascaline](images/chun/pascaline.jpg)<!-- .element: style="width: 70%" -->

Note:
- Invention de Blaise Pascal 
    - Clermontois
- ne fait que des additions et des soustractions
- existe en 2 exemplaires
    - aucune distribution à l'échelle
        - c'est un machine informatique confidentielle
        - une prouesse technique de l'époque
        - il y en a bien d'autres dont ...

---

<!-- .slide: data-background="images/chun/babbage.jpg" data-background-position="bottom" style="color: white;" class="trsp" -->
#### 1834 - La [Machine analytique](https://fr.wikipedia.org/wiki/Machine_analytique)

[![](images/chun/ada.png)<!-- .element: class="fragment" -->](https://fr.wikipedia.org/wiki/Ada_Lovelace)

Note:
- la machine analytique de Charles Babbage
- prévue pour produire des tables de calcul
    - pré calcul
    - préoccupation majeure jusqu'à la micro informatique
- un exemplaire
- elle fait les 4 opérations
- il passe sa vie à la construire et l'améliorer
- et une femme brilante en maths Ada Lovelace pense à comment faire des séquences d'opérations
    - début de la programmation via des cartes perforées
    - Ada Lovelace est connue comme la première développeuse (tout genre confonds ;)).

----

## (Res)sources

* [La pionnière de l'informatique - ADA LOVELACE ET LA MACHINE ANALYTIQUE](https://tube-arts-lettres-sciences-humaines.apps.education.fr/w/67a3a535-921b-4b50-9d2f-14fffcb8cf98)

---

<!-- .slide: data-background="images/chun/caisse.jpg" style="color: white;" class="trsp" -->
#### 1879 - Les [Caisses enregistreuses](https://fr.wikipedia.org/wiki/Caisse_enregistreuse)

Note:
- première machine informatique
    - invention états unienne et non européenne
    - industrialisée
        - accessible à un plus large public    
    - gain de précision
    - gain de temps
- (automatisation + optimisation de process) 
    - pour un large public 
        - ce qu'est l'informatique encore en 2024

---

<!-- .slide: data-background="images/chun/carte_perforee.jpg" style="color: white;" class="trsp" -->
#### 1890 - [Machine mécanographique Hollerith](https://en.wikipedia.org/wiki/Tabulating_machine)

[![Machine mécanographique Hollerith](images/chun/hollerith.jpg)<!-- .element style="width: 90%" class="fragment" -->](https://fr.wikipedia.org/wiki/IBM)

Note:
- appelée aussi tabulatrice
- outil d'analyse statistiques (reporting) mécanique à base de carte perforée et de trieuse
    - la statistique est un allié de choix pour l'optimisation
    - industrialisé la production d'indicateurs
    - utilisé pour le 11ème recensement Américain puis partout
    - les entrerpises du monde entier et en particulier les US
- Brevet récupéré par Watson qui forme IBM (NYC)
    - petit aparté contexte de l'époque
        - vient juste après Rockfeller, Carnegie, Morgan en terme de fortune
        - capitalisme sauvage de la fin 
            - suite et fin de la révolution industriel
            - peu de règlementation sur 
                - le travail 
                    - salaires minimum
                    - travail des enfants
                - la concurence 
                - anti trust
        - économie monopolistique
            - peu de gens possèdent énormément
        - génère des crises à répétition pour fin XIX° début XX° au US
        - mais les Hypergagnants préfèrent monter des oeuvres philantropique, plutôt que de payer des taxes / impôts à l'état
        - c'est le libéralisme
- Watson donc!
    - vendez des machine hollerite et les services associés
    - une partie de sa fortune a été faite pendant la guerre en réussissant à déguiser IBM en société allemande (DEHOMAG)
    - un montage financier permet de récolter les recettes en passant par la suisse dans toute l'europe envahie
    - proche de Roosvelt IBM ne sera pas inquiétée pour sa collaboration à l'après guerre
- pourtant la collaboration entre IBM et le régime nazi fut quasiment continue pendant la durée de la 2ème guerre mondiale
    - pour gérer l'effort de guerre
    - pour le recensement des juifs (1/2 juifs, 1/4 de juis, 1/8 de juifs) 
    - pour la gestion des camps de travail et de concentration dans le cadre de la solution finale
        - rarement précisé dans ce que j'ai pu lire ou voir
- La mécanographie rend les organisations efficientes IBM a permis un passage à l'échelle du génocide en un temps record
- Après la guerre Les accords de Bretton Woods de Keyns sous Roosvelt vont mettre à contribution les plus riches
- on entre dans l'ère du capitalimse régulé
- éducation => recherche => croissance
- avancées sociales & sanitaires


----

## (Res)sources

* [📖 IBM et l'holocauste](https://www.babelio.com/livres/Black-IBM-et-lHolocauste-Lalliance-strategique-entre-/208921)
    * [Quand IBM collaborait avec les nazis](https://www.lemonde.fr/archives/article/2001/02/13/quand-ibm-collaborait-avec-les-nazis_4182310_1819218.html)
    * [IBM, fournisseur trop zélé du IIIe Reich](https://www.liberation.fr/evenement/2001/02/13/ibm-fournisseur-trop-zele-du-iiie-reich_354464/)
    * [«Les nazis n'avaient nul besoind'IBM pour leurs machines»](https://www.liberation.fr/futurs/2001/02/23/les-nazis-n-avaient-nul-besoind-ibm-pour-leurs-machines_355673/)
* [🔗 New Deal](https://en.wikipedia.org/wiki/New_Deal)
* [🔗 Keynésianisme](https://fr.wikipedia.org/wiki/Keyn%C3%A9sianisme)
* [🔗 Accords de Bretton Woods](https://fr.wikipedia.org/wiki/Accords_de_Bretton_Woods)
* [🎥 L'horreur existentielle de l'usine à trombones](https://www.youtube.com/watch?v=ZP7T6WAK3Ow)

---

<!-- .slide: data-background="images/chun/campus_stanford.jpg" style="color: white;" class="trsp" -->
#### 1824-1893 - [Stanford University](https://en.wikipedia.org/wiki/Stanford_University)

[![Lelan Stanford](images/chun/lelan_stanford.jpg)<!-- .element style="width: 40%" class="fragment" -->](https://fr.wikipedia.org/wiki/Leland_Stanford)

Note:
- le business notamment international s'organise aux USA
    - parmis les Hypergagnants de l'époque
        - Lelan Stanford
            - possède les chemins de fer
- à 28 ans iIl quitte NYC 
    - fonde une véritable  école de pensée entreprenariale en Californie
        - innover
        - aller vite
        - droit à l'erreur
- Stanford est un disrupteur 
    - Méthode Palo Alto pour l'élevage de chevaux
        - accélération du cycle d'élevage basée sur des données
- fondateur de l'université 1885 
    - il amène les capitaux en Californie
        - à l'est à l'époque
        - c'est la conquête de l'ouest (western il n'y a pas si longtemps)
- fondateur de la ville de Palo alo Alto en 1894 
- de fait le fondateur de la Silicon Valley

----

## (Res)sources

* [🎥 Leland Stanford : La vie controversée du magnat des chemins de fer de l’Ouest américain](https://www.youtube.com/watch?v=RM92AWoDJeQ)
* [🎥 Capitalisme américain - Le culte de la richesse](https://boutique.arte.tv/detail/capitalisme-americain-le-culte-de-la-richesse?srsltid=AfmBOorOSvvuScQRL-AaXlHo_uT1DwHY_14uJ__3iuYA6zzPsj5GNp8r)
* [🎥 Capitalisme - Les ultra riches contre l'humanité](https://www.youtube.com/watch?v=_z_MPg38RzE&t=1644s)

---

<!-- .slide: data-background="images/chun/tube_electronique.jpg" style="color: white;" class="trsp bottom" -->
#### 1906 - [le tube électronique](https://fr.wikipedia.org/wiki/Historique_des_tubes_%C3%A9lectroniques)

Note:
- e tube électronique
- la silicon valley à l'époque n'a pas encore son nom, ni son objet
- les tubes assurent l'amplification et la transformation du signal électrique
    - ce sont des circuits électrique pouvant implémenter de la logique
- l'informatique passe de mécanographique à électronique
    - gain de vitesse
    - perspective simplification dans l'utilisation des calculateurs
    - toujours encombrant
    - peu fiable 
        - on doit les remplacer souvent

---

<!-- .slide: data-background="images/chun/transistors.jpg" style="color: white;" class="trsp bottom" -->
#### 1937 - [le transistor](https://en.wikipedia.org/wiki/Transistor)

Note:
- inventé par Bell Labs (New Jersey)
    - on reparle ;)
- les avancées techniques ouvrent des perspectives
    - miniaturisation
    - fiabilité
    - plus de vitesse

----

## Res(Sources)

* [🎧 Épisode 1/6 : Transistor : la révolution à sauts de puce](https://www.radiofrance.fr/franceculture/podcasts/entendez-vous-l-eco/transistor-la-revolution-a-saut-de-puce-9759644)
* [🎥 cocadmin / Les ordi étaient dla merde avant que ce génie arrive (Morris / tsmc)](https://www.youtube.com/watch?v=z9f12rod30Q&t=5s)

---

<!-- .slide: data-background="images/chun/hp.jpeg" style="color: white;" class="trsp bottom" -->
#### 1939 - [Hewlett Packard](https://en.wikipedia.org/wiki/Hewlett-Packard)

Note:
- les pionniers du silicium fleurissent en californie
    - notamment grâce à 2 anciens de l'Université de Stanford 
    - vendent des oscillateurs (notamment pour Walt Disney)
        - puis mini-ordinateurs (calculette électronique)
- la Silicon Valley a sa matière (le silicum - la matière première du transistor) mais pas encore son nom
    - à Palo Alto 
- naissance de la légende du garage incubateur
    - Story telling typique de la Silicon valley
    - alimente la mythologie de l'entrepreneur
    - les grandes success story partent de rien
        - nan! très rarement

----

## Res(Sources)

* [🎥 HP Origins - Hewlett Packard Documentary](https://www.youtube.com/watch?v=Iqv6DhtLay4)
* [🎥 L'histoire de HP 1939](https://www.youtube.com/watch?v=0sb5sNt_Ifg)

---

<!-- .slide: data-background="images/chun/eniac.jpg" style="color: white;" class="trsp" -->
#### 1943 - L'[ENIAC](https://fr.wikipedia.org/wiki/ENIAC)

Note:
- Turing Complete
    - permet d'implémenter une machine de Turing
        - offre une logique universel de programmation
- à base de lampe électronique
    - origin du mot bug
        - quand on elever les papillons
- calculateur compliqués à programmer
    - il faut recabler sans cesse
- pas de mémoire
    - ajout en 1947 suite au rapport John von Neumann qui théorise l'architecture de l'ordinateur telle que nous la connaissons aujourd'hui
- fabriqué par des universitaires
- Influencera le [Mark I](https://en.wikipedia.org/wiki/Harvard_Mark_I) D'IBM
    - utilisé par John Von Neumann dans le cadre du projet Manhattan
        - rarement précisé

----

## Res(Sources)

* [🎥 NSA releases copy of internal lecture delivered by computing giant Rear Adm. Grace Hopper](https://www.nsa.gov/Press-Room/Press-Releases-Statements/Press-Release-View/Article/3884041/nsa-releases-copy-of-internal-lecture-delivered-by-computing-giant-rear-adm-gra/)
* [🔗 Turing Machine – Working Lego Computer](https://ideas.lego.com/projects/10a3239f-4562-4d23-ba8e-f4fc94eef5c7?utm_source=twitter)

---

<!-- .slide: data-background="images/chun/Alan_Turing.png" style="color: white;" class="trsp bottom" -->
### 1942 - [décryptage d'enigma](https://fr.wikipedia.org/wiki/Enigma_(machine))

Note:
- [La machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing) pense l'algorithmique et lui donne un représentation mécanique
- Alan Turing est énaglais et il a aussi
    - Décryptaer enigma le système de transcription chiffré des nazis
        - on estime qu'il a écourté la guerre de 2 ans
    - on lui doit le test de Turing 
        - qui vise à voir sir une intelligence artificilelle peut duper un humain

- homosexuel réprimé par son propre gouvernement il se suicide à 40 ans en mangeant une pomme empoissonnée
- ses travaux ne sont reconnus qu'à leur déclassification secret défense vers 1974 
    - le logo d'Apple serait un hommage à cette fin tragique

----

## (Res)sources

* [🔗 Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing)
* La vie d'Alan Turing
    * [🎧 Épisode 1/4 : Enigma, la guerre du code](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/enigma-la-guerre-du-code-2085217)
    * [🎧 Épisode 2/4 : Des marguerites à l’ordinateur](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/des-marguerites-a-l-ordinateur-1510370)
    * [🎧 Épisode 3/4 : Le bug](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/le-bug-5843912)
    * [🎧 Épisode 4/4 : Les mythologies d’Alan Turing](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/les-mythologies-d-alan-turing-5870730)
* [🎬 The Imitation Game](https://en.wikipedia.org/wiki/The_Imitation_Game)

---

<!-- .slide: data-background="images/chun/JohnVonNeuman.jpg" style="color: white;" class="trsp bottom" -->
### 1945 - [Architecture de von Neumann](https://fr.wikipedia.org/wiki/Architecture_de_von_Neumann)

Note:
- la "Théorie des jeux"
    - émergences des sciences socials, hummaines, molles
    - interdisciplinarité
- Rapport Von Neumann 
    - conception moderne de l'ordinateur
- calcul la hauteur optimale de l'explosion pour assurer un impact optimum de la première bombe A avec le Mark I
    - Projet Manhattan (complètement oublié dans le film oppenheimer)

----

## Res(Sources)

* [🔗 Architecture de von Neumann](https://fr.wikipedia.org/wiki/Architecture_de_von_Neumann)
* [🔗 John von Neumann](https://fr.wikipedia.org/wiki/John_von_Neumann)

---

<!-- .slide: data-background="images/chun/NorbertWiener.png" style="color: white;" class="trsp bottom" -->
## 1947 - [Cybernétique](https://fr.wikipedia.org/wiki/Cybern%C3%A9tique)

Note:
- Norbert Wiener mon pref
- cybernétique
    - Nouveau paradigme
    - rupture dans l'appréhension du monde 
        - s'intéresse à l'information dans les systèmes
            - et à aux rétroactions provoquées par ce flux d'information 
                - oebjctif d'atteindre un état (stable, croissance, etc ...)
        - les systèmes peuvent interagir entre eux
        - fin du cartésianisme - découpages en problèmes silotés
        - palces aux Mathématiques appliquées
            - aux machines / robots
            - aux vivants
            - aux cerveaux
            - aux sciences
                - physique bien sur 
                - sociale (Wiener n'était pas chaud)
    - La cybernétique va notamment beaucoup influencé la pensée néocommunaliste
        - mais aussi celle de la DARPA 
            - et finalement du monde entier
- Norbert Wiener comprend vite que
    - celui qui contrôle l'information contrôle le système 
- John Von Neummann a été un acteur majeur du projet Manhattan (oubli dans Oppenheimer)
    - bellissiste il sait est complètement à l'aise avec la finalité dela bombe A
- Norbert Wiener pacifiste refuse de participer

----

## (Res)sources

* [🔗 Norbert Wiener](https://en.wikipedia.org/wiki/Norbert_Wiener)
* [🎧 La double vie de Norbert Wiener : le père de la cybernétique !](https://www.radiofrance.fr/franceculture/podcasts/la-marche-des-sciences/la-double-vie-de-norbert-wiener-le-pere-de-la-cybernetique-1832904)
* [📖 Cybernétique et société (*Norbert wiener*)](http://www.seuil.com/ouvrage/cybernetique-et-societe-norbert-wiener/9782757842782)

---

<!-- .slide: data-background="images/chun/claude_shannnon.png" style="color: white;" class="trsp bottom" -->
### 1948 - [Théorie de l'information](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27information)

Note:
- théorie de l'information
- notion d'entropie issue des lois de la thermodynamique
- crypto pendant la guerre

----

## Res(Sources)

* [🔗 Claude Shannon](https://en.wikipedia.org/wiki/Claude_Shannon)

---

#### 1954 - [TRADIC](https://fr.wikipedia.org/wiki/TRADIC) par [Bell Labs](https://fr.wikipedia.org/wiki/Laboratoires_Bell)

![TRADIC](images/chun/TRADIC.jpg)<!-- .element style="width: 40%"-->

Note:
- Premier ordinateur à base de transistor
- Réalisé par Bell Labs (New Jersey) qui produira également
    - 29 000 brevets jusqu'en 2012
    - C, C++
    - UNIX
    - la cellule photoélectrique
    - le laser 
    - la fibre optique ...

---

<!-- .slide: data-background="images/chun/moore.jpg" style="color: white;" class="trsp" -->
#### 1965 - [Loi de Moore](https://fr.wikipedia.org/wiki/Loi_de_Moore) / 1968 - [Intel](https://fr.wikipedia.org/wiki/Intel)

Note:
- le transistor amène d'autres perspectives industrielles
- doublement du nombre de transistors présents sur une puce de microprocesseur tous les deux ans
- limité par les lois de la physique (2015)
- pas une loi  scientifique
    - loi économique de planification
        - on la dit morte
        - mais on a trouvé des astuces pour qu'elle perdure
- Moore fondera Intel en 1968 avec un plan de croissance auto réalisateur :D
    - à Santa Clara
    - La Silicon Valley devient enfin la Silicon Valley
        * nom trouvé par des journalistes de l'époque

---

<!-- .slide: data-background="images/chun/IBM_System.jpg" style="color: white;" class="trsp" -->
#### 1966 - IBM System

Note:
- qui n'est pas dans la Silicon Valley
- premier ordinateur vendu
    - La notion de premier est tout à fait relative
        - plus grand public
        - l'informatique rentre dans les entreprises
            - jusque là restées à la mécanogreaphie
- le processeur
    - 34,500 instructions par second
- la révolution informatique est en marche!
- Quelques éléments de contexte pour comprendre la suite
    - Au delà des aspects techniques et financiers

---

<!-- .slide: data-background="images/chun/BuckminsterFuller.png" style="color: white;" class="trsp" -->
#### 1967 - [Design](https://fr.wikipedia.org/wiki/Design)  / [Buckminster Fuller](https://en.wikipedia.org/wiki/Buckminster_Fuller)

Note:
- Buckminster Fuller se fait connaître avec le Dôme géodésique
    - popularise le design
    - une façon de résoudre des problèmes ou d'améliorer les choses
        - initialement avec une vision durable (au sens écolo)
        - suggère une émancipation de l'humain via le Design
- Incarné dans les années 60-70 par la figure du Designer Universel
    - celui qui comprendrait et produirait des moyens d'émancipation pour l'humanité
- le Design par sa force créatrice est très largement liés aux arts

---

<!-- .slide: data-background="images/chun/MarshallMcLuhan.png" style="color: white;" class="trsp" -->
#### 1967 - [Village planétaire](https://fr.wikipedia.org/wiki/Village_plan%C3%A9taire) / [Marshall McLuhan](https://fr.wikipedia.org/wiki/Marshall_McLuhan)

---

#### 1968 - [Whole earth catalog](https://fr.wikipedia.org/wiki/Whole_Earth_Catalog)

[![Whole earth catalog](images/chun/wholeearthcatalog.jpg)<!-- .element style="width: 40%" -->](https://wholeearth.info/p/whole-earth-catalog-fall-1968)
Note:
- Le magazine des néocommunalistes & de la contre culture
- contre culture populaire en adéquation avec la science de 20 ans plutôt
    - horizontalité des savoirs
    - critique de la hiérarchie
    - soustraction / alternative au complexe militaro industriel (guerre froide)
    - foi en la technolgie émancipatrice et le design
- Les néocommunalistes
    - organisation en communautés autonomes
        - micro société
        - soutenues par la technologi et le design
    - blancs / instruis / hippies
    - sex / drugs / Rock'n'roll    
        - Jefferson Airplane
        - Gratefull Dead (on en reparle)
        - Summer of Love
    - les communautés pericliteront rapidement
        - Charles Manson
        - l'idéologie de ces communautés se retouvera bientôt dans les premières communuatés informatiques
            - en ligne (BBS/NewsGroup/Forum) ou non (hacker/club utilisateurs)
        - les néo communalistes ne sont pas tous partis élevés des chèvres ,loin de là
    - Steve Jobs et bien d'autres de sa génération ont été très marqués par ce magazine
- Whole earth catalog
    - c'est un magazine participatif
        - échange sur les communautés
            - surtout sur les technologies émancipatrices et autonomisantes
        - considéré comme l'ancêtre de l'Internet
    - lancé par Steward Brand personnage très influent dans l'histoire numérique (The Well/WIRED/EFF)
    - la photo de la terre (nouvelle à l'époque car prise de la lune) n'est pas anodine

---

<!-- .slide: data-background="images/chun/MotherOfAllDemos.png" style="color: white;" class="trsp bottom" -->
#### 1968 - [The Mother Of All Demos / SRI](https://fr.wikipedia.org/wiki/The_Mother_of_All_Demos)

Note:
- gros financement de la défense
    - compliqué avec ceux qui travaillent dans la recherche

----

## Res(Sources)

* [🎥 The Mother of All Demos, presented by Douglas Engelbart (1968)](https://www.youtube.com/watch?v=yJDv-zdhzMY)
* [🔗 Douglas C. Engelbart](https://de.wikipedia.org/wiki/Douglas_C._Engelbart)


---

<!-- .slide: data-background="images/chun/XeroxParc.jpg" style="color: white;" class="trsp" -->
#### 1970 - [Palo Alto Research Center / Xerox](https://fr.wikipedia.org/wiki/Palo_Alto_Research_Center)

<div style="text-align: center">

![1973/Xerox Alto/2000 exemplaires](images/chun/XeroxAlto.jpg)<!-- .element:  style="width: 350px" -->

#### [1973 - Xerox Alto - 2000 exemplaires<!-- .element:  style="color: white" -->](https://en.wikipedia.org/wiki/Xerox_Alto)

</div><!-- .element: class="fragment" -->

Note:
- début 1970 Xerox réunit des jeunes ingénieurs brillants pour penser la société de demain
    - ils crééent
        - les interfaces graphiques pilotables à la souris
        - le LAN
        - un système de messagerie
        - les imprimantes laser
        - Xerox ne sait pas quoi faire de ces innovations
- gros financement de la DARPA
----

## (Res)sources

* [🔗 The Xerox PARC Visit](https://web.stanford.edu/dept/SUL/sites/mac/parc.html)
* [🎥 Volcamp 2023 - Keynote Jour 1 Xerox Parc](https://www.youtube.com/watch?v=6-DDdgjkPnU&list=PLX-g5EWaxNn43RpSD9EUS-kM_m6FItgK7)

---

#### ~1970 - Les [réseaux informatiques](https://fr.wikipedia.org/wiki/R%C3%A9seau_informatique)

* 1969 - [Arpanet](https://en.wikipedia.org/wiki/ARPANET)
* 1970 - [Xerox PARC](https://fr.wikipedia.org/wiki/Palo_Alto_Research_Center)
* 1972 - [Cyclades / le datagramme](https://france3-regions.francetvinfo.fr/normandie/seine-maritime/rouen/video-les-francais-qui-n-ont-pas-invente-internet-le-rendez-vous-manque-de-l-histoire-de-l-informatique-2868899.html)

![Cyclades](images/chun/cyclades.jpg)

Note:
- L'informatique personnel est pensée et partiellement implémentée
- Le sujet de l'interconnexion des systèmes est lui aussi en route
    - on pense désormais la planète terre comme un système fait de système connectés 
        - depuis le whole earth catalog
- Arpanet
    - version bêta d'Internet
    - Commandité par la DARPA
    - Réalisé par des jeunes universitaires emprunts de contre culture
    - les universités se connectent très vite 
        - le réseau comme interconnexion des savoirs
            - intelligence collective
- Le Parc on vient d'en parler
- Plan calcul en France de De Gaulle, abrogé par Giscard d'Estaing
    - Louis Pouzin inventeur du datagramme
    - VS lobby des réseaux téléphoniques 

----

## (Res)sources

* [🎧 Une histoire de… l'Internet (*Julien Le Bot*)](https://www.radiofrance.fr/franceculture/podcasts/serie-une-histoire-de-l-internet)
* [🎥 Chronologie du réseau Internet](http://www.tiki-toki.com/timeline/embed/137139/6372410394/#vars!date=1954-07-27_20:20:56!)
* [🎥 Une histoire d'Internet (*Laurent Chemla*)](http://www.chemla.org/textes/hinternet.html)
* [🎥 Une contre histoire de l'Internet](https://www.youtube.com/watch?v=MUTABXD8f24)
* [🎬 Cybertraque](https://www.allocine.fr/film/fichefilm_gen_cfilm=23117.html)
* [🎥 The Net: The Unabomber, LSD & the Internet](https://www.youtube.com/watch?v=GY6fb59XFbQ)
* [🎥 Les Français qui n'ont pas inventé Internet](https://www.france.tv/france-3/auvergne-rhone-alpes/la-france-en-vrai-auvergne-rhone-alpes/3021457-les-francais-qui-n-ont-pas-invente-internet.html)

----

## (Res)sources

- Voyage dans les internets
    - [🎧 60/70's - l'écho psychédélique du bout des tuyaux](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/60-70-s-l-echo-psychedelique-du-bout-des-tuyaux-1705776)
    - [🎧 70/80's - cowboys nomades, astragale et feux de camps électriques](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/70-80-s-cowboys-nomades-astragale-et-feux-de-camps-electriques-7094566)
    - [🎧 ~1990 - Un Web et des bulles](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/les-annees-1990-un-web-et-des-bulles-6566075)
    - [🎧 ~2000 - sous les pop-up, l'indépendance](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/les-annees-2000-sous-les-pop-up-l-independance-3189017)
    - [🎧 Un Internet à la dérive. Des internets en résistance](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/un-internet-a-la-derive-des-internets-en-resistance-6466951)

---

#### ~1975 - La micro-informatique

* 1973 - [Micral](https://en.wikipedia.org/wiki/Micral)
* 1975 - [Altair 8800 (Intel)](https://en.wikipedia.org/wiki/Altair_8800) / [Microsoft](https://en.wikipedia.org/wiki/Microsoft) / [Basic](https://en.wikipedia.org/wiki/BASIC_interpreter)
* 1976 - [Apple](https://en.wikipedia.org/wiki/Apple_Inc.)
* 1976 - [Apple I](https://en.wikipedia.org/wiki/Apple_I) / [Homebrew computer club](https://en.wikipedia.org/wiki/Homebrew_Computer_Club)

![Altair 8800](images/chun/Altair8800.png)<!-- .element style="width: 50%" -->

Note:
- époque foisonnante
    - perspectives industrielles
    - pas très user friendly
- Micral
    - Français
- Altair 8800
    - Microsoft se créé pour venddre le Basic sur ce micro
- Homebrew Computer Club
    - club d'échange entre passionné autour des micros
        - Steve Jobs & Wozniac y présente l'Apple I
            - à l'époque ils vendent des blue box
                -  piratage de ligne téléphonique 
                    -  Phreaking captain crunch
    - 2 univers s'oppose déjà
        - les "hackers" au sens éthymologique du terme 
            - c'est à dire bricoleurs et partageurs
                - et pas psécialement dangereux
        - VS Bill Gates 
            - qui a "vu" le business du software 
                - [An Open Letter to Hobbyists](https://en.wikipedia.org/wiki/An_Open_Letter_to_Hobbyists) 
- Notez les génies disrupteurs qui deviendront multimillardaires 
    - en train de tripper devant des LED et des interrupteurs 
        - quand les ingénieurs de Xerox ont déjà inventé l'informatique personnel 3 ans auparavant

----

## Resources

* [🎥 Micral N, le premier micro-ordinateur de l'histoire](https://www.youtube.com/watch?v=ERp5KkRdYRU)

* [🎥 Hackers The History Of Hacking - Phone Phreaking, Cap.Crunch, Wozniak, Mitnick](https://www.youtube.com/watch?v=FufYSx2_6Bg)

---

<!-- .slide: data-background="images/chun/VincentMazenod1.jpg" style="color: white;" class="trsp" -->
## 1977 - Vincent Mazenod

---

#### ~ 1980 - L'informatique personnel

* 1977 - [Apple II](https://en.wikipedia.org/wiki/Apple_II) (4 couleurs)![Macintosch](images/chun/SteveJobs.jpg)<!-- .element style="float: right" -->
* 1978 - [La loi Informatique et Libertés](https://fr.wikipedia.org/wiki/Loi_informatique_et_libert%C3%A9s)
* 1979 - [Margaret Thatcher](https://fr.wikipedia.org/wiki/Margaret_Thatcher)
* 1981 - [compatible IBM](https://en.wikipedia.org/wiki/IBM_PC%E2%80%93compatible) / [MS DOS](https://en.wikipedia.org/wiki/MS-DOS)
* 1981 - [Ronald Reagan](https://fr.wikipedia.org/wiki/Ronald_Reagan)
* 1984 - [Macintosh](https://en.wikipedia.org/wiki/Macintosh_128K)
* 1985 - [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)

Note:
- le marché se met en place et c'est la fête
- Tatcher (UK) et Reagan (US) marquent un virage ultralibérale amplifié par la mondialisation 
    - fin du capitalisme régulé on dérégule pour libérer le marché
        - main invisible d'Adam Smith
    - l'informatique personnelle booste la productivité et la production 
    - les entreprises et l'administration avec les PC compatibles IBM
        - PC compatible copiables
            - pour lancer le produit et le marché
        - avantage contractuel de Microsoft qui devient l'OS par défaut de tous les fabricants de PC
    - Jobs en fait un match idélogique entre IBM VS Macintosch
        - sa mission c'est libéré l'hommde ses contingences
            - et IBM va vous mettre les chaînes
                - c'est big brother de Orwaell dans 1984 
        - story telling du petit disrupteur
        - mais il se trompe d'ennemi ...
            - il a entendu des innovations du parc
                - il se fait faire une démo
                    - il est fasciné part l'UI fenêtré et la souris
                        - de son propre aveu il ne voit que ça et passe à coté du réseau et du système de messagerie
                    - il revient avec une équipe de dev Apple et se fait refaire une démo
                        - l'équipe du PARC bloque
                        - la direction de Xerox force l'équipe Parc à faire la démo
                - il créé le macintosh avec toute la partie UI/UX du Parc
                    - mais il a un problème: il n'a pas de logiciel pour son nouveau système
                    - il demande à Microsoft de porter ses logiciels sur Macintosh
                        - son ennemi est IBM big blue à l'époque et donc le PC d'IBM
                    - Microsoft écrit les logiciels
                        - ... et sort Windows dans la foulée
                    - Apple VS Microsoft 
                        - départ de Jobs en 1985
                    - Retour à Apple (après NeXt et Pixar) fin 1996 (windows 95 est un carton lmondial)
                        - en 1997 Bill Gates auve Apple en iinjectant 150 000 000 $
                            - il évite ainsi une situation de monopole
                                - lois antirtrust
- A noter l'invisibilisation des innovateurs au profit des commerciaux
    - symboliser par Woz qui va se faire escroquer par Jobs
    - l'informatique personnelle est commercialisée 
        - incarnée 12 ans après avoir été inventé par engelbart
- apparition dégalemet es lois informatiques et libertés 
    - équivalents dans d'autres pays
    - liées en France à l'utilisation de l'informatique pour le fichage d'état
- en 1985 avec les OS à fenêtres l'ordinateur est prêt pour entrer dans les foyers!

----

## (Res)sources

* [🎥 Comment ça marche ? La fabuleuse aventure du micro ordinateur, 1994 , Documentaire (Maurice Chevalet)](https://www.youtube.com/watch?v=W20d8Pj2fy8)
* [🎬 Les pirates de la Silicon Valley (1/2)](https://www.dailymotion.com/video/x36qy6l)
* [🎬 Les pirates de la Silicon Valley (2/2)](https://www.dailymotion.com/video/x36qv1k)
* [🎬 Les cinglés de l'informatique](https://www.youtube.com/watch?v=PH-Qjy4zVKQ&t=3s)
* [🎥 L'histoire de Microsoft](https://www.youtube.com/watch?v=r8AsQ9TVKuA)
* [🎥 1984 Super Bowl APPLE MACINTOSH Ad by Ridley Scott](https://www.youtube.com/watch?v=ErwS24cBZPc)

---

<!-- .slide: data-background="images/chun/MurBerlin.png" style="color: white" class="trsp"-->
#### ~ 1990 - Le web, la liberté et les cryptowars

![Richard Stallman](images/chun/RichardStallman.jpeg)<!-- .element style="width: 17%" --> ![Tim Berners-Lee](images/chun/TimeBernersLee.jpg)<!-- .element style="width: 21%" --> ![Pil Zimmermann](images/chun/PhilZimmermann.jpg)<!-- .element style="width: 20%" --> ![JeffBezos](images/chun/JeffBezos.png)<!-- .element style="width: 24%" --> 

* 1985 - [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman) / [GNU GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License)
* 1989 - [Tim Berners-Lee](https://en.wikipedia.org/wiki/Tim_Berners-Lee) / [Web](https://en.wikipedia.org/wiki/World_Wide_Web)
* 1989 - Chute du mur de Berlin
* 1991 - [Phil Zimmermann](https://en.wikipedia.org/wiki/Phil_Zimmermann) / [PGP](https://www.philzimmermann.com/EN/essays/WhyIWrotePGP.html)
* 1994 - [Netscape](https://en.wikipedia.org/wiki/Netscape_Navigator) / [Jeff Bezos](https://en.wikipedia.org/wiki/Jeff_Bezos) / [Amazon](https://en.wikipedia.org/wiki/Amazon_(company))
* 1995 - [John Perry Barlow](https://fr.wikipedia.org/wiki/John_Perry_Barlow) / [Déclaration d'indépendance du cyberespace](https://fr.wikipedia.org/wiki/D%C3%A9claration_d%27ind%C3%A9pendance_du_cyberespace)

Note:
- Richard Stallman créé le logiciel libre
    - 4 libertés + copyleft (le hack juridique qui force le code commun à le rester)
    - fait écho aux loi informatiques et libertés
    - si l'utilisateur ne maitrise pas un système
        - c'est le système qui maitrise l'utilisateur
        - au même moment Eric S Raymond théorise l'open source avec la cathédrale et le bazar
            - Mais ce n'est pas la même chose
- le web rend l'Internet grand public
    - Armée > Université > Mr ou  Mme Tout le monde

- La chute du mur de Berlin arrive à point
    - le monde libre est enfin unifié
        - on parle de la fin de l'histoire
    - Mais aussi la place tien an men
    - la chute de ceausescu
- La cryptographie dépend à l'époque de la défense dans tous les pays
    - Phil Zimmermann dans une démarche proche de celle de Stallamn écrit PGP
        - le met à dosposition de tous
        - est poursuivi par la Défense américaine
        - persiste en publiant le code dans un livre 
            - 1er amendement sur la liberté d'expression
- finalement la cryptographie est mise à disposition du grand public grâce au e-commerce
- Netscape 
    - javascript
    - cookie de session
    - ouvre la perspective du e-commerce
    - Marc Andersen devenu grosse venture
- Amazon site de ventes de livres en ligne
    - Jeff Bezos voit le potentiel de la vente en ligne
- Les néocommunalistes colonisent l'Internet
    - John Perry Barlow parolier du Greatfull Dead (Summer of Love ROck'n'roll)
        - écrrit la déclaration d'indépendance du cyberespace
            - Nous sommes des avatars dans un monde virtuel
            - vos lois ne s'appliquent pas ici
            - Laisser nous en paix
- j'ai 12 ans, j'ai un amiga 500 je ne fais que jouer dessus, je passe mes journées à pirater des disques de la médiathèque sur K7 et je ne sais même pas que ce que je fais est illégale

----

## (Res)sources

* [🔗 En quoi l'open source perd de vue l'éthique du logiciel libre](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html)
* [🔗 Libre et opensource, la différence](https://yoandemacedo.com/blog/libre-et-opensource-la-difference/)
* [📖 Richard Stallman et la révolution du logiciel libre - Une biographie autorisée (*Sam Williams, Richard Stallman &Christophe Masutti*)](https://framabook.org/docs/stallman/framabook6_stallman_v1_gnu-fdl.pdf)
* [🎬 The Internet's Own Boy - l'histoire d'Aaron Schwartz](https://www.youtube.com/watch?v=7ZBe1VFy0gc)
* [🔗 Code is law / le code fait loi (*Lawrence Lessig*)](https://framablog.org/2010/05/22/code-is-law-lessig/)
* [📖 The Cathedral and the Bazaar](http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/)

----

## (Res)sources

* [📖 Déclaration d’indépendance du Cyberespace (*John P. Barlow*)](http://editions-hache.com/essais/barlow/barlow2.html)
* [📖 Une nouvelle déclaration d’indépendance du cyberespace (*Libération*)](http://www.liberation.fr/amphtml/debats/2018/02/09/une-nouvelle-declaration-d-independance-du-cyberespace_1628377)

---

<!-- .slide: data-background="images/chun/11septembre.png" style="color: white" class="trsp"-->
#### ~2000 - la bulle Internet

* 1997 - [L'invention du pop up](https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/ethan-zuckerman-l-homme-qui-regrette-d-avoir-invente-la-pub-pop-up-7560331)
* 1997 - [Deep Blue VS Kasparov](https://en.wikipedia.org/wiki/Deep_Blue_versus_Garry_Kasparov)
* 1998 - [Larry Page](https://en.wikipedia.org/wiki/Larry_Page) & [Sergey Brin](https://en.wikipedia.org/wiki/Sergey_Brin) / **[Google](https://en.wikipedia.org/wiki/Google)**
* 1998 - [Open-source-software movement](https://en.wikipedia.org/wiki/Open-source-software_movement)
* 2000 - [Bulle Internet](https://fr.wikipedia.org/wiki/Bulle_Internet)
* 2001 - [Eric Schmidt](https://en.wikipedia.org/wiki/Eric_Schmidt) PDG de **[Google](https://en.wikipedia.org/wiki/Google)**
* 2001 - 11 septembre 
* 2001 - [Wikipedia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia)

Note:
- 1997 - Ma première connexion à l'Internet chez moi en France modem 28K
- L'IA gagne ses premières batailles dans l'opinion public
    - elle existe sous une forme supérieure à l'intelligence humaine
    - avec plus ou moins de bonne foi
- 1998 Google organise le web
    - rapide 
    - sans pub
    - organise le savoir présent sur le web
    - conférences dans les universités
    - émergence de l'idée d'une économie de la connaissance
        - incarné quelques années plus tard par wikipedia
- l'open source se détache du logiciel libre
    - moins militant
    - plus pragmatique parce que plus orienté orienté business
    - participe à l'idée d'économie de la connaissane ou de capitalisme cognitif
        - mais ne génère pas de commun au final
- en 2001 La bulle Internet réclame des business model
    - les business angels se crispent
    - qui paient?
    - comment allez vous gagnez de l'argent?
    - Les revenus publicitaires ne sont pas ce qu'ils sont aujourd'hui et le modèle reste à trouver
        - C'est Eric Schmidt qui met sur pied le modèle de Google en 2001
            - il comprend que les données de recherche cumulées des utilisateurs permettent d'avoir du contexte, pour vendre des encarts publicitaires plus pertinent
        - met en place l'accaparement, l'extraction et l'exploitation du surplus comportemental
            - rendez vous au prochain cours pour le détail
- le 11 septembre représente un virage sécuritaire
    - l'extraction de données s'empare du renseignement
- J'ai une connexion ADSL
- Je fais du PHP 3, j'héberge mes sites et ceux des copains
- Je continue de pirater et je sais très bien ce que je fais
- Je promeus et j'ai l'impression de servir l'économie de la connaissance
    - "Partager une somme d’argent c’est la diviser, partager une information c’est la multiplier".

----

## (Res)sources

* [🎬 Rematch](https://www.arte.tv/fr/videos/RC-025577/rematch/)

---

<!-- .slide: data-background="images/chun/adsl.jpg" style="color: white" class="trsp"-->
#### ~2000 - Web 2.0

* 2002 - **Google AdWords** 
* 2003 - **Google AdSense**
* 2003 - [The Pirate Bay](https://fr.wikipedia.org/wiki/The_Pirate_Bay)
* 2004 - Firefox
* 2004 - **Gmail**
* 2004 - **Google scholar**
* 2004 - facebook
* 2005 - Youtube
* 2005 - **Google Maps**
* 2005 - **Google Analytics**

Note:
- Google met en place son système de monétisation AdWords/AdSense
    - concrétise la vision du modèle de Schmidt en posant les bases d'un marché qui deviendra monopolistique
        - par la systématisation
        - par l'ampleur du traffic
        - par l'extension du champs de collecte
        - c'est cette idée que déclineront facebook / twitter et qui se généralisera        
    - Avec Analytics Google s'assure le contrôle des données de visite
    - Avec Maps Google il s'accapare tout l'espace, y compris public 
        - données de géolocalistion
        - L'un des ingénieurs de Maps est à l'origine de Pokemon Go
    - Youtube attire énormément de traffic et donc de données
        - racheté par Google en 2006
    - A l'époque on en dit pas IA mais découverte de connaissances
- Fire Fox incarne l'alternative libre pour tous
- The pirate bay incarne l'économie de la connaissane décliné à la culture
- J'ai des comptes sur tous les réseaux du moment 
    - je suis persuadé, comme benjamin Bayart que "l'imprimerie a appris aux gens à écrire, et que l'Internet leur apprendra à écrire"

----

## (Res)sources

* [🎥 Internet VS minitel 2.0 (*Benjamin Bayard*)](https://cfeditions.com/cyberstructure/)
* [🎥 TPB AFK: The Pirate Bay Away From Keyboard](https://www.youtube.com/watch?v=eTOKXCEwo_8)

---

<!-- .slide: data-background="images/chun/htc-tytn.jpg" style="color: white" class="trsp"-->
#### ~2005 - BigData
* 2006 - Twitter
* 2006 - **Google translate**
* 2006 - **Google documents**
* 2006 - **Google agenda**
* 2007 - Amazon Mechanical Turk
* 2007 - Wikileaks
* 2007 - iPhone
* 2007 - [Crise de subprimes](https://fr.wikipedia.org/wiki/Crise_des_subprimes)
* 2008 - **Google Android**
* 2008 - **Google Chrome**
* 2009 - Bitcoin

Note:
- Google contniue son expansion
    - continue de massifier les données
        - on en fait pas de l'IA mais du big data
    - extension du domaine de la collecte
- Amazon créé Amazon Mechanical Turk
    - prémisse de l'ubérisation
    - plateforme de microtravail / microtache
        - précarité
    - complément aux modèles statistiques imparfait
        - OCR
        - correction /vérification de réponses automatiques
    - marketing gris
        - ferme à clic
    - participe à la magie de l'IA
    - pas de relation d'employeur / employé
        - client plateforme
- avec l'iPhone arrive plus qu'un nouvel appareil
    - le design : l'objet est beau!
    - extension/augmentation de l'humain 
        - on l'a toujours avec nous et on fait tout avec
    - le no stock (flux tendu)
- L'alternative c'est 
    - Assange avec Wikileaks
        - Le 5ème pouvoir
            - journalisme sensé ne pas être d'opinion
                - produit par la communauté
            - câbles diplomatiques
            - Vidéos d'assassinat de civil (Bradley/Chelsea Manning)
            - Clairement à charge contre les (raisons) d'état
        - ambassade d'Équateur à Londres 2012-2019
    - Le Bitcoin
        - qui propose un système décentralisé
        - dynamite les banques
            - et le pouvoir qu'à l'état sur la création monétaires
        - deviendra un actif spéculatif en qq années
            - mafia
            - premiers entrants
            - pyramide de ponzi

----

## (Res)sources

* [🎬 Le 5ème pouvoir](https://fr.wikipedia.org/wiki/Le_Cinqui%C3%A8me_Pouvoir)
* [🎬 Undergound: L'histoire de Julian Assange](https://fr.wikipedia.org/wiki/Underground_:_L%27Histoire_de_Julian_Assange)

---

<!-- .slide: data-background="images/chun/DataCenter.jpg" style="color: white" class="trsp"-->
#### ~2010 - cloud / IoT
* 2012 - **Google Drive**
* 2013 - **Google Glass**
* 2013 - Edward Snowden
* 2014 - **Google Car**
* 2014 - **Android TV**
* 2014 - **Android Auto**
* 2015 - **Alphabet**
* 2015 - Tor
* 2016 - **Google Nest (home)**
* 2017 - Alpha Go

Note:
- La dépossession devient totale
     - extraction de données personnelles
     - extension du domaine des collectes
     - il ne s'agit plus de collectes mais de stockage massif de tout
         - datafication du monde
- E.Snowden disait que "le pire était qu'il ne se passe rien" à la suite de ses déclarations
    - Elles ont surtout été un prétexte pour Google
        - pour pousser le https
            - difficulté pour les états 
            - sans altération de ses propres canaux d'extraction (AdWords, ASdSens, Analytics)
- Un nouveau pas est franchi dans les prouesses
    - le jeu de Go est plus complexe que les échecs
    - la notion d'IA infuse dans l'insonscient collectif
- L'alternative incarné par ToR
    - chiffrement et anonymat complet

----

## (Res)sources

* [📖 Mémoires vives *E. Snowden*](https://fr.wikipedia.org/wiki/M%C3%A9moires_vives_(livre))
* [🎬 Citizen four - (Laura Poitras)](https://www.youtube.com/watch?v=ABBPRd7ARYM)
* [🎬 Snowden](https://fr.wikipedia.org/wiki/Snowden_(film))
* [📖 Menaces sur nos libertés (Julian Assange / Jacob Appelbaum / Müller-Maguhn / Jérémie Zimmermann)](https://korben.info/menace-sur-nos-libertes-assange.html)

---

<!-- .slide: data-background="images/chun/robots.png" style="color: white" class="trsp bottom"-->
## ~2020 - IA / VR

* 2022 - ChatGPT
* 2024 - Apple Vision Pro
* etc ...

---

<!-- .slide: data-background="images/chun/matrix.jpg" -->

---

<!-- .slide: data-background="images/chun/industrie.jpg" style="color: white" class="trsp bottom"-->
## L'informatique est une industrie

* matérielle
* logicielle
* gourmande
    * en matières premières
    * en main d'oeuvre bon marché
    * en [patents](https://en.wikipedia.org/wiki/Patent)
* devenue indispensable aux indutries qui la précèdent

Note:
- extraction des métaux rares
- farbication des devices
- l'économie de la connaissance est une façade et au fond on emprisonne les idées

----

## (Res)sources

* [📖 L'enfer numérique, Voyage au bout d'un like, Guillaume Pitron](https://www.editionslesliensquiliberent.fr/livre-L_enfer_num%C3%A9rique-9791020909961-1-1-0-1.html)

---

<!-- .slide: data-background="images/chun/cybernetic.jpg" style="color: white" class="trsp bottom"-->
## Un système cybernétique

* n'est jamais neutre
    * ni son matériel
    * ni sa logique
    * ni l'information qui y transite

* sert toujours un but
    * celui du concepteur 
        * mais surtout celui du propriétaire

Note:
- de la solution finale de l'Allemagne nazie
- aux business model des BigTech sur le surplus comportemental
- la finalité essentielle a rarement été l'émancipation de l'homme
- la finalité est tout le temps la maximisation des gains

---

<!-- .slide: data-background="images/chun/BlackWoman.png" data-background-size="50%" data-background-position="right" style="color: white" class="trsp bottom"-->

## L'informatique invisibilise

* son coût énergétique 
* son coût environnemental
* son coût humain
* son coût social
* son histoire

Note:
- le design
    - culte de l'objet
- la novlangue
    - l'informatique dans les nuages
    - l'IA
        - semble immatérielle
        - pourtant sont couteux
            - en énergie
            - en matériaux
- les travaux les plus précaires
- on devient tous matrixés
- z'avez vu beaucoup de femmes, beaucoup de noirs dans cette huistoire?
- l'appréhension de tous les enjeux réels est parasitée
- il y a une idéologie derrière cette histoire

----

## (Res)sources

* [🔗 No web without women](https://nowebwithoutwomen.com/)
* [🎬 Les figures de l'ombre](https://fr.wikipedia.org/wiki/Les_Figures_de_l%27ombre)
* [🔗 Novlangue (*wikipedia*)](https://fr.wikipedia.org/wiki/Novlangue)
* [🔗 Quand la "vidéoprotection" remplace la "vidéosurveillance" (*Le monde*)](https://www.lemonde.fr/societe/article/2010/02/16/quand-la-videoprotection-remplace-la-videosurveillance_1306876_3224.html)
* [🎥 sur la manipulation des mots (*Franck Lepage*)](https://www.youtube.com/watch?v=mD-G5lHXviY)
* [🎥 Le mot qui a remplacé "Hiérarchie" (*Franck Lepage*)](https://www.youtube.com/watch?v=IUB1XsT5IQU)
* [🎥 La langue de bois décryptée avec humour ! (*Franck Lepage*)](https://www.youtube.com/watch?v=oNJo-E4MEk8)

---

<!-- .slide: data-background="images/chun/unclesam.png" style="color: white" class="trsp bottom"-->

### informatique & idéologie

* méritocratie
    * story telling entrepreunarial
* technosolutionnisle
    * peut régler tous les problèmes
* évolutionisme
    * cybernétisation du monde
* élitisme
    * voire eugénisme

Note:
* la méritoractie est une petite musique qui tourne en boucle depuis longtemps.
    * le discours de l'échec comme chemin vers le succés sert bien cet argument car
        * si on ne réussit pas: c'est parce que l'on a pas suffisamment essayé
            * c'est donc la reponsabilités de celui qui est pauvre
            * et il est normal que ce qui essaient et finissent du coup forcément par réussir soit riche
                * raisonnement confortable, commun et largement diffusé chez les millardaires de toutes les époques (Rockfeller, Gates, Musk, etc ..) 
    - il faut déconstruire la mythologie de l'entrepreneur
        - ils part rarement de rien
            - beacoup de self made man pourtant
            - tous sont naits au bon endroit (education, localistaion, conetxte aisé)
        - la légende du garage est un élément de récit parmi d'autre

- technosolutionnisme
    - la science se pose la question de si l'IA va nous aider ou pas dace au dérèglement climatique
        - spoiler: non! elle empire les choses chaque jour
    - en revanche la technologie va continuer de pousser son propre calendrier
        - car il est juteux
        - tant pis s'il faut être cynique ou faire l'autruche

- évolutioniste 
    - parce que les technologies nous transforme
        * chacun et de fait en tant qu'espèce
            * on n'est pas complètement certain que ce soit en mieux

- élitiste
    - si on a réussi c'est qu'on le mérite
        - il est légitime de ne pas s'embarasser de la médiocrité d'autrui

- Elon Musk, Peter Thiel & la paypal mafia, Marc Andersen sont les déclarés de cette mouvance
    - Larry Page, Serbeï Brin, Mark Zuckerberg, Bill Gates, Raymond Kurzweil
    - Dr Laurent Alexandre chez les français
- ils s'appellent les libertariens

----

## (Res)sources

* [📖 Le mythe de l'entrepreneur: Défaire l'imaginaire de la Silicon Valley (*Anthony Galluzzo*)](https://www.editionsladecouverte.fr/le_mythe_de_l_entrepreneur-9782355221972)
* [🎧 Elizabeth Holmes, l’arnaqueuse de la Silicon Valley](https://www.radiofrance.fr/franceinter/podcasts/affaires-sensibles/affaires-sensibles-du-lundi-17-octobre-2022-6852465)
* [🎥 LES ENTREPRENEURS ET LE MÉRITE : ORIGINE D'UN MYTHE](https://www.youtube.com/watch?v=SvlnjWvHJYg)
* [📖 Steve Jobs (*Walter Isaacson*)](https://fr.wikipedia.org/wiki/Steve_Jobs_(livre))
* [🎬 Steve Jobs (Danny Boyle)](https://fr.wikipedia.org/wiki/Steve_Jobs_(film))
* [🎬 Don't look up](https://en.wikipedia.org/wiki/Don%27t_Look_Up)
* [🎬 Silicon Valley (série télévisée)](https://fr.wikipedia.org/wiki/Silicon_Valley_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e))

---

<!-- .slide: data-background="images/chun/AtlasShrugged.jpg" data-background-position="bottom" style="color: white" class="trsp"-->
#### Le libertarianisme

* [Atlas Shruggled](https://en.wikipedia.org/wiki/Atlas_Shrugged) Ayn Rand
    * influence 
        * Steve Jobs
        * Peter Thiel
        * Donald Trump
        * Javier Milei
        * ... Edward Snowden

*le livre qui "a le plus influencé les Américains, après la Bible" selon une sérieuse étude de la Bibliothèque du Congrès*

Note:

- cette idéologie est de plus en plus populaire au dela des grosses fortune
- on parle aussi d'objectivisme ou d'égoïsme rationnel
- se définit comme libertaire
    - clairement anti état
        - encore plus anti état providence
        - l'idée est d'échapper systématiquement aux taxes
            - privilégiant les oeuvres philantropiques personnelles
                - et défiscaliser
- économiquement libéraux
- ultra individualiste 
- à distinguer des libertaires de traditions anarchistes
    - ne partagent que 
        - les défenses des libertés
        - et la défiance envers l'état
- mais les libertaires anarchistes sont collectivistes


----

## (Res)sources (1/2)

* [🔗 How influential was Atlas Shrugged upon Steve Jobs](https://www.quora.com/How-influential-was-Atlas-Shrugged-upon-Steve-Jobs)
* [🔗 Ayn Rand et la folie libertarienne](https://philitt.fr/2018/09/10/ayn-rand-et-la-folie-libertarienne/)
* [🔗 Edward Snowden, héros moderne de Ayn Rand](https://www.contrepoints.org/2017/08/23/266537-edward-snowden-ayn-rand-etait-muse)
- [🎥 Ayn Rand, la gourou du capitalisme](https://www.youtube.com/watch?v=xFJ0go1EOBg)
- [🎥 Comprendre la destruction créatrice](https://www.youtube.com/watch?v%253DRgtsEdozYfI)
- [🎥 Introduction à Schumpeter](https://www.cairn.info/revue-l-economie-politique-2006-1-page-82.htm)
- [🎥 Darwin contre le « Darwinisme social »](https://www.youtube.com/watch?v%253D0myevHgYIxw)
- [🎥 Darwin et Kropotkine, compétion ou solidarité ?](https://www.youtube.com/watch?v%253DSs4kNrxCllM)

----

## (Res)sources (2/2)

* La vie d'Ayn Rand
    - [🎧 Épisode 1/5 : De Saint-Petersbourg à New York](https://www.radiofrance.fr/franceculture/podcasts/avoir-raison-avec/de-saint-petersbourg-a-new-york-5412548)
    - [🎧 Épisode 2/5 : La Gloire, enfin](https://www.radiofrance.fr/franceculture/podcasts/avoir-raison-avec/la-gloire-enfin-8549982)
    - [🎧 Épisode 3/5 : Ayn Rand et les libertariens](https://www.radiofrance.fr/franceculture/podcasts/avoir-raison-avec/ayn-rand-et-les-libertariens-6903040)
    - [🎧 Épisode 4/5 : L'héritage littéraire](https://www.radiofrance.fr/franceculture/podcasts/avoir-raison-avec/l-heritage-litteraire-7286845)
    - [🎧 Épisode 5/5 : Avoir 20 ans et lire Ayn Rand](https://www.radiofrance.fr/franceculture/podcasts/avoir-raison-avec/avoir-20-ans-et-lire-ayn-rand-2490960)
* Ayn Rand, pasionaria du capitalisme !
    * [🎧 épisode 1/3](https://www.radiofrance.fr/franceinter/podcasts/la-bas-si-j-y-suis/ayn-rand-pasionaria-du-capitalisme-7314035)
    * [🎧 épisode 2/3](https://www.radiofrance.fr/franceinter/podcasts/la-bas-si-j-y-suis/ayn-rand-pasionaria-du-capitalisme-2-4349884)
    * [🎧 épisode 3/3](https://www.radiofrance.fr/franceinter/podcasts/la-bas-si-j-y-suis/ayn-rand-pasionaria-du-capitalisme-3-1438061)

---

<!-- .slide: data-background="images/chun/gnu.png" data-background-size="50%" data-background-position="top" style="color: white" class="trsp"-->
### Les alternatives?

* souvent libertariennes
* durent peu
    * illégales donc réprimées
    * absorbées / transformées par le business        
* nécessitent des compétences techniques
* l'économie de la connaissance a fait peu d'eMule
* elles restent à réinventer

Note:

- de cette ambiguité "libertaire" peut résulter d'une mauvaise compréhension des alternatives
- la grève est aussi le livre préféré d'Edward Snowden et de Julian Assange
    - on regarde wikileaks et l'affaire Snowden autrement non?
- Jimmy Wales le fondateur de wikipedia aussi
- John Perry fondateur de l'EEF et auteur de la déclaration d'indépendance du cyberespace aussi 
- les cypherpunks et autre crypto boy sont le relais du libertarianisme
    - bitcoin est libertarien
    - très actif idéologiquement sur twitter 

- la piraterie des années 2000 dure encore un peu
    - mais peine à survivre
    - largement réprimée par les lois, les copyrights
        - bizarrement les lois peinent à s'appliquer au Bigtechs
            - RGPD
            - anti trust
            - leak cambridge analytica  
    - ou sont absorbées par le businness
        - spotify qui voulait la musique gratuite
            - mais finit sur un modèle d'abonnement par plus mal rémunérés les artistes que les maisons de disques qu'ils voulaient combattre

- le logiciel libre est probablement l'alternative la plus aboutie
    - c'est un hack juridique
    - mais son utilisation exige un savoir faire que seuls les techniciens possèdent
        - du coup même les techniciens ne savent plus ce que c'est
            - et le confonde avec l'opensource
    - linux est probablement ce qui reste
        - mais seules Debian et Arch sont libres ...
        - RedHat Oracle, même ubuntu fondation etc ...
    - dévoyé par l'open source
- sans parler des exigences de ToR (intéressant aussi)
    - MAIS probablement plus de la moitié du réseau est détenu par la CIA
        - rend l'anonyma bien relatif

- peu d'eMule
    - parce qu'on nomme mal les choses
        - "Mal nommer les choses, c'est ajouter au malheur du monde!" Albert Camus
    - parce que les Hypergagnants le deviennent en exploitant les communs au fur et à mesure qu'ils se construisent 
        - penser au rachat de produit open source par Oracle (MariaD ou IBM (ansible)
        - penser aussi à google maps, goole books ...
        - on verra ça au cours prochain dans le détail ...


----

## (Res)source

* [🎬 The playlist](https://fr.wikipedia.org/wiki/The_Playlist)
* [🎥 L'éthique des hackers (*Steven Levy*)](https://www.editions-globe.com/lethique-des-hackers/)

---

<!-- .slide: data-background="images/chun/banksy_cpyright.png" style="color: white" class="trsp bottom" -->

### CODE IS LAW<!-- .element: class="fragment" -->

Note:
* l'utopie est déchue
* le cynisme des grands vainqueurs est affligeant
- la loi défend (avant tout?) la propriété
    * les communs sont grignotés car il n'y a pas de vraies volonité de les défendre
* finalement on pourrait opposer 2 visions du monde
    * collectiviste celle qui s'éloigne
    * individualiste celle qui se dessine
        * avec un cynisme absolue

---

<!-- .slide: data-background="images/jello.jpg" style="color: white" class="trsp bottom" -->
#### "Don't hate the media, become the media"<!-- .element class="fragment" -->
## "Become the message?""<!-- .element class="fragment" -->

Note:
* Malgré tout c'est nous qui construisons ces technologiques
    * et le code c'est la loi : CODE IS LAW
        * donc nous la subissons tous
        * mais nous seuls 
            * vous et moi informaticiens
                * celle loi nous la faisons
                    * car nous gouvernons l'impélmentation du système  
* j'espère que vous avez appris des choses
* j'espère vous avoir sensibilisé aux concepts de communs
    * j'espère vous avoir donner envie de les penser et à les développer
* j'espère surtout que ça vous servira un jour
    * pourquoi pas à vous raconter propre histoire?

---

<!-- .slide: data-background="images/chun/applause.gif" style="color: white" class="trsp" -->
# Merci