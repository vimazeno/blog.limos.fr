## RGPD

Règlement général sur la protection des données [UE]

* [Comprendre le règlement européen](https://www.cnil.fr/fr/comprendre-le-reglement-europeen)
* [Règlement européen sur la protection des données : ce qui change pour les professionnels](https://www.cnil.fr/fr/reglement-europeen-sur-la-protection-des-donnees-ce-qui-change-pour-les-professionnels)
* [**07 octobre 2015** - Invalidation du « safe harbor » par la CJUE : une décision clé pour la protection des données](https://www.cnil.fr/fr/invalidation-du-safe-harbor-par-la-cour-de-justice-de-lunion-europeenne-une-decision-cle-pour-la-0)
* En France la CNIL devient autorité de contrôle


## Périmètre

* Prise de position
* Réglement européen
  * envigueur le 25 mai 2018
  * Dans tous les états membres de l'UE
* <strike>Déclaration</strike>
  * responsabilité
    * démonstration permanente du maintien de la conformité


## Périmètre

* Tout service ou sous traitant (y compris hors UE) traitant des données de résidents de l'UE
  * Entreprises
  * Associations
  * Organismes publics
  * Sous traitants


## Objectifs

* Renforcer la transparence
  * Quelles données sont collectées?
  * Dans quels buts?
  * Pour combien de temps?


## Objectifs

* Faciliter l'exercice des droits
  * droit à la rectification
  * droit à la portabilité
    * récupération
    * communication à un autre traitement


## Objectifs

* Faciliter l'exercice des droits
  * droit à l'oubli
    * suppression de données personnelles
      * dès qu'elles ne sont plus nécessaires au traitement
      * dès que le consentement de l'utilisateur a été retiré
      * dès que la personne s'y oppose


## Objectifs

* Crédibiliser la régulation
  * Réclamation auprès de l'autorité de contrôle
  * Droit de recours contre le responsable du traitement ou un sous traitant
  * Actions collectives ou personnelles
  * Sanctions
    * 4% du chiffre d'affaire annuel mondial
    * 20 000 000 €


## Objectifs

* Responsabiliser les acteurs traitant des données
  * Obligation d'information en cas de violation de données à caractère personnel
    * autorité de contrôle
    * La personne concernée


## Principes

* Accountability
  * tous responsables
  * tous auditables
  * <strike>Déclaration CNIL</strike>
* Privacy By Design
  * Protection des données prise en compte du début
* Security By Default
  * Mesures de sécurité nécessaires
  * Détection de compromission


## Principes

* DPO (Data Protection Officer)
  * conformité RGPD
  * Point de contact avec les autorités
* Analyse d'impact (PIA)
  * [un logiciel pour réaliser son analyse d’impact sur la protection des données (PIA)](https://www.cnil.fr/fr/rgpd-un-logiciel-pour-realiser-son-analyse-dimpact-sur-la-protection-des-donnees-pia)


## [RPGD : en 6 étapes (CNIL)](https://www.cnil.fr/fr/principes-cles/rgpd-se-preparer-en-6-etapes)

1. Désigner un pilote
2. Cartographier
3. Prioriser
4. Gérer les risques
5. Organiser
6. Documenter


## 1.Désigner un pilote

### Délégué à la protection des données (DPO / DPD)

* Information
* Conseil
* Contrôle en interne la conformité au RGPD.


## 2.Cartographier

* Catégoriser les données traitées
* Recenser précisement les traitements de données personnelles
  * Documenter chaque traitement de données personnelles
* Lister les objectifs
* Identifier les acteurs
* Identifier les flux des données


## 3.Prioriser

* Ne collecter et traiter que les données nécessaires
* Vérifier
  * l'obtention du consentement de la personne
  * les contrats
  * les sous traitants
  * les obligations légales
* Réviser ses mentions d’information
  * Transparence
  * Transitivité


## 3.Prioriser

* Prévoir les modalités d’exercice des droits des personnes
  * consultation
  * rectification
  * portabilité
  * retrait du consentement
  * opposition

* Vérifier les mesures de sécurité mises en place


## Données sensibles

* origines raciales ou ethniques
* opinions politiques, philosophiques ou religieuses
* appartenance syndicale
* santé
* orientation sexuelle
* génétiques ou biométriques
* infraction ou de condamnation pénale
* sur les mineurs

Soumises à autorisation de la CNIL


## 3.Transfert des données hors UE

* Vérifier que le pays vers lequel vous transférez les données est reconnu comme adéquat par la Commission européenne
  * Dans le cas contraire, encadrez vos transferts


## 4.Gérer les risques

* Privacy Impact Assessment (PIA)
  * Analyse de risques sur les données personnelles
    * détermine les mesures techniques et organisationnelles de protection


## 4.Gérer les risques

1. description du traitement étudié et de ses finalités
2. évaluation de la nécessité et de la proportionnalité des opérations de traitement au regard des finalités
3. évaluation des risques pour les droits et libertés des personnes
4. mesures envisagées pour faire face aux risques


## 4.Qui participe au PIA?

* Le responsable de traitement
  * valide et applique le PIA.
* Le délégué la protection des données (DPO)
  * élabore le plan d'action
  * vérifie son exécution


## 4.Qui participe au PIA?

* Le(s) sous-traitant(s)
  * fournissent les informations nécessaires à l’élaboration du PIA (conformité)
* Les métiers (RSSI, maîtrise d'ouvrage, maîtrise d'oeuvre)
  * aident à la réalisation du PIA


## 5.Organiser

* Protection des données personnelles dès la conception
* Organiser la remontée d'information
* Traiter les réclamations et les demandes
  * exercice des droits des utilisateurs
* Anticiper les violations de données
  * information dans les 72 heures aux autorités et personnes concernées


## 6.Documenter

### Prouver la conformité = Avoir la documentation nécessaire

* Pour les responsables de traitements
  * registre des traitements
* Pour les sous-traitants
  * catégories d'activités de traitements
* Pour les traitements à risque
  * PIA
* Encadrement des transferts de données hors UE


## 6.Documenter

* Mentions d'information / légales
  * [Modèles de mentions légales CNIL](https://www.cnil.fr/fr/modeles/mention)
* Procédures pour l’exercice des droits
* Contrats avec les sous-traitants
* Procédures internes en cas de violations de données
* Preuves du consentement


# cadre légal hors RGPD


## dispositifs spéciaux

* Recherche publique
  * [Protection du Potentiel Scientifique et Technique de la nation](http://fr.wikipedia.org/wiki/Protection_du_potentiel_scientifique_et_technique_de_la_nation_%28PPST%29)  
* Données de santé
  * [ASIP santé - L'agence française de la santé numérique](http://esante.gouv.fr/)
* Etablissements de crédit
  * [Garanties spécifiques de sécurité](http://www.fbf.fr/fr/contexte-reglementaire-international/cadre-juridique/les-principaux-textes-regissant-le-secteur-bancaire-francais)


## Conservation des logs de modification

* [Décret n° 2011-219 du 25 février 2011 relatif à la conservation et à la communication des données permettant d’identifier toute personne ayant contribué à la création d’un contenu mis en ligne](http://blog.crimenumerique.fr/2011/03/04/decret-dapplication-de-la-lcen-sur-la-conservation-des-donnees-par-les-fai-et-hebergeurs/)

  * ip, url, protocole, date heure, nature de l'opération
    * éventuellement les données utilisateurs
    * éventuellement données bancaires


## Conservation des logs de modification

  * accédés dans le cadre d’une réquisition judiciaire
  * conservés un an
    * données utilisateurs pendant un an après la clôture

[Article 60-2](http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006575051): mise à disposition dans les  meilleurs délais

[Article 226-20](http://legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006417977): les logs ont une date de péremption

Note:
- Décret d'application mettant en oeuvre la LCEN


## Risques encourus par le pirate

* Système de Traitement Automatisé de Données
  * maintien frauduleux de l'accès
    * 2 ans d'emprisonnement & 30 000 € d'amende
  * suppression ou modification des données      
    * 3 ans d'emprisonnement & 45 000 € d'amende
  * si données à caractère personnel  
    * 5 ans d'emprisonnement & 75 000 € d'amende

[Article 323-1](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418316&cidTexte=LEGITEXT000006070719)


## risques encourus par le pirate

* altération du fonctionnement
  * 5 ans d'emprisonnement et de 75 000 € d'amende
* si données à caractère personnel  
  * 7 ans d'emprisonnement & 100 000 € d'amende

[Article 323-2](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000025585005&cidTexte=LEGITEXT000006070719)

Note:
- interdiction d'exercer dans la fonction publique entre autre
- privé de droits civique
- d'exercer la profession dans laquelle le délis a été commis
- confiscation du matos
- [Article 323-5](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418326&cidTexte=LEGITEXT000006070719)


## risques encourus par le pirate en pratique

pas de condamnation pour le pirate si

* aucune protection
* aucune mention de confidentialité
* accessible via les outils de navigation grand public
* même en cas de données nominatives

[KITETOA VS TATI](http://kitetoa.com/Pages/Textes/Les_Dossiers/Tati_versus_Kitetoa/index.shtml)

[bluetouff VS ANSES](http://bluetouff.com/2013/04/25/la-non-affaire-bluetouff-vs-anses/)

Note:
- Kitetoa
  - Suite au signalement d'une faille non corrigée et à l'écriture d'un article prétendant l'avoir exploité
  - Cour d’appel de Paris, le 30 octobre 2002
- Bluetouff
  - Publication de fichiers condidentiels L’ANSES (Agence nationale de sécurité sanitaire - OIV)
  - 2013


## risques encourus par le pirate en pratique

* Atteintes aux [intérêts fondamentaux de la nation](http://fr.wikipedia.org/wiki/Int%C3%A9r%C3%AAts_fondamentaux_de_la_nation)
  * [Sécurité nationale](http://fr.wikipedia.org/wiki/S%C3%A9curit%C3%A9_nationale)
    * [Article 410-1 à 411-6](http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006136044&cidTexte=LEGITEXT000006070719)
* Secret des communication pour l'autorité publique et FAI
  * 3 ans d'emprisonnement et de 45 000 € d'amende
    * [Article 432-9](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418513&cidTexte=LEGITEXT000006070719)


## risques encourus par le pirate en pratique

* Usurpation d'identité
  * 5 ans d'emprisonnement et de 75 000 € d'amende
    * [Article 434-23](http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070719&idArticle=LEGIARTI000006418661)
* Importer, détenir, offrir ou mettre à disposition un moyen de commettre une infraction est puni
  * [Article 323-3-1](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418323&cidTexte=LEGITEXT000006070719) (issu [de la Loi Godfrain](http://www.hackersrepublic.org/cultureduhacking/la-loi-godfrain-explications-et-illustrations))

Note:
- Sécurité nationale: ordre public et sécurité civile, relations extérieures et diplomatie, finance, matières premières, énergie, alimentation et produits industriels, santé publique, transports et télécommunications, travaux publics et sécurité des systèmes d'information.
- Secret des communication -> pas de cassage de chiffrement
- Usurpation d'identité -> ingénierie sociale
- Importer, détenir, offrir ou mettre à disposition un moyen de commettre une infraction est puni -> publication de vulnérabilités
- "vous les experts en sécurité informatique qui savez de quoi vous parlez, faites bien gaffe à ce que les autres utilisateurs disposent des moyens de se protéger des trucs[2] que vous publiez pour vous faire mousser". Pas complètement faux...
