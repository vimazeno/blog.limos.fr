## Je n'ai rien à cacher ...

![WC](images/jnarac/index/wc.jpg)<!-- .element style="width: 100%" -->

Note:
- Alors vous n'auriez rien à cacher?

- combien considère qu'il n'ont rien à cacher?
- vous fermez la porte quand vous allez aux toilettes?
- vous laissez la webcam perpétuellement allumée?
- je peux avoir les clés de votre habitat?
- vos transactions bancaires m'intéressent aussi
- votre historique de recherche
  - Google
  - facebook  
    - les gens à qui vous pensez le plus
- les gens avec qui vous conversez
  - ce que vous vous dites
- ou vous allez
  - avec qui


### ... donc rien à craindre ([novlangue](https://fr.wikipedia.org/wiki/Novlangue))

![Nothing to hide nothing to fear](images/jnarac/index/nothing_to_hide_nothing_to_fear.jpg)<!-- .element style="width: 30%" -->

Note:
- Je vous comprendrais car si vous n'avez rien à chacher vous n'aurais rien à craindre 
  - dystopie 1984 d'Orwell, le contrôle du langage se fait apr la novlangue qui vide les mots de leur sens
    - ref Franck Le Page
- implicitement
  - je n'ai donc rien à craindre
    - puisque je suis du "bon" côté
      - du côté des gens normaux
        - qui correspondent à la norme


## Ah!?

![Nothing to hide nothing to fear](images/jnarac/index/nothing_to_fear.jpg)<!-- .element style="width: 50%" -->

Note:
- la norme change pour le meilleur
  - l'homosexulalité
  - l'IVG
  - il faut pouvoir penser la transgression pour amorcer l'évolution
- la norme peut à tout moment changer pour le pire
  - pour les juifs
  - pour les étrangers surt un territoire donné
- la normalité n'est pas qu'une question de "volonté"
  - quid d'une personne malade
    - qui souffre ... drogue douce
  - quid des handicapés
    - leur comportement
    - le coup de leur prise en charge
- attention aux anglicismes, aux nouveaux mots, aux nouveaux emplois


## De La neutralité de la technologie ...

![Elf Surveillance Santa Camera](images/jnarac/index/Elf_Surveillance_Santa_Camera.png)

Note:
- controle non autoritaire, consentement par ajout de confort et de sécurité
- Dystopie "Le meilleur des mondes" d'Aldous Huxley


## à un projet global

![Edward Snowden](images/jnarac/index/twitt-snowden.png)

Note:
- on vend de la sécuriité en produisant de la surveillance


## [le panoptique](https://fr.wikipedia.org/wiki/Panoptique)

![Prison cubaine panoptique](images/jnarac/index/prison-cubaine-panoptique.jpg)<!-- .element style="width: 85%" -->

Note: 
- Est ce un problème de Conception ou un problème d'usage de l'outil numérique?
  - Le fait est qu'il est actuellement utilisé comme un outil de surveillance de masse.
      - le panoptique est un modèle carcéral
      - pour servir un objectif: le contrôle 
      - via différentes techniques
        - propagande: complot, secte, politique, (ultra)liberalisme, religion,
        - (neuro)marketing: vendre
        - utilsation de viralité / bias cognitif
          - ref le bug humain
  - accepter la surveillance de masse
    - c'est légitimé cette architecture dans le monde libre
      - et donc annihiler la liberté de parole et de penser
        - celui qui est dans la tour à le pouvoir sur les autres


![Degrés de séparation](images/jnarac/index/degres-separation.png)<!-- .element style="width: 50%" -->

Famille, amis, collègues, entreprises, institutions ... tout est imbriqué!
