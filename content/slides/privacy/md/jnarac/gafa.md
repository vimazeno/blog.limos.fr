## GAFAM / BATX

GAFAM = Google + Apple + Facebook + Amazon + Microsoft

<iframe width="560" height="315" src="https://www.youtube.com/embed/BQovQUga0VE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

BATX = Baidu + Alibaba + Tencent + Xiaomi


### Différents modèles économiques

![ads](images/jnarac/gafa/ads.jpg "ads")<!-- .element: width="90%" -->


![Apple](images/jnarac/gafa/apple.jpeg "apple")
[source @EconomyApp](https://twitter.com/EconomyApp/status/1654460936625987584)


![Amazon](images/jnarac/gafa/amazon.jpeg "amazon")
[source @EconomyApp](https://twitter.com/EconomyApp/status/1654460936625987584)


![Microsoft](images/jnarac/gafa/microsoft.jpeg "microsoft")
[source @EconomyApp](https://twitter.com/EconomyApp/status/1654460936625987584)


![Alphabet](images/jnarac/gafa/alphabet.jpeg "alphapet")
[source @EconomyApp](https://twitter.com/EconomyApp/status/1654460936625987584)


## Si c'est gratuit

c'est l'utilisateur le produit!

![Si c'est gratuit, c'est vous le produit!](images/jnarac/gafa/pigs-and-the-free-model.jpg "Si c'est gratuit, c'est vous le produit!")<!-- .element: style="width:40%;" -->

[les 4 vols de facebook](https://tuxicoman.jesuislibre.net/2012/06/le-web-2-0-explique-par-laurent-chemla.html)


### Données personnel

![Cartographie des données personnelles](images/jnarac/gafa/carto_donnees_personnelles.png)<!-- .element: width="80%" -->

Note:
- payé ou non ne change  rien en terme de tracage
- logique extractivisté, chercher de nouveau terrain, google maop
- digitalisation du réelle (googlez scan book)


## Bons apôtres <i class="fa fa-facebook" aria-hidden="true"></i>

![Zucki](images/jnarac/gafa/Mark_Zuckerberg.jpg)<!-- .element style="float: right; margin-left: 50px" -->

<!-- "People have really gotten comfortable not only sharing more information and different kinds, but more openly and with more people. [...] That social norm is just something that has evolved over time." -->

"Les gens sont désormais à l'aise avec l'idée de partager plus d'informations différentes, de manière plus ouverte et avec plus d'internautes. [...] La norme sociale a évolué."

[Mark Zuckerberg, PDG de Facebook, USTREAM, 10 janvier 2010](http://www.lemonde.fr/technologies/article/2010/01/11/pour-le-fondateur-de-facebook-la-protection-de-la-vie-privee-n-est-plus-la-norme_1289944_651865.html)

Note:
- Il a acheté toutes les maisons avec vis a vis sur la sienne


## Bons apôtres <i class="fa fa-facebook" aria-hidden="true"></i>

![Zucki se cache?](images/jnarac/gafa/zuc_cam.png)<!-- .element style="float: right; margin-left: 50px" -->


## Bons apôtres <i class="fa fa-google" aria-hidden="true"></i>

![Schmidti](images/jnarac/gafa/Eric_Schmidt.jpeg)<!-- .element style="float: left; margin-right: 50px" -->

"If you have something that you don't want anyone to know, maybe you shouldn't be doing it in the first place."

"S'il y a quelque chose que vous faites et que personne ne doit savoir, peut-être devriez vous commencer par ne pas le faire."

[Eric Schmidt, PDG de Google, The Register, 7 décembre 2009](http://www.theregister.co.uk/2009/12/07/schmidt_on_privacy/)

Note:
- on parle Google Facebook mais on parle GAFA et les autres
    - Même si Tim Cook communique sur l'importance de la vie privèe
- Keep Calm les GAFA et l' Asic (Association des services Internet communautaires)
    - se préocuppent aussi de défendre de notre vie privée
        - surtout si l'oppresseur c'est l'état


## <i class="fa fa-google" aria-hidden="true"></i> & <i class="fa fa-facebook" aria-hidden="true"></i> features

* [Google Mon activité](https://myactivity.google.com/myactivity)
* [Google position history - 20 novembre 2014](https://maps.google.com/locationhistory/b/0)
* [Google passwords](https://passwords.google.com/)
* Google [analytics](https://www.google.com/intl/fr/analytics/), [AdSense](https://www.google.fr/adsense/start/), [AdWords](https://adwords.google.com)
* [facebook stockerait des données sur les non membres](http://www.numerama.com/magazine/20237-facebook-stockerait-des-donnees-sur-les-non-membres.html)
* [hello facebook identifie les numéros inconnus](http://www.numerama.com/magazine/32889-avec-hello-facebook-identifie-les-numeros-inconnus.html)
* suggestion à partir de sujet non recherché
  * mais abordé vocalement


## <i class="fa fa-ambulance" aria-hidden="true"></i> protéger sa vie privée

* Lire les CGU
  * [Terms of Service; Didn't Read - TOSDR](https://tosdr.org/)
* Utiliser des services souverains
  * Conforme au [RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees)
* Etre [souverain](sovereignty.html) sur les outils que l'on utilise
  * Faire la différence entre logiciel
    * Gratuit
    * Open Source
    * Libre <i class="fa fa-smile-o" aria-hidden="true"></i>


## <i class="fa fa-ambulance" aria-hidden="true"></i> protéger sa vie privée

* Comprendre les [tracking](tracking.html) cookie
  * [<i class="fa fa-github" aria-hidden="true"></i> willdurand-edu/cookie-playground](https://github.com/willdurand-edu/cookie-playground)
* Eviter les tracking cookie
  * [<i class="fa fa-firefox" aria-hidden="true"></i> Ghostery](https://www.ghostery.com/fr/)
  * [<i class="fa fa-firefox" aria-hidden="true"></i> NoScript](https://noscript.net/)
* Utiliser Tor


## <i class="fa fa-google" aria-hidden="true"></i> & <i class="fa fa-facebook" aria-hidden="true"></i> stratégie 

* [Pourquoi Google n’a payé que 17 millions d’euros d’impôts en France en 2018](https://www.lemonde.fr/les-decodeurs/article/2019/08/02/pourquoi-google-paie-si-peu-d-impots-en-france_5496034_4355770.html) #tax
* [Google contourne habilement l’article 11. Les politiciens, hébétés, fulminent.](http://h16free.com/2019/10/02/64501-google-contourne-habilement-larticle-11-les-politiciens-hebetes-fulminent?fbclid=IwAR1LHE8yEaSfpTStwgyjSkFSoWdZlGFgj2MSqpa1HshbiqVofCudKe_Byoo) #copyright
* [Shot de dopamine : ce que Facebook fait au cerveau de mon amie Emilie](https://www.nouvelobs.com/rue89/notre-epoque/20171222.OBS9715/shot-de-dopamine-ce-que-facebook-fait-au-cerveau-de-mon-amie-emilie.html) #neuromarketing


## la société post vérité

[![RSA true](images/jnarac/www/rsa2.png)<!-- .element: width="45%" style="float: right" -->](http://rue89.nouvelobs.com/rue89-eco/2013/03/12/la-fable-bidon-de-la-famille-rsa-qui-gagne-plus-que-la-famille-salariee-240493)

[![RSA false](images/jnarac/www/rsa1.png)<!-- .element: width="45%" -->](http://rue89.nouvelobs.com/rue89-eco/2013/03/12/la-fable-bidon-de-la-famille-rsa-qui-gagne-plus-que-la-famille-salariee-240493)

[<small><i class="fa fa-newspaper-o" aria-hidden="true"></i> Quand on demande à Google si l’Holocauste a bien eu lieu...</small>](http://tempsreel.nouvelobs.com/rue89/rue89-sur-les-reseaux/20161226.RUE6067/quand-on-demande-a-google-si-l-holocauste-a-bien-eu-lieu.html)
[<small><i class="fa fa-newspaper-o" aria-hidden="true"></i> La tyrannie des agissants</small>](https://resistanceauthentique.net/tag/tyrannie-des-agissants/)
[<small><i class="fa fa-newspaper-o" aria-hidden="true"></i> Retour sur le scandale Cambridge Analytica et la (molle) réponse de Facebook</small>](https://www.nextinpact.com/news/106349-retour-sur-scandale-cambridge-analytica-et-molle-reponse-facebook.htm)


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### Fact-checking

* Identifier les __sources__
  * sites de la [fachosphère](https://fr.wikipedia.org/wiki/Extr%C3%AAme_droite_sur_Internet_et_Usenet)
  * sites [complotistes](http://rue89.nouvelobs.com/2016/01/01/charlie-hebdo-sont-sites-parlent-complot-257284)
  * sites à ligne éditoriale orientée
    * [RT en français](https://francais.rt.com/) c.f. [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> RT](https://fr.wikipedia.org/wiki/RT_(cha%C3%AEne_de_t%C3%A9l%C3%A9vision))
  * sites lobbyistes (environnement, santé, sécurité)
    * ikipédia?


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### avoir des repères

* [hoaxbuster](http://www.hoaxbuster.com/)
* [les décodeurs](http://www.lemonde.fr/les-decodeurs/)
  * [Décodex](http://www.lemonde.fr/verification/)
* [ontemanipule.fr](http://www.gouvernement.fr/on-te-manipule)
* [reddit <i class="fa fa-reddit" aria-hidden="true"></i>](https://www.reddit.com/)
* [Comment mieux repérer de fausses photos et vidéos](http://www.liberation.fr/desintox/2016/03/22/comment-mieux-reperer-de-fausses-photos-et-videos_1441248)
* [<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Les fake news n'existent pas (et c'est vrai)](http://www.bortzmeyer.org/fake-news.html)


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### un exemple

* [<i class="fa fa-newspaper-o" aria-hidden="true"></i> RAS-LE-BOL ! MICHEL SARDOU à HOLLANDE : « L’ISLAM est peut-être SUPER…Mais ON N’EN VEUT PAS !…»](http://lemondealenversblog.com/2015/07/26/ras-le-bol-michel-sardou-a-hollande-lislam-est-peut-etre-super-mais-on-nen-veut-pas/)
  * [<i class="fa fa-newspaper-o" aria-hidden="true"></i> Fausse lettre xénophobe : Michel Sardou est "abasourdi, effondré, sur le cul"](http://www.franceinfo.fr/emission/le-vrai-du-faux-numerique/2014-2015/michel-sardou-se-dit-impuissant-face-une-fausse-lettre-xenophobe-06-01-2015-17-52)
  * [<i class="fa fa-newspaper-o" aria-hidden="true"></i> PROUESSE ! BELGIQUE : elle se fait GREFFER UNE NOUVELLE OREILLE… cultivée dans son BRAS](http://lemondealenversblog.com/2015/06/19/prouesse-belgique-elle-se-fait-greffer-une-nouvelle-oreille-cultivee-dans-son-bras/#more-10347)

#### viralité = émotion + instantanéité

Note:
- centraliser la validation de la vérité sur 8 médictatures
    - est ce crédible


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### Tous les détails comptent!

<a href="https://faketrumptweet.com/fake-tweet/li25daqg_94kaca_mtnx66"><img src="https://s.faketrumptweet.com/li25daqg_94kaca_mtnx66.png" title="Made in America at faketrumptweet.com"/></a>

tapoter sept fois sur la coque de son smartphone avant de partager


## [Digital labor](https://fr.wikipedia.org/wiki/Travail_num%C3%A9rique)

* [« Sur Internet, nous travaillons tous, et la pénibilité de ce travail est invisible »](http://www.lemonde.fr/pixels/article/2017/03/11/sur-internet-nous-travaillons-tous-et-la-penibilite-de-ce-travail-est-invisible_5093124_4408996.html)
* [Faut pas prendre les usagers des gafa pour des datas sauvages](http://affordance.typepad.com//mon_weblog/2018/01/faut-pas-prendre-les-usagers-des-gafa-pour-des-datas-sauvages-.html)
* [Les éboueurs du Web, modérateurs invisibles des réseaux sociaux](https://usbeketrica.com/article/les-eboueurs-du-web-moderateurs-invisibles-des-reseaux-sociaux)


## [<i class="fa fa-amazon" aria-hidden="true"></i>  Mechanical Turk](https://www.mturk.com/)

[![Mechanical turk](images/jnarac/gafa/The_turk.jpg)<!-- .element style="width:30%;" -->](https://en.wikipedia.org/wiki/The_Turk)

["A la rencontre des raters petites mains des Big Data"](https://theconversation.com/a-la-rencontre-des-raters-petites-mains-des-big-data-86484)


## Intelligence artificielle

* ["Nous sommes les idiots utiles des GAFA et des BATX"](http://blog.barbayellow.com/2017/01/28/ia-education-et-revenu-universel/)
![ReCaptcha](images/jnarac/gafa/reCaptcha.png)<!-- .element style="width:20%;float:right;display: inline" -->
* [MIT Moral Machine](http://moralmachine.mit.edu/)
* [Deep Face - reconnaissance faciale](https://research.facebook.com/publications/480567225376225/deepface-closing-the-gap-to-human-level-performance-in-face-verification/)
* [Algorithmes partout, intelligence nulle part](https://www.affordance.info/mon_weblog/2018/12/algorithmes-partout-intelligence-nulle-part-.html)

[ThinkerView - Éric Sadin : l'asservissement par l'Intelligence Artificielle ?](https://www.youtube.com/watch?v=VzeOnBRzDik)


## IoT

* [Votre téléphone vous écoute, ce n’est pas de la paranoïa](https://www.vice.com/fr/article/wjbzzy/votre-telephone-vous-ecoute-ce-nest-pas-de-la-paranoia)
* [Kinect pour Xbox One : un espion sans pareil dans votre salon ?](https://www.developpez.com/actu/56258/Kinect-pour-Xbox-One-un-espion-sans-pareil-dans-votre-salon-Non-assure-Microsoft-qui-rappelle-que-la-console-peut-etre-entierement-eteinte/)
* [Fitbit dévoile l'activité sexuelle de ses utilisateurs sur le Net](http://www.01net.com/actualites/une-appli-devoile-l-activite-sexuelle-de-ses-utilisateurs-sur-le-net-535193.html)
* [À 6 ans, elle discute avec Echo d'Amazon et commande une maison de poupée et des cookies](http://www.slate.fr/story/133601/amazon-echo-fille-commande)

<!--
## Transhumanisme

* [NBIC](https://fr.wikipedia.org/wiki/Nanotechnologies,_biotechnologies,_informatique_et_sciences_cognitives)
  * Nanotechnologies
  * Biotechnologies
  * Informatique
  * Sciences cognitives
* [Libertarianisme](https://fr.wikipedia.org/wiki/Libertarianisme)
* [Seasteading Institue](https://www.seasteading.org/)
* [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Raymond Kurzweil](https://fr.wikipedia.org/wiki/Raymond_Kurzweil)
    *  directeur de l'ingénierie chez Google depuis 2012
-->