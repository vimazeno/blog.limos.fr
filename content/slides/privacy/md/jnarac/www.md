## Cybernétique

![Norbert Wiener](images/jnarac/www/Norbert_Wiener.jpg)<!-- .element width="60%" -->

Note:
- La cybernétique entre 2 guerres / les tourelles
  - système = jeu d'acteur interagissant par flux d'unformation
    - science de l'information dans les systèmes
      - paradoxe de la science qui n'a pas fait science
        - maintenant on est d'acord que c'est un style de SF 
          - dés sa création
        - ce serait maintenant la systémique ou les systèmes complexes
      - paradoxe VS la science qui façonne la pensée depuis 1968
        - systèmes


## Internet

![Hippies](images/jnarac/www/hippies-60s.jpg)<!-- .element width="60%" -->

Note:
- à la fin des années 60 la cybernétique à infusé
  - ainsi que le Design [Buckminster_Fuller](https://en.wikipedia.org/wiki/Buckminster_Fuller)
  - que la [théorie de l'information de shannono](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27information)
  - que la [machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing)
  - ...
  - Darpa / Hippies / Californie / 


## l'histoire des Internets

* [chronologie du réseau Internet](http://www.tiki-toki.com/timeline/embed/137139/6372410394/#vars!date=1954-07-27_20:20:56!)
* [Internet History](images/internet_history.jpg)
* [<i class="fa fa-youtube-play" aria-hidden="true"></i> Une contre histoire de l'internet](https://www.youtube.com/watch?v=MUTABXD8f24)
* [une histoire d'Internet](http://www.chemla.org/textes/hinternet.html) par [_Laurent Chemla_](https://fr.wikipedia.org/wiki/Laurent_Chemla)
* [Déclaration d’indépendance du Cyberespace](http://editions-hache.com/essais/barlow/barlow2.html) par [John P. Barlow](https://fr.wikipedia.org/wiki/John_Perry_Barlow)
* [Une nouvelle déclaration d’indépendance du cyberespace](http://www.liberation.fr/amphtml/debats/2018/02/09/une-nouvelle-declaration-d-independance-du-cyberespace_1628377)
*  pensé par ses pères comme un projet politique
  * but [code is law](https://framablog.org/2010/05/22/code-is-law-lessig/)

Note:
- "Internet a été inventé par l'armée américaine pour résister à une attaque atomique"
    - n'est qu'une toute petite partie de l'histoire
        - les technologies personnelles et communautés hippies
            - l'ordinateur personnel


## Internet

* __efficient__: les messages arrivent toujours

* __résiliant__: trouve d'autres chemin si besoin

* __décentralisé__: personne ne le contrôle réellement

* __ouvert__: très facile de s'y connecter

* __nativement non sécurisé__: tout cricule en clair

* __transmission par paquet__: via le protocol TCP / IP
  - [datagramme 🇫🇷 VS circuit virtuel](https://www.france.tv/documentaires/societe/3021457-les-francais-qui-n-ont-pas-invente-internet.html)


## le web

* n'est pas Internet, mais fonctionne grâce à l'Internet
* n'est ni Google, ni facebook
  * encore moins chrome ou firefox
* inventé par [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Tim Berners Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee) début 90
  * il est conçu comme une mine d'informations
    * pas comme un lieu de transactions économiques
      * tout est ouvert
        * [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Wikipédia](https://wikipedia.org) incarne les concepts fondamentaux du web


## le web

* repose sur le parcours discursif
  * lien hypertexte
    * [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> URL (Uniform Resource locator)](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator)

<br />

![url](images/jnarac/www/url.jpg "url")<!-- .element: width="55%" -->

* supporté par un protocole [HTTP](../1337/http.html)

Note:
- faisaez gaffe aux urls!!


## le web n'oublie jamais

[![wayback machine](images/jnarac/www/waybackmachine.png "Waybackmachine")](http://web.archive.org/web/20020331020421/http://vmazenod.free.fr/)

Note:
  - waybackmachine vous connaissez?
  - le tout est d'assumer ses coupes de cheveux
  - et ses propos antérieurs, on change, l'environnement change


## <i class="fa fa-ambulance" aria-hidden="true"></i>  le web n'oublie jamais

* Désactiver les partages automatiques

  * de localisation
  * de photos

* Réfléchir avant de mettre quoique ce soit en ligne
  * photos
  * partages d'information

* Se protéger et protéger les autres
  * Enfants, famille, amis, collègue, employeurs


## le web est résiliant

[![Effet Streisand](images/jnarac/www/Streisand_Estate.jpg "Effect Streisand")](http://fr.wikipedia.org/wiki/Effet_Streisand)

Note:
- effet Streisand en 2003
    - poursuite du photographe diffuseur
        - 420 000 visistes le mois suivant
            - l'image sur wikipedia en Creative common
                - ce qu'on essaie de supprimer peut rester


## <i class="fa fa-ambulance" aria-hidden="true"></i> le web est résiliant

* Faire valoir [son droit au déréférencement](https://www.cnil.fr/fr/le-droit-au-dereferencement)
  * suppression des résultats des moteurs de recherche
    * ne supprime pas l'information du web
* [Google Search Console (ex webmaster tools)](https://www.google.com/webmasters/tools/home?hl=fr&pli=1)
  * si vous avez "la main sur la page"
