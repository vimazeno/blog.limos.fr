# SSI

![SSI](images/jnarac/ssi/vigipirate.jpg "SSI")<!-- .element width="40%" -->


### Problématique nationale

#### [La défense en profondeur](http://circulaire.legifrance.gouv.fr/pdf/2009/04/cir_2014.pdf)

![fortification à la Vauban](images/jnarac/ie/Vauban_Fortifications.jpg "fortification à la Vauban")<!-- .element width="45%" style="float: right;margin: 15px;"-->

exploiter plusieurs techniques de sécurité afin de réduire le risque lorsqu'un composant particulier de sécurité est compromis ou défaillant

<small>Sébastien Le Prestre de Vauban</small>


## Stratégie de la défense en profondeur

* n'évite pas l'attaque
  * ralentit l'attaquant
    * chacun est un maillon des chaînes fonctionnelles Sécurité Défense & SSI et doit

<br>

### Sécurité = réduire le risque = rendre les attaques coûteuses


### Stratégie de la défense en profondeur

Chaque maillon est reponsable

* de l'analyse des risques inhérents à son périmètre pour mieux les maîtriser
* de l'anticipation & de la prévention des accidents et des actes de malveillance
* de l'amélioration continuelle de la sécurisation de son périmètre
  * le risque 0 n'existe pas
  * la sécurité peut toujours être améliorée


## la chaîne Sécurité Défense

![la chaîne Sécurité Défense](images/jnarac/ie/chaine_fonctionnelle.png "la chaîne Sécurité Défense")<!-- .element width="80%" -->


#### la chaîne Sécurité Défense

* Le SGDSN
  * service du premier ministre
    * pilote de la politique nationale en matière de SSI
      * pour chaque ministère
        * un HFDS
        * un conseiller du ministre pour la défense, la sécurité et la vie de la nation

  * s'appuie sur l'ANSSI


## la chaîne SSI

![Organisation nationale](images/jnarac/ie/organisation_nationale.png "Organisation nationale")<!-- .element width="45%" -->


## la chaîne SSI

* Le HFDS désigne et dirige
  * pour chaque ministère
    * un FSSI
      * charger de porter la réglementation SSI vers chaque établissement publique
        * un AQSSI par éatblissement
        * un RSSI par établissement
          * chargés de la gestion et du suivi des moyens de sécurité des SI


## l'ANSSI

[![ANSSI](images/jnarac/ie/Anssi.png "ANSSI")](http://www.ssi.gouv.fr/)


## l'ANSSI

* force d'intervention (CERT-FR) & de prévention
* contribue à l'élaboration de la stratégie nationale et européenne SSI
  * [EBIOS Expression des Besoins et Identification des Objectifs de Sécurité](https://www.ssi.gouv.fr/guide/ebios-2010-expression-des-besoins-et-identification-des-objectifs-de-securite/)
  * [Livre blanc sur la sécurité et la défense nationale](http://www.livreblancdefenseetsecurite.gouv.fr/)
    * renforcé par la LPM
      * protège 218 [OIV (Opérateurs d'Importance Vitale)](http://fr.wikipedia.org/wiki/Op%C3%A9rateur_d'importance_vitale) en France


#### ... d'importance Vitale

[SAIV (Secteur d'Activités d'Importance Vitale)](http://www.sgdsn.gouv.fr/site_rubrique70.html) - selon article R1332-2 du Code de la défense français

* **Secteurs étatiques**: activités civiles de l’Etat, activités militaires de l’Etat, activités judiciaires
* **Secteurs de la protection des citoyens**: santé, gestion de l'eau, alimentation
* **Secteurs de la vie économique et sociale de la nation**: énergie, communication, électronique, audiovisuel et information (les quatre représentent un secteur), transports, finances, industrie
* La liste exhaustives est secret défense


## Tour du monde SSI
* l'Allemagne ont le [BSI](http://www.bsi.bund.de)
* les Pays-Bas ont la NLNCSA
* l'Union Européenne a l’[ENISA](http://www.enisa.europa.eu)
* le Royaume-Uni a le [GCHQ](http://www.gchq.gov.uk) et le [CESG](http://www.cesg.gov.uk)
* les États-Unis ont la [NSA](http://www.nsa.gov/ia/) et son programme PRISM et le [DHS](http://www.dhs.gov/cyber)
  * [Five Eyes](https://fr.wikipedia.org/wiki/Five_Eyes)


## Tour du monde SSI

* Israël a l'[Unité 8200](http://fr.wikipedia.org/wiki/Unit%C3%A9_8200) et un programme de [cyber bouclier](http://tsahal.fr/2012/04/29/tsahal-en-2012-cest-aussi-la-guerre-cybernetique/)
* la Chine a l'[unité 61 398](http://www.liberation.fr/monde/2013/02/20/unite-61-398-l-armee-des-hackers-chinois_883298)
* la Russie  a [le programme SORM et le FSB](http://themoscownews.com/russia/20130617/191621273.html)
* la Corée du nord aurait une [armée de 200 trolls & 3000 cyberguerriers](http://french.ruvr.ru/news/2013_08_13/La-Coree-du-Nord-a-forme-une-armee-de-hackers-et-de-trolls-7236/)
