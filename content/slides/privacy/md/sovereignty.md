# Souveraineté

# <i class="fa-solid fa-power-off"></i>

Si tu n'utilises pas des outils que tu contrôles ...

tu es contrôlé par ces outils


## Hacking

[![Richard Stallamn - RMS](images/sovereignty/stallman.jpg "RMS")<!-- .element width="20%"-->](https://fr.wikipedia.org/wiki/Richard_Stallman)
[![Eric Steven Raymond - ESR](images/sovereignty/raymond.jpg "ers")<!-- .element width="37%" -->](https://fr.wikipedia.org/wiki/Eric_Raymond)

* Black Hat, white Hat, Grey Hat
* Ethical
  * [une brève histoire des hackers](https://usbeketrica.com/fr/article/une-breve-histoire-des-hackers)
  * [code is law](http://framablog.org/2010/05/22/code-is-law-lessig/)


## logicel libre

* Concentré sur la maîtrise de l'outil par l'utilisateur
* 4 libertés fondamentales
  * la liberté d'utiliser le logiciel
  * la liberté de copier le logiciel
  * la liberté d'étudier le logiciel
  * la liberté de modifier le logiciel et de redistribuer les versions modifiées
* La viralité
  * **🄯 le copie left** ~~©~~


## Open source

* Concentré sur la façon de concevoir le code
* [La cathédrale et le bazar](https://fr.wikipedia.org/wiki/La_Cath%C3%A9drale_et_le_Bazar)

* Même Microsoft s'ouvre
  * ["An Open Letter to Hobbyists" Bill Gates, 1976](https://fr.wikipedia.org/wiki/An_Open_Letter_to_Hobbyists)
  * ["Linux is a cancer" Steve Ballmer, 2001](http://www.theregister.co.uk/2001/06/02/ballmer_linux_is_a_cancer/)
  * [github](https://github.com), [type script](https://www.typescriptlang.org/), [vscode](https://code.visualstudio.com/), [et autres ...](https://opensource.microsoft.com/projects/)


## Logiciel privateur

[![Logiciel Propriétaire](https://upload.wikimedia.org/wikipedia/commons/1/14/Classification_des_licences.svg)](https://fr.wikipedia.org/wiki/Logiciel_propri%C3%A9taire)


## logicels libres & opensources

* OS: [Certains linux sont libres](https://www.gnu.org/distros/free-distros.fr.html), mais [pas ubuntu](https://www.developpez.com/actu/332740/Richard-Stallman-s-exprime-sur-l-etat-du-mouvement-du-logiciel-libre-et-declare-que-les-Macintosh-continuent-d-etre-des-prisons-pour-les-utilisateurs/)

* logiciels: [framasoft](https://framasoft.org/), [alternativeto](https://alternativeto.net/), [liste des logiciels libres](https://fr.wikipedia.org/wiki/Liste_de_logiciels_libres) (tiens il y a ubuntu?)

* Android: [LineageOS](https://lineageos.org/), [F-DROID](https://f-droid.org/fr/), [<i class="fab fa-reddit"></i> best android ROM for privacy](https://www.reddit.com/r/privacy/comments/6d3a33/best_android_rom_for_privacy/)


## Services

[Degooglisons Internet](https://degooglisons-internet.org/)

* Installer ses propres services
  * sur des machines accessibles physiquement si possible
    * enjeux des connexions personnelles
* Utiliser des services décentralisés
* Déjouer le [capitalisme de surveillance](https://www.zulma.fr/livre/lage-du-capitalisme-de-surveillance/)


## Services

* [OwnCloud](https://owncloud.org/) / [Nextcloud](https://nextcloud.com/) > [framadrive](https://framadrive.org/) ou [service-public.fr](https://www.service-public.fr/assistance-et-contact/aide/compte#Comment%20stocker%20vos%20documents%20) > [Google Drive](https://drive.google.com) / [Dropbox](https://www.dropbox.com) / [One Drive](https://onedrive.live.com)
* [OwnCloud](https://owncloud.org/) / [Nextcloud](https://nextcloud.com/) > [framagenda](https://framagenda.org)  > [Google calendar](https://calendar.google.com)
* [Etherpad](http://etherpad.org/) > [Framapad](https://framapad.org/) > [Google Docs](https://docs.google.com)
* [Postfix](http://www.postfix.org/) > [Proton mail](https://protonmail.com/) >  [GMail](https://mail.google.com/)
* [Qwant](https://www.qwant.com/) >  [DuckDuckGo](https://duckduckgo.com/) > [Google](https://www.google.fr)
  
<div style="text-align: center">
  <a href="https://www.chatons.org">
    <img src="https://www.chatons.org/sites/default/files/uploads/logo_chatons.png" style="width: 200px"/>
  </a>
</div>


## Auto-hébergement

[![raspberry](images/sovereignty/raspberry.png)<!-- .element width="45%" -->](https://www.raspberrypi.org/)

* matériel open source + connexion personnelle + logiciels libres = **<3**
* [yunohost ](https://yunohost.org/#/)
* [ispconfig](https://www.ispconfig.org/)
* [La brique Internet](https://labriqueinter.net/)
