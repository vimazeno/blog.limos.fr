## RGPD

![RGPD](images/jnarac/law/gdpr.jpg)


## RGPD

Règlement général sur la protection des données [UE]

* [Comprendre le règlement européen](https://www.cnil.fr/fr/comprendre-le-reglement-europeen)
* [Règlement européen sur la protection des données : ce qui change pour les professionnels](https://www.cnil.fr/fr/reglement-europeen-sur-la-protection-des-donnees-ce-qui-change-pour-les-professionnels)
* [**07 octobre 2015** - Invalidation du « safe harbor » par la CJUE : une décision clé pour la protection des données](https://www.cnil.fr/fr/invalidation-du-safe-harbor-par-la-cour-de-justice-de-lunion-europeenne-une-decision-cle-pour-la-0)
* En France la CNIL devient autorité de contrôle


## Périmètre

* Prise de position
* Réglement européen
  * envigueur le 25 mai 2018
  * Dans tous les états membres de l'UE
* <strike>Déclaration</strike>
  * responsabilité
    * démonstration permanente du maintien de la conformité


## Périmètre

* Tout service ou sous traitant (y compris hors UE) traitant des données de résidents de l'UE
  * Entreprises
  * Associations
  * Organismes publics
  * Sous traitants


## Objectifs

* Renforcer la transparence
  * Quelles données sont collectées?
  * Dans quels buts?
  * Pour combien de temps?


## Objectifs

* Faciliter l'exercice des droits
  * droit à la rectification
  * droit à la portabilité
    * récupération
    * communication à un autre traitement


## Objectifs

* Faciliter l'exercice des droits
  * droit à l'oubli
    * suppression de données personnelles
      * dès qu'elles ne sont plus nécessaires au traitement
      * dès que le consentement de l'utilisateur a été retiré
      * dès que la personne s'y oppose


## Objectifs

* Crédibiliser la régulation
  * Réclamation auprès de l'autorité de contrôle
  * Droit de recours contre le responsable du traitement ou un sous traitant
  * Actions collectives ou personnelles
  * Sanctions
    * 4% du chiffre d'affaire annuel mondial
    * 20 000 000 €


## Objectifs

* Responsabiliser les acteurs traitant des données
  * Obligation d'information en cas de violation de données à caractère personnel
    * autorité de contrôle
    * La personne concernée


## Principes

* Accountability
  * tous responsables
  * tous auditables
  * <strike>Déclaration CNIL</strike>
* Privacy By Design
  * Portection des données pris en compte du début
* Security By Default
  * Mesures de sécurité nécessaires
  * Détection de compromission


## Principes

* DPO (Data Protection Officer)
  * conformité RGPD
  * Point de contact avec les autorités
* Analyse d'impact (PIA)
  * [un logiciel pour réaliser son analyse d’impact sur la protection des données (PIA)](https://www.cnil.fr/fr/rgpd-un-logiciel-pour-realiser-son-analyse-dimpact-sur-la-protection-des-donnees-pia)


## [RPGD : en 6 étapes (CNIL)](https://www.cnil.fr/fr/principes-cles/rgpd-se-preparer-en-6-etapes)

1. Désigner un pilote (DPO/DPD)
2. Cartographier
3. Prioriser
4. Gérer les risques
5. Organiser
6. Documenter

### Prouver la conformité = Avoir la documentation nécessaire


## Transfert des données hors UE

* Vérifier que le pays vers lequel vous transférez les données est reconnu comme adéquat par la Commission européenne
  * Dans le cas contraire, encadrez vos transferts


## Données sensibles

* origines raciales ou ethniques
* opinions politiques, philosophiques ou religieuses
* appartenance syndicale
* santé
* orientation sexuelle
* génétiques ou biométriques
* infraction ou de condamnation pénale
* sur les mineurs

Soumises à autorisation de la CNIL


## dispositifs spéciaux

* Recherche publique
  * [Protection du Potentiel Scientifique et Technique de la nation](http://fr.wikipedia.org/wiki/Protection_du_potentiel_scientifique_et_technique_de_la_nation_%28PPST%29)  
* Données de santé
  * [ASIP santé - L'agence française de la santé numérique](http://esante.gouv.fr/)
* Etablissements de crédit
  * [Garanties spécifiques de sécurité](http://www.fbf.fr/fr/contexte-reglementaire-international/cadre-juridique/les-principaux-textes-regissant-le-secteur-bancaire-francais)
