## Lectures

<iframe border=0 frameborder=0 height=250 width=550 src="https://twitframe.com/show?url=https://twitter.com/Klubdesloosers/status/17401418682"></iframe>


## L'utopie numérique

[![Aux sources de l'utopie numérique](images/jnarac/books/avecwholeEarth.jpg "Aux sources de l'utopie numérique")<!-- .element: width="50%" -->](http://cfeditions.com/Turner/)


## Le cimetière de prague

![Le cimetière de Prague](images/jnarac/books/le-cimetiere-de-prague.jpg)


## La cathédrale et le bazar

[![La Cathédrale et le Bazar](images/jnarac/books/Cathedral-and-the-Bazaar-book-cover.jpg)<!-- .element: width="35%" -->](https://fr.wikipedia.org/wiki/La_Cath%C3%A9drale_et_le_Bazar)

* [Eric Raymond](https://fr.wikipedia.org/wiki/Eric_Raymond)


## Surveillance://

[![surveillance://](images/jnarac/books/surveillance_nitot.jpg)<!-- .element: width="60%" -->](http://standblog.org/blog/pages/Surveillance)

* [Tristan Nitot](https://fr.wikipedia.org/wiki/Tristan_Nitot)


### Attentifs ensemble!

[!["Attentifs ensemble!"](images/jnarac/books/thorel.jpg "Attentifs ensemble!")<!-- .element: width="35%" -->](http://www.editionsladecouverte.fr/catalogue/index-Attentifs_ensemble__-9782707174215.html)    


## Les marchands de peur

[![Les Marchands de peur](images/jnarac/books/Les-marchands-de-peur.jpg)<!-- .element: width="25%" -->](http://editionslibertalia.com/catalogue/a-boulets-rouges/Les-Marchands-de-peur)

* [Mathieu Rigouste](https://fr.wikipedia.org/wiki/Mathieu_Rigouste)


## Guide d'auto défense numérique

[![Guide d'auto défense numérique](images/jnarac/books/gadn.png)<!-- .element: width="40%" -->](https://guide.boum.org/)

Note:
- une suggestion de lecture pour les RSSI dans la salle
- permet d'être sur la même longueur d'onde que les geeks dans mon genre
- ouvrage non conventionel de SSI
- Faire marcher le bon sens
- bon pour tous
- ne pas croire aux listes noires mais au listes blanches
- analyse de risque personnel
- 15 €
- Prendre du recul ... voulez vous prendre un peu plus de recul?


## La société du risque

[![La société du risque](images/jnarac/books/la_societe_du_risque.jpg "La société du risque")](http://www.amazon.fr/La-soci%C3%A9t%C3%A9-risque-autre-modernit%C3%A9/dp/2081218887)

Note:
- horizons entièrement contrôlés et modelés par l'homme lui même
   - société post industrielle complètement affranchie des contraintes de la nature
       - l'homme n'est alors plus menacé par son environnement mais par la manière dont il le transforme
           - il gère du coup des risques
               - qu'il subit
               - dont il est lui même acteur
- La sécurité entre exactement dans ce paradigme
   - il n'y a pas de "bonnes décisions", ni de "systèmes sécruisés"
       - même hélise lucet le dit dans cash investigation  "busines de la peur"
   - il n'y a que des scénarios plus ou moins probables, des opportunités et seulement 4 comportements possibles face au risque pour le gérer
       - réduire le risque
       - accepter le risque
       - déporter le risque
       - supprimer le risque (ne pas le prendre)


## Norbert wiener cybernétique et société

[![La Cybernétique et société](images/jnarac/books/Cybernetique-et-societe.jpg)<!-- .element: width="35%" -->](http://www.seuil.com/ouvrage/cybernetique-et-societe-norbert-wiener/9782757842782)

* [Norbet Wiener](https://fr.wikipedia.org/wiki/Norbert_Wiener)
