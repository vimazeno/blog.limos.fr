## Surveillance

!["surveillance"](images/jnarac/surveillance/obama-prism-raccroche.jpg "surveillance") <!-- .element: width="70%" -->

Note:
- Barack
- snowden juin 2013
- NSA
- Prism
  - grand retour en arrière au moins en facade
    - bad for business


## Chine

![Chines cop](images/jnarac/surveillance/chinese_cop.jpg)<!-- .element width="85%" -->

[<small>Face Recognition Glasses Augment China’s Railway Cops</small>](http://www.sixthtone.com/news/1001676/face-recognition-glasses-augment-chinas-railway-cops)
[<small>La Chine met en place un système de notation de ses citoyens pour 2020</small>](http://www.lefigaro.fr/secteur/high-tech/2017/12/27/32001-20171227ARTFIG00197-la-chine-met-en-place-un-systeme-de-notation-de-ses-citoyens-pour-2020.php)


## whistle blowers

![Whistle blowers](images/jnarac/surveillance/whistle_blower.jpeg)<!-- .element width="85%" -->

Note:
- cypher punk
    - libertarien
    - le renseignement est il à combattre?


## Surveillance En France

!["surveillance"](images/jnarac/surveillance/valls-prism-raccroche.jpg "surveillance")<!-- .element: width="70%" -->

Note:
- France
- 7 janvier 2015 (Je suis Charlie)
- Je suis sur écoute
- Loi sur le renseignement
  - qui abroge un cadre légal existant
    - LIL
      - le business en fera les frais
      - la démocratie aussi


## LIL

[!["SAFARI ou la chasse aux Français"](images/jnarac/surveillance/safari.jpg "SAFARI ou la chasse aux Français")](http://fr.wikipedia.org/wiki/Syst%C3%A8me_automatis%C3%A9_pour_les_fichiers_administratifs_et_le_r%C3%A9pertoire_des_individus)
[!["cnil"](images/jnarac/surveillance/cnil.jpg "cnil")](http://www.cnil.fr/)


#### « Quand on consulte des images de djihadistes, on est un djihadiste. »

![« Quand on consulte des images de djihadistes, on est un djihadiste. »](images/jnarac/surveillance/sarko.jpg "« Quand on consulte des images de djihadistes, on est un djihadiste. »")<!-- .element width="50%" -->

![« Merde je suis Emma Watson nue. »](images/jnarac/surveillance/emma-watson.png "« Merde je suis Emma Watson nue. »")<!-- .element width="50%" -->


#### Mégafichier TES

![Méga fichiers TES](images/jnarac/surveillance/megafichier_TES.jpg)<!-- .element width="25%" -->

* [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Fichage en France](https://fr.wikipedia.org/wiki/Fichage_en_France)
* [Le «fichier des gens honnêtes»](http://www.slate.fr/story/138356/saga-generalisation-fichier-des-gens-honnetes)
* [Le «mégafichier» étendu au pas de charge](http://www.liberation.fr/futurs/2017/02/21/le-megafichier-etendu-au-pas-de-charge_1549968)
