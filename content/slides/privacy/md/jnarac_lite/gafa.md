## GAFAM / BATX

![GAFAM/BATX](images/jnarac/gafa/gafam_batx.png)<!-- .element style="width: 80%" -->


## GAFAM / BATX

### GAFAM

Google + Apple + Facebook + Amazon + Microsoft

### BATX

Baidu + Alibaba + Tencent + Xiaomi


### Différents modèles économiques

![ads](images/jnarac/gafa/ads.jpg "ads")<!-- .element: width="70%" -->

* [Shot de dopamine : ce que Facebook fait au cerveau de mon amie Emilie](https://www.nouvelobs.com/rue89/notre-epoque/20171222.OBS9715/shot-de-dopamine-ce-que-facebook-fait-au-cerveau-de-mon-amie-emilie.html) #neuromarketing


## pour un seul mantra ...

![alt text](images/jnarac/gafa/dontbeevil.jpg "don't be evil")<!-- .element: width="70%" -->

### *"make the world a better place*" ...

Note:
- pas de méchant
- que des bienfaiteurs de l'humanité
- ricains, tout est à vendre, tout est business


## For my business!

![Epubs stats](images/jnarac/gafa/epub-stats.png "Epubs stats!")<!-- .element: width="60%" -->

#### avec optimisation fiscale

Note:
- ça rapporte, et surtout ça croît à fond
- ce qu'on sait le mieux à moins de frais: c'est créer de l'information
- en faire générer par l'utilisateur, le consommateur et faire du business avec

## Si c'est gratuit
### c'est l'utilisateur le produit!

![Si c'est gratuit, c'est vous le produit!](images/jnarac/gafa/pigs-and-the-free-model.jpg "Si c'est gratuit, c'est vous le produit!")<!-- .element: style="width:40%;" -->


## Données personnel

![Cartographie des données personnelles](images/jnarac/gafa/carto_donnees_personnelles.png)<!-- .element: width="60%" -->

### Le nouveau pétrole


## Ce que <i class="fa fa-google" aria-hidden="true"></i> & <i class="fa fa-facebook" aria-hidden="true"></i> savent de vous

* [Google Mon activité](https://myactivity.google.com/myactivity)
* [Google position history - 20 novembre 2014](https://maps.google.com/locationhistory/b/0)
* [Google passwords](https://passwords.google.com/)
* Google [analytics](https://www.google.com/intl/fr/analytics/), [AdSense](https://www.google.fr/adsense/start/), [AdWords](https://adwords.google.com)
* [facebook peut savoir ce que vous n'avez pas osé publier](http://www.numerama.com/magazine/32751-facebook-peut-savoir-ce-que-vous-n-avez-pas-ose-publier.html)
* [facebook stockerait des données sur les non membres](http://www.numerama.com/magazine/20237-facebook-stockerait-des-donnees-sur-les-non-membres.html)
* [hello facebook identifie les numéros inconnus](http://www.numerama.com/magazine/32889-avec-hello-facebook-identifie-les-numeros-inconnus.html)


## Ce que <i class="fa fa-google" aria-hidden="true"></i> & <i class="fa fa-facebook" aria-hidden="true"></i> changent

* [Deep Face - reconnaissance faciale](https://research.facebook.com/publications/480567225376225/deepface-closing-the-gap-to-human-level-performance-in-face-verification/)
* [Picasa](http://www.google.com/intl/fr/picasa/) et la reconnaissance automatique notamment des gens dans le temps
* [Êtes-vous déprimés ? Demandez à Google](https://www.sciencesetavenir.fr/sante/etes-vous-deprimes-demandez-a-google_115784)
* [Facebook détecte notre classe sociale. Et déclenche la lutte (algorithmique) finale](http://affordance.typepad.com/mon_weblog/2018/02/cest-la-lutte-algorithmique-finale-.html)
  * [Patent  "Socioeconomic group classification based on user features"](http://pdfaiw.uspto.gov/.aiw?PageNum=0&docid=20180032883&IDKey=175CD64C2991&HomeUrl=http%3A%2F%2Fappft.uspto.gov%2Fnetacgi%2Fnph-Parser%3FSect1%3DPTO1%2526Sect2%3DHITOFF%2526d%3DPG01%2526p%3D1%2526u%3D%25252Fnetahtml%25252FPTO%25252Fsrchnum.html%2526r%3D1%2526f%3DG%2526l%3D50%2526s1%3D%25252220180032883%252522.PGNR.%2526OS%3DDN%2F20180032883%2526RS%3DDN%2F20180032883)
* [La tyrannie des agissants](https://resistanceauthentique.net/tag/tyrannie-des-agissants/)
