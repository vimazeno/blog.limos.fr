<!-- .slide: data-background="images/jnarac/jnarac2/jnarac.png" data-background-size="150%" style="color: white" class="trsp bottom"-->
## "Je n'ai rien à cacher"
comprendre le présent pour penser le futur

Note:
- je ne répète pas je donne les réponses aux questions qui sont posées sur la feuille que vous avez prises
- j'ai plein de trucs à dire
- je vais aller vite
- donc je pointe du doigt les bavards et ils sortent discrètement
    - merci
- je suis Vincent Mazenod
- je suis blanc, j'ai 50 ans
- hétéro sexuel, 2 enfants
- fonctionnaire, informaticien
- je suis baptisé, non pratiquant, et plutot agnostique
- je joue de la cornemuse et j'aime les musiques du monde
- j'ai été bercé par le rock 70s et le punk 
- bref moi je n'ai rien à cacher et si

----

## (Res)sources

* [🎥 Burning Man](https://www.youtube.com/watch?v=i9GfIxSLL94)

---

## Si je n'ai rien à cacher ...
![](images/jnarac/index/wc.jpg)

----

## (Res)sources

* [🔗 rien à cacher](https://fr.wikipedia.org/wiki/Rien_%C3%A0_cacher_(argument))
* [🔗 je n'ai rien à cacher](https://jenairienacacher.fr/)

---

#### ... alors je n'ai rien à craindre
![](images/jnarac/index/nothing_to_hide_nothing_to_fear.jpg)<!-- .element: style="width: 400px"-->

Note:
- c'est le complément implicite
- rien à craindre de qui?
  - de la loi 
    - donc de l'état qui possède le monopole de la violence 
      - pour faire respecter la loi
- cette patite musique est un appel la transparence de chacun au nom de la sécurité la sécurité de tous
- la novlangue d'Orwel dans 1984
  - le langage structure ma pensée
  - celui qui controle langage a le pouvoir
    - islamogauchisme
    - wokisme
    - frappe chirurgical
    - ecoterroriste
    - vidéoprotection 
    - ...

----

## (Res)sources

* [🔗 Novlangue (*wikipedia*)](https://fr.wikipedia.org/wiki/Novlangue)
* [🔗 Quand la "vidéoprotection" remplace la "vidéosurveillance" (*Le monde*)](https://www.lemonde.fr/societe/article/2010/02/16/quand-la-videoprotection-remplace-la-videosurveillance_1306876_3224.html)
* [🎥 sur la manipulation des mots (*Franck Lepage*)](https://www.youtube.com/watch?v=mD-G5lHXviY)
* [🎥 Le mot qui a remplacé "Hiérarchie" (*Franck Lepage*)](https://www.youtube.com/watch?v=IUB1XsT5IQU)
* [🎥 La langue de bois décryptée avec humour ! (*Franck Lepage*)](https://www.youtube.com/watch?v=oNJo-E4MEk8)

---

#### Vraiment!?
![](images/jnarac/index/nothing_to_fear.jpg)<!-- .element: style="width: 500px"-->

Note:
- On a un premier problème
  - tout ce qui est légal, n'est pas forcément moral
- L'homosexualité était illégale à bien plus d'endroit
- Pour qu'une socicété évolue il faut qu'elle puisse penser l'interdit
  - la vie privée est une condition à l'independance d'esprit
  - l'independance d'esprit est une condition au progressisme

---

#### Pourtant l'injonction est partout!
![](images/jnarac/index/Elf_Surveillance_Santa_Camera.png)
![](images/jnarac/jnarac2/fitbit.png)<!-- .element: style="width: 150px"-->

Note:
- quantified yourself
- géolocalisation
- sécurité
  -  risque 0 / fuite des reponsabilité
    - Vivre tue
- à qui profite cette transparence
- probablement à ceux qui soignent son acceptation de masse

---

<!-- .slide: data-background="images/jnarac/jnarac2/google.png" style="color: white" class="trsp" data-background-size="65%"-->
## Invitation à la prudence

"If you have something that you don't want anyone to know, maybe you shouldn't be doing it in the first place."

"S'il y a quelque chose que vous faites et que personne ne doit savoir, peut-être devriez vous commencer par ne pas le faire."

[Eric Schmidt, PDG de Google, The Register, 7 décembre 2009](http://www.theregister.co.uk/2009/12/07/schmidt_on_privacy/)

Note:
- On comprend bien qu'il sorte cet argument
- Ne mettez pas le nez dans mes affaires, occupez vous plutôt des vôtres
  - Google en c'est plus long sur nous que nous n'avons sur lui non?
  - on va y revenir

---

<!-- .slide: data-background="images/jnarac/jnarac2/fb.png" style="color: white" class="trsp" data-background-size="72%"-->
## Evolution des normes sociales

"Les gens sont désormais à l'aise avec l'idée de partager plus d'informations différentes, de manière plus ouverte et avec plus d'internautes. [...] La norme sociale a évolué."

[Mark Zuckerberg, PDG de Facebook, USTREAM, 10 janvier 2010](http://www.lemonde.fr/technologies/article/2010/01/11/pour-le-fondateur-de-facebook-la-protection-de-la-vie-privee-n-est-plus-la-norme_1289944_651865.html)

---

#### Mais il y a des exceptions

![](images/jnarac/gafa/zuc_cam.png)

Note:
- il a acheté les 3 maisons mitoyennes à la sienne
- les enfants des hypergagnants n'ont accès à aucun écran avant leur 14 ans
- l'adoption des évolutions de société sont donc à double vitesse

---

<!-- .slide: data-background="images/jnarac/jnarac2/dont_be_evil.png" style="color: white" class="trsp" data-background-size="100%"-->
### Naissance d'un modèle économique

* 2001 bulle Internet
* Amit Patel - data scientists
    * avril 2002
        * 5 pics successifs une seule requête 
    * les recherches sont prédictives
* Eric Schmidt devient PDG
    * capitalise sur la découverte

Note:
- «nom de jeune fille de Carol Brady»

---

| Avant  | Après |
| ------ | ----- |
| Les clients sont les utilisateurs | les clients sont les annonceurs |
| ![](images/jnarac/jnarac2/users_client.jpg)<!-- .element style="width: 60%" -->| ![](images/jnarac/jnarac2/ads_client.png) |

---

## de la promesse initale ...

![](images/jnarac/jnarac2/capitalisme_suveillance.png)

---

#### ... à la réalité  ...

![](images/jnarac/jnarac2/capitalisme_suveillance2.png)

---

<!-- .slide: data-background="images/jnarac/jnarac2/sushana.png" style="color: white" class="trsp" -->
## ... du capitalisme de surveillance

* Sushana Zuboff
* surplus comportemental
    * produit de prédictions
        * vendus sur le marché des comportements futurs
    * [pouvoir instrumentarien](https://fr.wikipedia.org/wiki/Scandale_Facebook-Cambridge_Analytica)
    * impératif d'extraction

---

<!-- .slide: data-background="images/jnarac/jnarac2/dollars.png" style="color: white" class="trsp"-->
## Mutation du capitalisme

![](images/jnarac/jnarac2/mutations_capitalisme.png)

Note:

- capitalisme sauvage fin XIX
    - matière première
        - pétrole, acier, banques et IBM
        - monopolistique
    - grosses inégalité
      - famines
      - précarités
      - crises successives
    - oeuvres philantropique 
    - ne participe pas à la redistribution        
- régulé avec l'arrivée de Roosvelt et l'après guerre
    - reconstruction
    - avancée social
    - syndicalisme
    - recouvrement de l'impot pour tous
        - progressif pas proportionnel
    - industrialisation fordisme / traylorisme
        - accessibilité au plsu grand nombre
        - American way of life
        - exportation du modèle
    - éducation
        - alimente l'innovation
    - les grandes fortunes sont en PLS
- ultralibéralisme
    - Reagan / Tatcher (Mitterand) vont arrêter la régulation d'état
        - halte à l'interventionnisme on dérégule
    - laisser la "main invisible du marché" d'Adm Smith (qui n'a jamais écrit ça) agir
        - profite aux grosses fortune
        - recul du syndicalisme
        - hausse des inégalités
    - boum industrielle informatique
    - financiarisation
        - importance des flux de capitaux de l'investissement etc ...
    - mondialisation
        - moins de protectionisme
        - plus d'import / export
        - beaucoup de transport
        - jusqu'à la crise des subprimes
          - fin de la confiance dans les marchés
- capital algorithmique
    - basée sur la technologie informatique
    - Mise en place de plateforme
        - précarisation salariale
        - hausse des inégalités
    - Nouveau modèle lié à la donnée
        - contournement
            - des lois travail
            - de l'impot
    - aussi appelé
        - capitalisme de surveillance (Sushan Zuboff)
        - technoféodalisme ou capital cloud (Yanis Varoufakis)

- Chaque nouvelle version du capitalisme asservie la précédente sans l'effacer
    - on a toujours besoin de pétrole
    - on farbique toujours des voitures
    - le système financier est toujours modial
    - mais pour foncionner la technologie est indispensables partout

----

## (Res)sources

* [📖 L’Âge du capitalisme de surveillance (*Shoshana Zuboff*)](https://www.zulma.fr/livre/lage-du-capitalisme-de-surveillance/)
* [📖 Le capital algorithmique (*Jonathan Martineau & Jonathan Durand Folco*)](https://ecosociete.org/livres/le-capital-algorithmique)

---

<!-- .slide: data-background="images/jnarac/jnarac2/karl_marx.png" style="color: white" class="trsp"-->
## le capitalisme

* infrastructure
  * outil de production
  * force de travail
* suprastructure
  * institutions
  * lois
    * ordre social
      * propriété privée 
* économie de marché

Note:
- la propriété est présenter comme une liberté dans la déclaration des droits de l'homme
  - c'est une vulnérabilité juridique
  - elle permet d'user mais aussi d'abuser de tout ce dont on est propriétaire
  - la propriété est le vol? Prudhon - a brûlé

---

<!-- .slide: data-background="images/jnarac/jnarac2/banksy_capitalisme.png" style="color: white" class="trsp bottom"-->
#### croissance & capital

* (M) les marchandises achetées 
![](images/jnarac/jnarac2/capital_process.png)<!-- .element style="float: right" -->
* (C) capital fixe 
* (V) capital variable
* (M') marchandises produites
* (PL) plus-value  
* (A') argent de la vente
* (P) profit

----

## (Res)sources

* [📖 Capital et Idéologie en bande dessinée](https://www.seuil.com/ouvrage/capital-et-ideologie-en-bande-dessinee-claire-alet/9782021469578)
* [📖 Le choix du chomage](https://www.futuropolis.fr/9782754825450/le-choix-du-chomage.html)

---

<!-- .slide: data-background="images/jnarac/jnarac2/banksy_sauvage.png" style="color: white" class="trsp"-->
#### Le capital en pratique

* prendre des objets qui existent en dehors de la dynamique du marché
    * les insérer dans cette dynamique du marché
        * les transformer en produits vendables
            * **marchandises fictionnelles**
* accaparement
* impéractif extractiviste
  * extension du champs de collecte
  * systématisation
* réduire les marges de production  
  * cout du travail
  * échapper aux taxes

---

<!-- .slide: data-background="images/jnarac/jnarac2/avatar.jpeg" style="color: white" class="trsp bottom"-->
#### Le surplus comportemental est une marchandise fictionnelle

* on fait vos poubelles à votre insu
  * on vous le cache
* on transforme vos déchets en avatar comportemental
  * à l'insu de votre plein gré
* on utilise cette avatar pour 
  * prédire
  * influencer
  * contrôler

Note:
- message target super personnalisé
  - individualisme VS classe
    - destruction de la lutte des classe

---

<!-- .slide: data-background="images/jnarac/jnarac2/world-clipart-md.png" data-background-size="50%" style="color: white" class="trsp bottom"-->
## vers une datafication totale

* avant tout marchandisation
  * activité online
  * mobilité
  * achats
  * interactions sociales

Note:
- uber (eat) / deliveroo
- airbnb
- spotify
    - nouveaux intermédiaires numériques
        - qui se sont rendu indispensables des maisons de disques, leader sur son marché d'ici là
- onflyFans
- applis de rencontres

---

<!-- .slide: data-background="images/jnarac/jnarac2/digital_labor.jpg" class="trsp" style="color: white" -->

## à moindre coût

<table>
<tr>
<td>
* scalabilité de l'infrastructure
* digital labor
  * PMA/PVD 
* ubérisation 
* surplus comportemental
* et le reste ... ![](images/jnarac/jnarac2/like_fb.png)<!-- .element: style="width: 20px" --> ![](images/jnarac/jnarac2/like_insta.png)<!-- .element: style="width: 20px" --> ![](images/jnarac/jnarac2/like_tiktok.png)<!-- .element: style="width: 20px" -->  

![](images/jnarac/jnarac2/tweet_moderation.png)

</td>
<td>

![](images/jnarac/jnarac2/recaptcha.png)<!-- .element: style="width: 200px" -->  
![](images/jnarac/jnarac2/google_advice.png)<!-- .element: style="width: 200px" -->  
</td>
</tr>
</table>

Notes:
- ils possèdent l'infra
  - AWS / Google cloud / Azure
  - si on complète avec les projets 
    - facebook qui déploie le réseau en Afrique
    - Google fiber
- Mekanical Turk On parle micro travail, micro tache, travailleurs du clics
- uberfiles casse du code du travail, accueilli par nos démocraties
- si nos déchets sur les plateformes permettent d'enrichir les plateformes
  - c'est qu'utiliser les plateformes
  - c'est travailler pour les plateformes
- temps de travail VS temps de loisir sur 50 ans

----

## (Res)sources

* [🔗 Monopole. En Afrique, Facebook pratique le colonialisme numérique](https://www.courrierinternational.com/article/monopole-en-afrique-facebook-pratique-le-colonialisme-numerique-webfirst-nouveau-site)
* [🔗  Google Fiber](https://fr.wikipedia.org/wiki/Google_Fiber)
* [🔗 Google Recaptcha is not an anti-fraud solution](https://www.evina.com/google-recaptcha-is-not-an-anti-fraud-solution/)
* [🔗 UberFiles](https://fr.wikipedia.org/wiki/Uber_Files)


---

<!-- .slide: data-background="images/jnarac/jnarac2/technofeodalisme.png" class="trsp bottom" style="color: white" -->
#### Le capitalisme est peut être déjà mort!

* techno serf
* techno prolo
* techno vassauts
* capital cloud

Note:
- grand bon en arrière

----

## (Res)sources

* [🎥 “LE CAPITALISME EST DÉJÀ MORT”](https://www.youtube.com/watch?v=qDJh9YyM3nc)
* [📖 Technoféodalisme - Cédric Durand](https://www.librairielesvolcans.com/livre/9782355221156-technofeodalisme-critique-de-l-economie-numerique-cedric-durand/)
* [📖 Les nouveaux serfs de l'économie - Yanis Varoufakis](https://www.librairielesvolcans.com/livre/9791020924186-les-nouveaux-serfs-de-l-economie-yanis-varoufakis/)

---

<!-- .slide: data-background="images/jnarac/jnarac2/who-watches-the-watchmen-graffiti.jpg" -->

---

<!-- .slide: data-background="images/jnarac/jnarac2/unclesam.png" data-background-size="55%" class="trsp" style="color: white" -->
### No limit? 

* BigTechs & Bigstates 
  * relations symbiotiques
    * consommateurs 
    * victimes 
    * profiteurs
* Projet commun de technologie totale
* Militarisation des esprits

----

## (Res)sources
* [📖 Technopolitique : Comment la technologie fait de nous des soldats](https://www.librairielesvolcans.com/livre/9782021548549-technopolitique-comment-la-technologie-fait-de-nous-des-soldats-asma-mhalla/)

---

#### En Chine le message est clair

![](images/jnarac/jnarac2/china1.jpg)<!-- .element style="width: 80%" -->

Note:
- point sur la constitution de l'Internet chinois
  - cloisonné pendant des années
    - comme les russes
- le rend indépendant
  - résilient
  - avec la barrière de la langue
    - impénétrable pour les marchés extérieurs
      - Google n'a pas pénétré le marché chinois BATX
      - les chinois ont pénétré le marché US avec TikTok
        - un vrai missile anti aérien

---

## ... la collecte est organisée

![](images/jnarac/jnarac2/china2.jpg)

---

#### ... et tout le monde participe

![](images/jnarac/jnarac2/china3.jpg)<!-- .element style="width: 80%" -->

---

<!-- .slide: data-background="images/jnarac/jnarac2/trump-musk.png" class="trsp" data-background-position="top" -->

# USA?

---

#### En france?

![](images/jnarac/surveillance/macron_anon.png)

Note:
- Face aux enjeux de sécuité 
  - terrorisme
  - harcèlement
  - vol d'identité?
    - usurpation du RSA (607.75€)
- Face aux forces en présence
    - BigTech
    - l'état cherche où exercer son pouvoir
    - notre renseignement est traité par palantir

----

## (Res)sources

* [📖 Fichage en france](https://fr.wikipedia.org/wiki/Fichage_en_France)
* [📖 Attentifs ensemble! (Jérôme Thorel)](http://www.editionsladecouverte.fr/catalogue/index-Attentifs_ensemble__-9782707174215.html)
* [🔗 RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees)

---

## Fort du constat que: 

« Quand on consulte des images djihadistes, on est un djihadiste. »

![](images/jnarac/surveillance/sarko.jpg)<!-- .element style="width: 75%" -->

Note:
- les petites phrases
  - les petits raccourcis
    - tiennent en 140 caractères
    - pas l'explication de l'impôt progressif et de la redistribution

---

## hum!

![](images/jnarac/surveillance/emma-watson.png)<!-- .element style="width: 125%" -->

---

<!-- .slide: data-background="images/jnarac/jnarac2/tescreal.jpeg"  class="trsp bottom" style="color: white" -->
## quel est le projet?

* **T**ranshumanism
* **E**xtropianism
* **S**ingularitarianism
* **C**osmism
* **R**ationalism
* **E**ffective **A**ltruism
* **L**ongtermism

Note:

- transhumanisme: réparé, amélioré, Dr Laurent Alexandre
- longtermisime que vous la vie des hommes vivants ici et maintenant face à la responsabilité de ce qui viendront dans 20 ans, 50 ans, 100 ans
- dans la continuité du discours méritocratique, libertarien et élististe servi par la mythologie de la Silicon Valley ...
- Jobs / Zuckerberg / Gates / Musk / Thiel / Bezos / Anderseen / la PayPal Mafia sont des adeptes avérés, connus et même assumé
  - probablement que tous les gens que vous avez vu dans la vidéo du début ont à voir avec cette mouvance
  - pas de taxes pour eux
  - de la fondation en oeuvre philantropique
  - et du business créé en dépouillant l'état 
    - de son argent
    - et de sa souveraineté
- comment va t on adhérer à un projet qui programme notre extinction et la consécration des plus riches et influents?

----

## (Res)sources

* [🔗 The Acronym Behind Our Wildest AI Dreams and Nightmares](https://www.truthdig.com/articles/the-acronym-behind-our-wildest-ai-dreams-and-nightmares/)

---

<!-- .slide: data-background="images/jnarac/jnarac2/biais.png"  class="trsp" style="color: white" -->
#### Par quels moyens?

* exploitation de biais cognitif
* striatum
  * plus de nourriture
  * plus de sexe
  * plus de condition social
  * plus d'information
  * moins d'effort
* inhibé par le cortex préfrontal
  * éducation
  * moral

----

## (Res)sources

* [🔗 Biais cognitif](https://fr.wikipedia.org/wiki/Biais_cognitif)
* [🔗 Striatum](https://fr.wikipedia.org/wiki/Striatum)
* [🔗 Cortex préfrontal](https://fr.wikipedia.org/wiki/Cortex_pr%C3%A9frontal)
* [📖 Le bug humain](https://www.librairielesvolcans.com/livre/9782266306249-le-bug-humain-sebastien-bohler/)

---

<!-- .slide: data-background="images/jnarac/jnarac2/dealers.jpg"  class="trsp" style="color: white" -->
### Les crochets addictifs

* logique de but
* temporalité du progès et de l'intensification
  * progression dosée
  * récompenses régulières
* la rétroaction et l'interaction sociale
* boucle d'inachèvement
  * déroulé infini

#### "créer des émotions, génére des habitudes"

Note:
* design technologique
* design cognitif 
  * économie de l'attention
* sécurité
* confort 
* rapidité
* comme un dealer de crack qui propose des doses gratuites
* flippant non?

----

## (Res)sources

* [🎥 Dopamine](https://www.arte.tv/fr/videos/RC-017841/dopamine/)
* [🎥 Comment les applis piègent notre cerveau?](https://www.arte.tv/fr/videos/109374-000-A/dopamine-comment-les-applis-piegent-notre-cerveau/)
* [🎥 La puissance des réseaux sociaux en question](https://www.arte.tv/fr/videos/109374-000-A/dopamine-comment-les-applis-piegent-notre-cerveau/)
* [🎥 Le Phénomène Terrifiant des Ipad Kids (ils sont foutus)](https://www.youtube.com/watch?v=Kbxxbs7emZw)

---

<!-- .slide: data-background="images/jnarac/jnarac2/freeparty.png"  class="trsp" style="color: white" -->
## Quoi faire?

* utiliser / penser l'alternative
  * la popriété est elle une liberté?
  * le pouvoir est il une nécessité?
  * créer des TAZ (ZAT)
* consommer / s'approprier l'art, la poésie, le beau, le tendre
* philosopher / faire de la propagande
* objectiver sa dépendance
* désactiver les notfis

----

## (Res)sources

* [📖 Taz ; zone autonome temporaire - Hakim Bey](https://www.librairielesvolcans.com/livre/9782841620203-taz-zone-autonome-temporaire-hakim-bey/)

---

<!-- .slide: data-background="images/jnarac/jnarac2/no_signal.gif" data-background-position="top" data-background-size="105%" -->

---

<!-- .slide: data-background="images/chun/applause.gif" style="color: white" class="trsp" -->
# Merci

---

<!-- .slide: data-background="images/jnarac/jnarac2/BigBrother.jpg" style="color: white;" class="trsp middle" -->
# Questions
# Réactions

---


<!--
<blockquote class="twitter-tweet">
    <p lang="en" dir="ltr">Thank fuck we jailed those 2 who threw soup over a completely covered painting for 2 years . <a href="https://t.co/Bjj8mau6kV">pic.twitter.com/Bjj8mau6kV</a></p>
    &mdash; Weeton4💙 (@Weeton64) 
    <a href="https://twitter.com/Weeton64/status/1839741067194728596?ref_src=twsrc%5Etfw">September 27, 2024</a>
</blockquote> 
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

---

* https://x.com/RachelTobac/status/1840138057812771301
* https://x.com/lecoindeslgbt/status/1817998752013001085
* https://x.com/Push_Olve/status/1824207784977543614
* https://x.com/vinceflibustier/status/1821948075482460322


## Société post vérité & démocratie

* https://www.avast.com/fr-fr/c-how-google-uses-your-data
* https://policies.google.com/technologies/partner-sites?hl=fr-CA
* https://support.google.com/accounts/answer/162744?hl=fr
* https://takeout.google.com

* https://www.privacyaffairs.com/fr/facebook-data-collection/
* https://www.lesnumeriques.com/societe-numerique/tout-ce-que-sait-facebook-sur-vous-et-plus-encore-a213289.html
* https://comarketing-news.fr/facebook-collecte-vos-donnees-meme-si-vous-navez-pas-de-compte/

-->