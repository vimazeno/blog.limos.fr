## X.509
* Certificat créé par la CA
* 1 seule signature : celle de la CA
* Confiance centralisée
  * Forêt d’arbres de confiance


## PGP
* Certificat créé par l’utilisateur
* Plusieurs signatures
* Confiance distribuée
  * Graph orienté de confiance


## X.509 centralise la confiance sur les CA

vs

## PGP distribue la confiance entre utilisateurs
