# Mots de passe

# <i class="fa fa-user-secret"></i>


### <i class="fa-solid fa-trophy"></i> Les mots de passes

1. ça ne se prête pas
2. ça ne se laisse pas traîner à la vue de tous
3. ça ne s'utilise qu'une fois
4. si ça casse on remplace immédiatement
5. un peu d'originalité ne nuit pas
6. la taille compte
7. il y a une date de péremption
8. mieux vaut les avoir avec soi


### <i class="fa-solid fa-head-side-mask"></i> C'est une question d'hygiène!

![preservatif](images/passwords/preservatif-darvador.jpg)<!-- .element width="30%" -->

[CNIL / Authentification par mot de passe : les mesures de sécurité élémentaires](https://www.cnil.fr/fr/authentification-par-mot-de-passe-les-mesures-de-securite-elementaires)


### [<i class="fab fa-firefox"></i> Firefox](https://www.mozilla.org/fr/firefox/new/)

* Stocke les mots de passes en clair
  * *Préférences > Sécurité > Identifiants enregistrés*
  * Propose un "mot de passe principal"
    * [Vulnérable](https://www.raymond.cc/blog/how-to-find-hidden-passwords-in-firefox/)

* [Firefox Sync](https://www.mozilla.org/en-US/firefox/sync/)


### [<i class="fab fa-chrome"></i> Chrome](https://www.google.fr/chrome/browser/desktop/)

* Stocke les [mots de passe en ligne](https://passwords.google.com/settings/passwords)
  * non [souverain](sovereignty.html)
    * comme [LastPass](https://www.lastpass.com/fr), [Dashlane](https://www.dashlane.com/), [iCloud](https://www.icloud.com/), ..., [Firefox Sync](https://www.mozilla.org/en-US/firefox/sync/)

![/o\](images/passwords/password.google.png)


### <i class="fa-solid fa-key"></i> [KeePass](http://keepass.info/)

* Gestionnaire (base de données, wallet, ...) de mots de passe
    * **souverain**

* <i class="fa fa-bullhorn"></i> [Produits Certifiés CSPN par l'ANSSI](https://www.ssi.gouv.fr/entreprise/certification_cspn/keepass-version-2-10-portable/)

* <i class="fa fa-bullhorn"></i> reco CNRS à l'[ANF Protection des données par le chiffrement (2015)](http://resinfo.org/spip.php?article86)                


### <i class="fa-solid fa-key"></i> [KeePass](http://keepass.info/)

<i class="fab fa-windows"></i> Full windows

* intégration en [mono](http://www.mono-project.com/) sous linux & Mac OS X
  * bugué sous certains environnement linux
* [nombreux plugins](http://keepass.info/plugins.html)
  * dont [keepasshttp](https://github.com/pfn/keepasshttp) <i class="fa fa-thumbs-o-up"></i> qui permet l'intégration au navigateur


### <i class="fa-solid fa-key"></i> [KeePass](http://keepass.info/)

* KeePass 1 (Classic Edition)
  * .kdb

* KeePass 2 <i class="fa fa-thumbs-o-up"></i> (Professional Edition)
  * .kdbx <i class="fa fa-thumbs-o-up"></i>
    * meilleure interropérabilité
  * permet d'attacher des fichiers
  * permet de lier un utilisateur windows à une base de données    


### <i class="fa-solid fa-key"></i> [KeePassX](https://www.keepassx.org/) <i class="fas fa-thumbs-down"></i>

* Portage officieux de keepass
  * cross-platform (<i class="fab fa-windows"></i> Windows, <i class="fab fa-apple"></i> Mac OS X, <i class="fab fa-linux"></i> Linux)

* Développement chaotique
    * non intégration d'une partie du code produit par la communauté
        * notamment le portage du plugin [keepasshttp](http://keepass.info/plugins.html#keepasshttp)


### <i class="fa-solid fa-key"></i> [KeePassXC](https://keepassxc.org/) <i class="fas fa-thumbs-up"></i>

* Portage officieux de keepass
    * [KeepassXC – A cross-platform community fork of KeepassX](https://news.ycombinator.com/item?id=13468261)
    * cross-platform (<i class="fab fa-windows"></i>, <i class="fab fa-apple"></i>, <i class="fab fa-linux"></i>)

* <strike>Récemment</strike> parfaitement stable
  * la [pulse du projet](https://github.com/keepassxreboot/keepassxc/pulse)

* Implémente en natif <strike>le RPC via http</strike> Unix sockets (ou pipes sous Windows)  pour l'intégration aux navigateurs <i class="fa fa-thumbs-o-up"></i>


### [<i class="fab fa-firefox"></i> <i class="fab fa-chrome"></i> KeePassXC-Browser Migration](https://keepassxc.org/docs/keepassxc-browser-migration/)

* pré-requis
  * [KeePassXC](https://keepassxc.org/) avec l'intégration aux bon navigateurs activée
* cherche un couple login mot de passe dans [KeePassXC](https://keepassxc.org/) à partir de l'url consultée
* sauvegarde les nouvelles entrées dans [KeePassXC](https://keepassxc.org/)


### <i class="fa-brands fa-android"></i> Android

* [KeePass2Android](https://play.google.com/store/apps/details?id=keepass2android.keepass2android&hl=fr) [<i class="fab fa-github"></i>](https://github.com/PhilippC/keepass2android)

* [KeePassDX](https://www.keepassdx.com/) [<i class="fab fa-github"></i>](https://github.com/Kunzisoft/KeePassDX)
    * disponible sur l'app store alternatif [F-Droid](https://f-droid.org/)
        * catalogue of FOSS (Free and Open Source Software) applications for the Android platform


### <i class="fa-brands fa-apple"></i> iOS

* N'hésitez pas à m'envoyer vos pointeurs


### <i class="fa-solid fa-server"></i> [Vault by HashiCorp](https://www.vaultproject.io/)

[![vault](images/passwords/vault.png)](https://www.vaultproject.io/)


## <i class="fa-solid fa-microchip"></i> Solution Hardware

[![mooltipass](images/passwords/mooltipass.jpg)](https://www.themooltipass.com/)
