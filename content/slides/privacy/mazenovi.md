#### [Vincent Mazenod](https://perso.limos.fr/mazenod)

<i class="fa fa-flask"></i> Ingénieur de recherche au [CNRS](https://cnrs.fr)

<i class="fa fa-desktop"></i> Responsable informatique / Dev / DevOps

<i class="fa fa-cogs"></i> En poste au [LIMOS](http://limos.fr)

<i class="fa fa-building-o"></i>  Bureau A115 - 1<sup>ère</sup> étage

<i class="fa fa-phone"></i>04 73 40 50 27

<i class="fa fa-envelope-o"></i> <a href="mailto:vincent.mazenod@isima.fr">vincent.mazenod@isima.fr</a>

<i class="fa fa-user-secret"></i> Expert [SSI](http://www.ssi.gouv.fr/) à la [CRSSI DR7 CNRS](http://www.dr7.cnrs.fr/spip.php?rubrique856)

<!-- i class="fa fa-pied-piper-alt"></i> Internetologue & futuropathe -->

[<i class="fa fa-briefcase"></i> cv](http://vincent.mazenod.fr)

Note:
- j'ai deux défauts
  - parler vite,je vais tout faire pour ralentir
  - utiliser des noms et autres acronymes qui semblent ne pas parler de manière égale à tout le monde
    - je fréquent pas mal dev
